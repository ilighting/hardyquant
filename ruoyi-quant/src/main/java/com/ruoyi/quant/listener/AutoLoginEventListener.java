package com.ruoyi.quant.listener;

import com.ruoyi.common.cjyocr.OcrService;
import com.ruoyi.common.config.EmailService;
import com.ruoyi.common.events.JobEvent;
import com.ruoyi.common.events.LoginEvent;
import com.ruoyi.quant.trade.api.request.BaseTradeRequest;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeUser;
import com.ruoyi.quant.trade.crud.service.IDfcfTradeService;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AutoLoginEventListener {

    private static final Logger log= LoggerFactory.getLogger(AutoLoginEventListener.class);
    @Autowired
    OcrService ocrService;

    @Autowired
    IDfcfTradeService dfcfTradeService;

    @EventListener
    public synchronized void handleMyEvent(LoginEvent event) {
        DfcfTradeUser dfcfTradeUser = dfcfTradeService.selectDfcfTradeByAccountId(event.getAccountId());
        String yzmApiUrl= BaseTradeRequest.TradeRequestMethod.YZM.getApiUrl();

        String randNum = "0.903" + new Date().getTime();
        String yzmUrl = yzmApiUrl + randNum;

        String identifyCode = ocrService.process(yzmUrl);
        log.info(identifyCode);
        dfcfTradeUser.setRandomCode(identifyCode);
        dfcfTradeUser.setRandomNum(randNum);
        String s = dfcfTradeService.loginDfcfApi(dfcfTradeUser);
        log.info(s);
    }
}