package com.ruoyi.quant.listener;

import com.ruoyi.common.config.EmailService;
import com.ruoyi.common.events.JobEvent;

import com.ruoyi.quant.trade.crud.service.IDfcfTradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class MyEventListener {

    @Autowired
    EmailService emailService;


    @EventListener
    public void handleMyEvent(JobEvent event) {
        System.out.println(event);

        String to = "508049890@qq.com";
        String subject = event.getJobName();
        String text = event.getContent()+"----" +
                ""+event.getAccountId();

        emailService.sendEmail(to, subject, text);
    }
}