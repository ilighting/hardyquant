package com.ruoyi.quant.trade.utils;

import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.StringColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.selection.Selection;

public class TablesawDemo {

    public static void main(String[] args) {
//
//        // 创建一个包含一些值的Double类型列
//        DoubleColumn column = DoubleColumn.create("column1", 2.5, 7.6, 4.8, 9.2, 3.1);
//
//        // 创建一个Selection对象，并选择所有大于5的行
//        Selection selection = column.isGreaterThan(5);
//
//        // 将满足条件的值设置为1
//        column.set(selection, 1d);
//        Table table=Table.create(column);
//
//        // 打印更新后的列中的值
//        System.out.println(table);
//


        // 创建一个示例表格
        StringColumn col1 = StringColumn.create("Name", "John", "Alice", "Bob");
        StringColumn col2 = StringColumn.create("City", "New York", "Paris", "London");
        Table table = Table.create("People", col1, col2);

        // 遍历每一行并打印行数据
        int rowCount = table.rowCount();
        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
            System.out.println("Row " + rowIndex + ": " + table.row(rowIndex));
        }


    }
}
