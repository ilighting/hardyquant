package com.ruoyi.quant.trade.api.request;

public class GetOrdersDataRequest extends BaseQueryRequest {

    public GetOrdersDataRequest(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.GetOrdersData;
    }

}
