package com.ruoyi.quant.trade.services.impl;


import com.ruoyi.quant.trade.model.po.TradeUser;
import com.ruoyi.quant.trade.services.ITradeUserService;
import jakarta.annotation.Resource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TradeUserServiceImpl implements ITradeUserService {

    @Resource
    private JdbcTemplate jdbcTemplate;




    @Override
    public TradeUser getById(String id) {
        return jdbcTemplate.queryForObject(
                "select id, account_id as accountId, password, cookie, validate_key as validateKey from dfcf_trade_user where account_id = ?",
                BeanPropertyRowMapper.newInstance(TradeUser.class), id);
    }

    @Override
    public void update(TradeUser tradeUser) {
        jdbcTemplate.update("update dfcf_trade_user set cookie = ?, validate_key = ? where id = ?",
                tradeUser.getCookie(), tradeUser.getValidateKey(), tradeUser.getId());
    }

    @Override
    public List<TradeUser> getList() {
        return jdbcTemplate.query(
                "select id, account_id as accountId, password, cookie, validate_key as validateKey from dfcf_trade_user",
                BeanPropertyRowMapper.newInstance(TradeUser.class));
    }

}
