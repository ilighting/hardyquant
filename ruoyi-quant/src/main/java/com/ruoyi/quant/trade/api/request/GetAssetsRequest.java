package com.ruoyi.quant.trade.api.request;

public class GetAssetsRequest extends BaseTradeRequest {

    private String hblx = "RMB";

    public GetAssetsRequest(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.GetAsserts;
    }

    public String getHblx() {
        return hblx;
    }

    public void setHblx(String hblx) {
        this.hblx = hblx;
    }

}
