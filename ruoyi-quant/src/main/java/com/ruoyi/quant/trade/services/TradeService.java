package com.ruoyi.quant.trade.services;


import com.ruoyi.quant.trade.api.request.BaseTradeRequest;
import com.ruoyi.quant.trade.api.response.CrQueryCollateralResponse;
import com.ruoyi.quant.trade.api.response.GetDealDataResponse;
import com.ruoyi.quant.trade.api.response.GetOrdersDataResponse;
import com.ruoyi.quant.trade.api.response.GetStockListResponse;
import com.ruoyi.quant.trade.model.po.*;
import com.ruoyi.quant.trade.model.vo.PageParam;
import com.ruoyi.quant.trade.model.vo.PageVo;
import com.ruoyi.quant.trade.model.vo.trade.DealVo;
import com.ruoyi.quant.trade.model.vo.trade.OrderVo;
import com.ruoyi.quant.trade.model.vo.trade.StockVo;
import com.ruoyi.quant.trade.model.vo.trade.TradeRuleVo;
import com.ruoyi.quant.trade.utils.StockConsts;
import org.springframework.cache.annotation.Cacheable;

import java.util.Date;
import java.util.List;

public interface TradeService {

    TradeMethod getTradeMethodByName(String name);

    TradeMethod getTradeMethod(BaseTradeRequest.TradeRequestMethod name);

    List<TradeUser> getTradeUserList();

    TradeUser getTradeUserById(String id);

    void updateTradeUser(TradeUser tradeUser);

    List<DealVo> getTradeDealList(List<? extends GetDealDataResponse> data);

    List<StockVo> getTradeStockList(List<GetStockListResponse> stockList);

    List<StockVo> getCrTradeStockList(List<CrQueryCollateralResponse> stockList);

    List<OrderVo> getTradeOrderList(List<? extends GetOrdersDataResponse> orderList);

    List<StockVo> getTradeStockListBySelected(List<StockSelected> selectList);

    PageVo<TradeRuleVo> getTradeRuleList(PageParam pageParam);

    void changeTradeRuleState(int state, int id);

    List<TradeOrder> getLastTradeOrderListByRuleId(int ruleId, String userId);

    void saveTradeOrderList(List<TradeOrder> tradeOrderList);

    void resetRule(int id);

    List<TradeDeal> getTradeDealListByDate(Date date);

    void saveTradeDealList(List<TradeDeal> list);

}
