package com.ruoyi.quant.trade.api.request;

public abstract class BaseTradeRequest {

    // 返回格式类型
    // { Data: [] }
    public static final int VERSION_DATA_LIST = 0;
    // { Data: {} }
    public static final int VERSION_DATA_OBJ = 1;
    // msg...
    public static final int VERSION_MSG = 2;
    // { }
    public static final int VERSION_OBJ = 3;

    private String userId;

    protected BaseTradeRequest(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public abstract TradeRequestMethod getMethod();

    public enum TradeRequestMethod {

        GetAsserts("get_asserts","https://jywg.18.cn/Com/queryAssetAndPositionV1?validatekey=${validatekey}","我的资产"),
        Submit("submit","https://jywg.18.cn/Trade/SubmitTradeV2?validatekey=${validatekey}","提交委托"),
        Revoke("revoke","https://jywg.18.cn/Trade/RevokeOrders?validatekey=${validatekey}","撤单"),
        GetStockList("get_stock_list","https://jywg.18.cn/Search/GetStockList?validatekey=${validatekey}","我的持仓"),
        GetOrdersData("get_orders_data","https://jywg.18.cn/Search/GetOrdersData?validatekey=${validatekey}","当日委托"),
        GetDealData("get_deal_data","https://jywg.18.cn/Search/GetDealData?validatekey=${validatekey}","当日成交"),
        Authentication("authentication","https://jywg.18.cn/Login/Authentication","登录"),
        AuthenticationCheck("authentication_check","https://jywg.18.cn/Trade/Buy","登录验证"),
        GetHisDealData("get_his_deal_data","https://jywg.18.cn/Search/GetHisDealData?validatekey=${validatekey}","历史成交"),
        GetHisOrdersData("get_his_orders_data","https://jywg.18.cn/Search/GetHisOrdersData?validatekey=${validatekey}","历史委托"),
        GetCanBuyNewStockListV3("get_can_buy_new_stock_list_v3","https://jywg.18.cn/Trade/GetCanBuyNewStockListV3?validatekey=${validatekey}","查询可申购新股列表"),
        GetConvertibleBondListV2("get_convertible_bond_list_v2","https://jywg.18.cn/Trade/GetConvertibleBondListV2?validatekey=${validatekey}","查询可申购新债列表"),
        SubmitBatTradeV2("submit_bat_trade_v2","https://jywg.18.cn/Trade/SubmitBatTradeV2?validatekey=${validatekey}","批量申购"),
        YZM("yzm","https://jywg.18.cn/Login/YZM?randNum=","登录验证码"),
        CrGetRzrqAsserts("cr_get_rzrq_asserts","https://jywg.18.cn/MarginSearch/GetRzrqAssets?validatekey=${validatekey}","信用我的资产"),
        CrQueryCollateral("cr_query_collateral","https://jywg.18.cn/MarginSearch/QueryCollateral?validatekey=${validatekey}","信用我的持仓"),
        CrSubmit("cr_submit","https://jywg.18.cn/MarginTrade/SubmitTradeV2?validatekey=${validatekey}","信用提交委托"),
        CrGetOrdersData("cr_get_orders_data","https://jywg.18.cn/MarginSearch/GetOrdersData?validatekey=${validatekey}","信用当日委托"),
        CrGetDealData("cr_get_deal_data","https://jywg.18.cn/MarginSearch/GetDealData?validatekey=${validatekey}","信用当日成交"),
        CrGetHisDealData("cr_get_his_deal_data","https://jywg.18.cn/MarginSearch/GetHisDealData?validatekey=${validatekey}","信用历史成交"),
        CrGetHisOrdersData("cr_get_his_orders_data","https://jywg.18.cn/MarginSearch/GetHisOrdersData?validatekey=${validatekey}","信用历史委托"),
        CrRevoke("cr_revoke","https://jywg.18.cn/MarginTrade/RevokeOrders?validatekey=${validatekey}","信用撤单"),
        CrGetCanBuyNewStockListV3("cr_get_can_buy_new_stock_list_v3","https://jywg.18.cn/MarginTrade/GetCanBuyNewStockListV3?validatekey=${validatekey}","信用查询可申购新股列表"),
        CrGetConvertibleBondListV2("cr_get_convertible_bond_list_v2","https://jywg.18.cn/MarginTrade/GetConvertibleBondListV2?validatekey=${validatekey}","信用查询可申购新债列表"),
        CrSubmitBatTradeV2("cr_submit_bat_trade_v2","https://jywg.18.cn/MarginTrade/SubmitBatTradeV2?validatekey=${validatekey}","信用批量申购");
        private String value;
        private String apiUrl;
        private String desc;

        public String getApiUrl() {
            return apiUrl;
        }

        public void setApiUrl(String apiUrl) {
            this.apiUrl = apiUrl;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        TradeRequestMethod(String value, String apiUrl, String desc) {
            this.value = value;
            this.apiUrl = apiUrl;
            this.desc = desc;
        }
    }

    public int responseVersion() {
        return VERSION_DATA_LIST;
    }

}
