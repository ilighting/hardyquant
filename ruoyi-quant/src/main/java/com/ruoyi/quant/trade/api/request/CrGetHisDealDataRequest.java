package com.ruoyi.quant.trade.api.request;

public class CrGetHisDealDataRequest extends GetHisDealDataRequest {

    public CrGetHisDealDataRequest(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.CrGetHisDealData;
    }

}
