package com.ruoyi.quant.trade.api.request;

public class GetCanBuyNewStockListV3Request extends BaseTradeRequest {

    public GetCanBuyNewStockListV3Request(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.GetCanBuyNewStockListV3;
    }

    @Override
    public int responseVersion() {
        return VERSION_OBJ;
    }

}
