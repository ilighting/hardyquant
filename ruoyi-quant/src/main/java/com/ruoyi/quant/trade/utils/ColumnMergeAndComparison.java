package com.ruoyi.quant.trade.utils;

import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.Table;

public class ColumnMergeAndComparison {

    public static void main(String[] args) {

        // 创建示例表
        DoubleColumn closePrice = DoubleColumn.create("ClosePrice", new double[]{100.5, 99.8, 105.2, 98.7, 110.0});
        DoubleColumn movingAverage = DoubleColumn.create("MA5", new double[]{101.2, 98.6, 100.1, 99.5, 105.3});

        // 合并两列
        Table mergedTable = Table.create(closePrice,movingAverage);

        // 进行交叉比较筛选
        //ma5>ma30 buy
        //ma5<ma10 sell
        Table filteredTable = mergedTable.where(
                mergedTable.numberColumn("ClosePrice").isGreaterThan(mergedTable.numberColumn("MA5"))
        );

        // 打印结果
        System.out.println(filteredTable);
    }
}