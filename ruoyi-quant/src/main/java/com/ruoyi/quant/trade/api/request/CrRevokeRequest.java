package com.ruoyi.quant.trade.api.request;

public class CrRevokeRequest extends RevokeRequest {

    public CrRevokeRequest(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.CrRevoke;
    }

}
