package com.ruoyi.quant.trade.crud.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.quant.trade.crud.mapper.DfcfTradeOrderMapper;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeOrder;
import com.ruoyi.quant.trade.crud.service.IDfcfTradeOrderService;
import com.ruoyi.common.core.text.Convert;

/**
 * 订单记录Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-09
 */
@Service
public class DfcfTradeOrderServiceImpl extends ServiceImpl<DfcfTradeOrderMapper,DfcfTradeOrder> implements IDfcfTradeOrderService
{
    @Autowired
    private DfcfTradeOrderMapper dfcfTradeOrderMapper;

    /**
     * 查询订单记录
     *
     * @param id 订单记录主键
     * @return 订单记录
     */
    @Override
    public DfcfTradeOrder selectDfcfTradeOrderById(Long id)
    {
        return dfcfTradeOrderMapper.selectDfcfTradeOrderById(id);
    }

    /**
     * 查询订单记录列表
     *
     * @param dfcfTradeOrder 订单记录
     * @return 订单记录
     */
    @Override
    public List<DfcfTradeOrder> selectDfcfTradeOrderList(DfcfTradeOrder dfcfTradeOrder)
    {
        return dfcfTradeOrderMapper.selectDfcfTradeOrderList(dfcfTradeOrder);
    }

    /**
     * 新增订单记录
     *
     * @param dfcfTradeOrder 订单记录
     * @return 结果
     */
    @Override
    public int insertDfcfTradeOrder(DfcfTradeOrder dfcfTradeOrder)
    {
        dfcfTradeOrder.setCreateTime(DateUtils.getNowDate());
        return dfcfTradeOrderMapper.insertDfcfTradeOrder(dfcfTradeOrder);
    }

    /**
     * 修改订单记录
     *
     * @param dfcfTradeOrder 订单记录
     * @return 结果
     */
    @Override
    public int updateDfcfTradeOrder(DfcfTradeOrder dfcfTradeOrder)
    {
        dfcfTradeOrder.setUpdateTime(DateUtils.getNowDate());
        return dfcfTradeOrderMapper.updateDfcfTradeOrder(dfcfTradeOrder);
    }

    /**
     * 批量删除订单记录
     *
     * @param ids 需要删除的订单记录主键
     * @return 结果
     */
    @Override
    public int deleteDfcfTradeOrderByIds(String ids)
    {
        return dfcfTradeOrderMapper.deleteDfcfTradeOrderByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单记录信息
     *
     * @param id 订单记录主键
     * @return 结果
     */
    @Override
    public int deleteDfcfTradeOrderById(Long id)
    {
        return dfcfTradeOrderMapper.deleteDfcfTradeOrderById(id);
    }
}
