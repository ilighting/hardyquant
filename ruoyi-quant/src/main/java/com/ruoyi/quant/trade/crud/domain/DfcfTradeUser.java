package com.ruoyi.quant.trade.crud.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 东方财富交易界面对象 dfcf_trade
 *
 * @author ruoyi
 * @date 2023-10-29
 */
public class DfcfTradeUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long id;
    /** 账号id */
    @Excel(name = "账号id")
    private String accountId;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 验证秘钥 */
    @Excel(name = "验证秘钥")
    private String validateKey;

    /** cookie缓存 */
    @Excel(name = "cookie缓存")
    private String cookie;

    /** 账户总额 */
    @Excel(name = "账户总额")
    private BigDecimal totalAmount;

    /** 可用余额 */
    @Excel(name = "可用余额")
    private BigDecimal ableAmount;

    /** 可提账户 */
    @Excel(name = "可提账户")
    private BigDecimal wdAbleAmount;

    /** 冻结账户 */
    @Excel(name = "冻结账户")
    private BigDecimal frozenAmount;

    private String randomCode;

    private String randomNum;

    private String userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getRandomNum() {
        return randomNum;
    }

    public void setRandomNum(final String randomNum) {
        this.randomNum = randomNum;
    }

    public String getRandomCode() {
        return randomCode;
    }

    public void setRandomCode(final String randomCode) {
        this.randomCode = randomCode;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getAccountId()
    {
        return accountId;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }
    public void setValidateKey(String validateKey)
    {
        this.validateKey = validateKey;
    }

    public String getValidateKey()
    {
        return validateKey;
    }
    public void setCookie(String cookie)
    {
        this.cookie = cookie;
    }

    public String getCookie()
    {
        return cookie;
    }
    public void setTotalAmount(BigDecimal totalAmount)
    {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalAmount()
    {
        return totalAmount;
    }
    public void setAbleAmount(BigDecimal ableAmount)
    {
        this.ableAmount = ableAmount;
    }

    public BigDecimal getAbleAmount()
    {
        return ableAmount;
    }
    public void setWdAbleAmount(BigDecimal wdAbleAmount)
    {
        this.wdAbleAmount = wdAbleAmount;
    }

    public BigDecimal getWdAbleAmount()
    {
        return wdAbleAmount;
    }
    public void setFrozenAmount(BigDecimal frozenAmount)
    {
        this.frozenAmount = frozenAmount;
    }

    public BigDecimal getFrozenAmount()
    {
        return frozenAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("accountId", getAccountId())
            .append("password", getPassword())
            .append("validateKey", getValidateKey())
            .append("cookie", getCookie())
            .append("totalAmount", getTotalAmount())
            .append("ableAmount", getAbleAmount())
            .append("wdAbleAmount", getWdAbleAmount())
            .append("frozenAmount", getFrozenAmount())
            .toString();
    }
}
