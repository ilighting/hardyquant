package com.ruoyi.quant.trade.api.request;

public class CrGetDealDataRequest extends GetDealDataRequest {

    public CrGetDealDataRequest(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.CrGetDealData;
    }

}
