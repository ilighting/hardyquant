package com.ruoyi.quant.trade.clients;

import java.io.IOException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.quant.trade.utils.HttpUtil;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class TradeClient {

    private static final Logger log= LoggerFactory.getLogger(TradeClient.class);

    private final ThreadLocal<ClientWrapper> threadLocal = new ThreadLocal<>();

    @Autowired
    private CloseableHttpClient httpClient;

    public String send(String url, Map<String, Object> params, Map<String, String> header) {
        String content = HttpUtil.sendPost(httpClient, url, params, header);
        if (content.contains("Object moved")) {
            throw new UnauthorizedException("unauthorized " + url);
        }
        return content;
    }

    public String sendListJson(String url, List<Map<String, Object>> list, Map<String, String> header) {
        String content = HttpUtil.sendPostJson(httpClient, url, list, header);
        if (content.contains("Object moved")) {
            throw new UnauthorizedException("unauthorized " + url);
        }
        return content;
    }

    public void openSession() {
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(2)
                .setConnectTimeout(2)
                .setSocketTimeout(2).build();

        ClientWrapper wrapper = new ClientWrapper();
        wrapper.cookieStore = new BasicCookieStore();
        wrapper.httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setDefaultCookieStore(wrapper.cookieStore).build();
        threadLocal.set(wrapper);
    }

    public String sendNewInstance(String url, Map<String, Object> params, Map<String, String> header) {
        ClientWrapper clientWrapper = threadLocal.get();
        assertOpened(clientWrapper);
        return HttpUtil.sendPost(clientWrapper.httpClient, url, params, header);
    }

    public String getCurrentCookie() {
        ClientWrapper clientWrapper = threadLocal.get();
        assertOpened(clientWrapper);
        return clientWrapper.cookieStore.getCookies().stream().map(cookie -> cookie.getName() + "=" + cookie.getValue()).collect(Collectors.joining("; "));
    }

    public void destoryCurrentSession() {
        try {
        ClientWrapper clientWrapper = threadLocal.get();
        threadLocal.remove();
        assertOpened(clientWrapper);

            clientWrapper.httpClient.close();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    private void assertOpened(ClientWrapper clientWrapper) {
        if (clientWrapper == null) {
            throw new ServiceException("please call open session first");
        }
    }

    private static class ClientWrapper {
        private CloseableHttpClient httpClient;
        private BasicCookieStore cookieStore;
    }

}
