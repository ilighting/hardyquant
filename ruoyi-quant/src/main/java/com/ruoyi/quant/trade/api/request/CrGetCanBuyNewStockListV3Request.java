package com.ruoyi.quant.trade.api.request;

public class CrGetCanBuyNewStockListV3Request extends GetCanBuyNewStockListV3Request {

    public CrGetCanBuyNewStockListV3Request(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.CrGetCanBuyNewStockListV3;
    }

}
