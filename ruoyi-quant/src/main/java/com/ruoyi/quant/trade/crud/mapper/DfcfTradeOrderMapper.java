package com.ruoyi.quant.trade.crud.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单记录Mapper接口
 *
 * @author ruoyi
 * @date 2023-11-09
 */
@Mapper
public interface DfcfTradeOrderMapper extends BaseMapper<DfcfTradeOrder>
{
    /**
     * 查询订单记录
     *
     * @param id 订单记录主键
     * @return 订单记录
     */
    public DfcfTradeOrder selectDfcfTradeOrderById(Long id);

    /**
     * 查询订单记录列表
     *
     * @param dfcfTradeOrder 订单记录
     * @return 订单记录集合
     */
    public List<DfcfTradeOrder> selectDfcfTradeOrderList(DfcfTradeOrder dfcfTradeOrder);

    /**
     * 新增订单记录
     *
     * @param dfcfTradeOrder 订单记录
     * @return 结果
     */
    public int insertDfcfTradeOrder(DfcfTradeOrder dfcfTradeOrder);

    /**
     * 修改订单记录
     *
     * @param dfcfTradeOrder 订单记录
     * @return 结果
     */
    public int updateDfcfTradeOrder(DfcfTradeOrder dfcfTradeOrder);

    /**
     * 删除订单记录
     *
     * @param id 订单记录主键
     * @return 结果
     */
    public int deleteDfcfTradeOrderById(Long id);

    /**
     * 批量删除订单记录
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDfcfTradeOrderByIds(String[] ids);
}
