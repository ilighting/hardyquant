//package com.ruoyi.quant.trade.utils;
//
//import tech.tablesaw.api.DoubleColumn;
//import tech.tablesaw.api.Table;
//
//public class Main {
//    public static void main(String[] args) {
//        // 创建示例表
//        DoubleColumn closePrice = DoubleColumn.create("ClosePrice", new double[]{100.5, 99.8, 105.2, 98.7, 110.0});
//        DoubleColumn movingAverage = DoubleColumn.create("MA5", new double[]{101.2, 98.6, 100.1, 99.5, 105.3});
//
//        // 合并两列
//        Table mergedTable = Table.create(closePrice, movingAverage);
//
//        // 创建新列并根据筛选条件赋值
//        DoubleColumn signalColumn = mergedTable.doubleColumn("ClosePrice").apply(
//                        value -> value > mergedTable.doubleColumn("MA5").get(value.rowNumber()) ? 1.0 : 0.0)
//                .setName("Signal");
//
//        // 将新列添加到表格中
//        mergedTable.addColumns(signalColumn);
//
//        // 打印结果表格
//        System.out.println(mergedTable);
//    }
//}
