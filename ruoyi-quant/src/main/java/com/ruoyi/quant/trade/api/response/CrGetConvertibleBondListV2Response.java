package com.ruoyi.quant.trade.api.response;

public class CrGetConvertibleBondListV2Response extends GetConvertibleBondListV2Response {

    private String BUYREDEMPRICE;

    public String getBUYREDEMPRICE() {
        return BUYREDEMPRICE;
    }

    public void setBUYREDEMPRICE(String bUYREDEMPRICE) {
        BUYREDEMPRICE = bUYREDEMPRICE;
    }

}
