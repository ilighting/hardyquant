package com.ruoyi.quant.trade.api.request;

public class GetConvertibleBondListV2Request extends BaseTradeRequest {

    public GetConvertibleBondListV2Request(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.GetConvertibleBondListV2;
    }

}
