-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单记录', '2008', '1', '/quant/tradeOrder', 'C', '0', 'quant:tradeOrder:view', '#', 'admin', sysdate(), '', null, '订单记录菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单记录查询', @parentId, '1',  '#',  'F', '0', 'quant:tradeOrder:list',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单记录新增', @parentId, '2',  '#',  'F', '0', 'quant:tradeOrder:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单记录修改', @parentId, '3',  '#',  'F', '0', 'quant:tradeOrder:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单记录删除', @parentId, '4',  '#',  'F', '0', 'quant:tradeOrder:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('订单记录导出', @parentId, '5',  '#',  'F', '0', 'quant:tradeOrder:export',       '#', 'admin', sysdate(), '', null, '');
