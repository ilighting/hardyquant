package com.ruoyi.quant.trade.utils;

import tech.tablesaw.api.DateColumn;
import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.selection.Selection;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class TableMergeByDate {

    public static void main(String[] args) {

        // 创建示例表1
        DateColumn date1 = DateColumn.create("date", Arrays.asList(new LocalDate[]{LocalDate.parse("20210101"), LocalDate.parse("2021-01-03"),LocalDate.parse( "2021-01-03")}));
        DoubleColumn price1 = DoubleColumn.create("close", new double[]{100.5, 99.8, 105.2});
        Table table1 = Table.create("Table1", date1, price1);

        // 创建示例表2
        DateColumn date2 = DateColumn.create("date",new LocalDate[]{LocalDate.parse("20210101", DateTimeFormatter.ISO_LOCAL_DATE), LocalDate.parse("20210102"), LocalDate.parse("2021-01-04")});
        DoubleColumn price2 = DoubleColumn.create("open", new double[]{102.0, 105.5, 108.2});
        Table table2 = Table.create("Table2", date2, price2);

        // 按日期合并表格
        Table mergedTable = table1.joinOn("date").inner(table2);
        Selection close = mergedTable.numberColumn("close").isGreaterThan(1);

        // 创建一个长度为10的Double类型列，初始值都为1
        DoubleColumn column = DoubleColumn.create("column1", 1.0, 10);


//        mergedTable.column("close").set(close,column);
        // 打印合并后的结果
        System.out.println(mergedTable);
    }
}
