-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('回测订单分析图', '2008', '1', '/quant/tradeView', 'C', '0', 'quant:tradeView:view', '#', 'admin', sysdate(), '', null, '回测订单分析图菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('回测订单分析图查询', @parentId, '1',  '#',  'F', '0', 'quant:tradeView:list',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('回测订单分析图新增', @parentId, '2',  '#',  'F', '0', 'quant:tradeView:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('回测订单分析图修改', @parentId, '3',  '#',  'F', '0', 'quant:tradeView:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('回测订单分析图删除', @parentId, '4',  '#',  'F', '0', 'quant:tradeView:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('回测订单分析图导出', @parentId, '5',  '#',  'F', '0', 'quant:tradeView:export',       '#', 'admin', sysdate(), '', null, '');
