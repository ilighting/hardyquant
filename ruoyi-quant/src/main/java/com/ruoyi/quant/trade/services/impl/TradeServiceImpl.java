package com.ruoyi.quant.trade.services.impl;


import com.ruoyi.quant.trade.api.request.BaseTradeRequest;
import com.ruoyi.quant.trade.api.response.CrQueryCollateralResponse;
import com.ruoyi.quant.trade.api.response.GetDealDataResponse;
import com.ruoyi.quant.trade.api.response.GetOrdersDataResponse;
import com.ruoyi.quant.trade.api.response.GetStockListResponse;
import com.ruoyi.quant.trade.model.po.*;
import com.ruoyi.quant.trade.model.vo.PageParam;
import com.ruoyi.quant.trade.model.vo.PageVo;
import com.ruoyi.quant.trade.model.vo.trade.DealVo;
import com.ruoyi.quant.trade.model.vo.trade.OrderVo;
import com.ruoyi.quant.trade.model.vo.trade.StockVo;
import com.ruoyi.quant.trade.model.vo.trade.TradeRuleVo;
import com.ruoyi.quant.trade.services.ITradeUserService;
import com.ruoyi.quant.trade.services.TradeService;
import com.ruoyi.quant.trade.utils.StockConsts;
import com.ruoyi.quant.trade.utils.StockUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;



import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TradeServiceImpl implements TradeService {


    @Autowired
    private ITradeUserService tradeUserService;


    @Cacheable(value = StockConsts.CACHE_KEY_TRADE_METHOD, key = "#name", unless="#result == null")
    @Override
    public TradeMethod getTradeMethodByName(String name) {
        String methodUrl = getMethod(name);
        TradeMethod method=new TradeMethod();
        method.setName(name);
        method.setUrl(methodUrl);
        method.setState(1);
        return method;
    }

    @Cacheable(value = StockConsts.CACHE_KEY_TRADE_METHOD2, key = "#name", unless="#result == null")
    @Override
    public TradeMethod getTradeMethod(BaseTradeRequest.TradeRequestMethod name) {
        TradeMethod method=new TradeMethod();
        method.setName(name.getDesc());
        method.setUrl(name.getApiUrl());
        method.setState(1);

        return method;
    }

    public String getMethod(String name){
        String apiUrl="";
        for (BaseTradeRequest.TradeRequestMethod value : BaseTradeRequest.TradeRequestMethod.values()) {
            if (value.getValue().equals(name)) {
                apiUrl=value.getApiUrl();
            }
        }
        return apiUrl;
    }


    @Cacheable(value = StockConsts.CACHE_KEY_TRADE_USER_LIST, key = "'all'", unless="#result.size() == 0")
    @Override
    public List<TradeUser> getTradeUserList() {
        return tradeUserService.getList();
    }

    @Cacheable(value = StockConsts.CACHE_KEY_TRADE_USER, key = "#id.toString()", unless="#result == null")
    @Override
    public TradeUser getTradeUserById(String id) {
        TradeUser tradeUser = tradeUserService.getById(id);
        if (tradeUser != null && "资金账号".equals(tradeUser.getAccountId())) {
            return null;
        }
        return tradeUser;
    }

    @CacheEvict(value = StockConsts.CACHE_KEY_TRADE_USER, key = "#tradeUser.id.toString()")
    @Override
    public void updateTradeUser(TradeUser tradeUser) {
//        tradeUserService.updateById(tradeUser);
    }

    @Override
    public List<DealVo> getTradeDealList(List<? extends GetDealDataResponse> data) {
        return new ArrayList<>();
    }

    @Override
    public List<StockVo> getTradeStockList(List<GetStockListResponse> stockList) {
        return stockList.stream().map(v -> {
            StockVo stockVo = getStockVo(v.getZqdm(), null);
            stockVo.setName(v.getZqmc());
            stockVo.setTotalVolume(Integer.parseInt(v.getZqsl()));
            stockVo.setPrice(new BigDecimal(v.getZxjg()));
            stockVo.setCostPrice(new BigDecimal(v.getCbjg()));

            stockVo.setAvailableVolume(Integer.parseInt(v.getKysl()));
            stockVo.setProfit(new BigDecimal(v.getLjyk()));
            return stockVo;
        }).collect(Collectors.toList());
    }

    @Override
    public List<StockVo> getCrTradeStockList(List<CrQueryCollateralResponse> stockList) {
        List<StockVo> list = stockList.stream().map(v -> {
            StockVo stockVo = getStockVo(v.getZqdm(), null);
            stockVo.setName(v.getZqmc());
            stockVo.setAvailableVolume(Integer.parseInt(v.getGfky()));
            stockVo.setTotalVolume(Integer.parseInt(v.getZqsl()));
            stockVo.setPrice(new BigDecimal(v.getZxjg()));
            stockVo.setCostPrice(new BigDecimal(v.getCbjg()));
            stockVo.setProfit(new BigDecimal(v.getFdyk()));
            return stockVo;
        }).collect(Collectors.toList());
        return list;
    }

    @Override
    public List<StockVo> getTradeStockListBySelected(List<StockSelected> selectList) {
        List<String> codeList = selectList.stream().map(v -> StockUtil.getFullCode(v.getCode())).collect(Collectors.toList());

        return new ArrayList<>();
    }

    private StockVo getStockVo(String stockCode, List<DailyIndex> dailyIndexList) {

        return new StockVo();
    }

    @Override
    public List<OrderVo> getTradeOrderList(List<? extends GetOrdersDataResponse> orderList) {
        ArrayList<OrderVo> objects = new ArrayList<>();
        return objects;
    }

    @Override
    public PageVo<TradeRuleVo> getTradeRuleList(PageParam pageParam) {
        return null;
    }

    @Override
    public void changeTradeRuleState(int state, int id) {
//        tradeRuleDao.updateState(state, id);
    }

    @Override
    public List<TradeOrder> getLastTradeOrderListByRuleId(int ruleId, String userId) {
        return new ArrayList<>();
    }

    @Override
    public void saveTradeOrderList(List<TradeOrder> tradeOrderList) {
        for (TradeOrder tradeOrder : tradeOrderList) {

        }
    }

    @Override
    public void resetRule(int id) {

    }

    @Override
    public void saveTradeDealList(List<TradeDeal> list) {

    }

    @Override
    public List<TradeDeal> getTradeDealListByDate(Date date) {
        return new ArrayList<>();
    }

}
