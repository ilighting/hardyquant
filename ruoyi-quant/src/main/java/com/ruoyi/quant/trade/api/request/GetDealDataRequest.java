package com.ruoyi.quant.trade.api.request;

public class GetDealDataRequest extends BaseQueryRequest {

    public GetDealDataRequest(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.GetDealData;
    }

}
