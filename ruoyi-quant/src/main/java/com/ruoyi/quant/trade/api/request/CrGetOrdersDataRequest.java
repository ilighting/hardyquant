package com.ruoyi.quant.trade.api.request;

public class CrGetOrdersDataRequest extends GetOrdersDataRequest {

    public CrGetOrdersDataRequest(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.CrGetOrdersData;
    }

}
