package com.ruoyi.quant.trade.crud.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeOrder;

/**
 * 订单记录Service接口
 *
 * @author ruoyi
 * @date 2023-11-09
 */
public interface IDfcfTradeOrderService extends IService<DfcfTradeOrder>
{
    /**
     * 查询订单记录
     *
     * @param id 订单记录主键
     * @return 订单记录
     */
    public DfcfTradeOrder selectDfcfTradeOrderById(Long id);

    /**
     * 查询订单记录列表
     *
     * @param dfcfTradeOrder 订单记录
     * @return 订单记录集合
     */
    public List<DfcfTradeOrder> selectDfcfTradeOrderList(DfcfTradeOrder dfcfTradeOrder);

    /**
     * 新增订单记录
     *
     * @param dfcfTradeOrder 订单记录
     * @return 结果
     */
    public int insertDfcfTradeOrder(DfcfTradeOrder dfcfTradeOrder);

    /**
     * 修改订单记录
     *
     * @param dfcfTradeOrder 订单记录
     * @return 结果
     */
    public int updateDfcfTradeOrder(DfcfTradeOrder dfcfTradeOrder);

    /**
     * 批量删除订单记录
     *
     * @param ids 需要删除的订单记录主键集合
     * @return 结果
     */
    public int deleteDfcfTradeOrderByIds(String ids);

    /**
     * 删除订单记录信息
     *
     * @param id 订单记录主键
     * @return 结果
     */
    public int deleteDfcfTradeOrderById(Long id);
}
