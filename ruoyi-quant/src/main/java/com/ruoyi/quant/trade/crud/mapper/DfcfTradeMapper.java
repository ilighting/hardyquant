package com.ruoyi.quant.trade.crud.mapper;

import java.util.List;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeUser;

/**
 * 东方财富交易界面Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-29
 */
public interface DfcfTradeMapper 
{
    /**
     * 查询东方财富交易界面
     * 
     * @param accountId 东方财富交易界面主键
     * @return 东方财富交易界面
     */
    public DfcfTradeUser selectDfcfTradeByAccountId(String accountId);

    /**
     * 查询东方财富交易界面列表
     * 
     * @param dfcfTradeUser 东方财富交易界面
     * @return 东方财富交易界面集合
     */
    public List<DfcfTradeUser> selectDfcfTradeList(DfcfTradeUser dfcfTradeUser);

    /**
     * 新增东方财富交易界面
     * 
     * @param dfcfTradeUser 东方财富交易界面
     * @return 结果
     */
    public int insertDfcfTrade(DfcfTradeUser dfcfTradeUser);

    /**
     * 修改东方财富交易界面
     * 
     * @param dfcfTradeUser 东方财富交易界面
     * @return 结果
     */
    public int updateDfcfTrade(DfcfTradeUser dfcfTradeUser);

    /**
     * 删除东方财富交易界面
     * 
     * @param accountId 东方财富交易界面主键
     * @return 结果
     */
    public int deleteDfcfTradeByAccountId(String accountId);

    /**
     * 批量删除东方财富交易界面
     * 
     * @param accountIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDfcfTradeByAccountIds(String[] accountIds);
}
