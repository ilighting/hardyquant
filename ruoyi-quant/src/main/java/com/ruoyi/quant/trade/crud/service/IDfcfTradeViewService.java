package com.ruoyi.quant.trade.crud.service;

import java.util.List;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeView;

/**
 * 回测订单分析图Service接口
 * 
 * @author ruoyi
 * @date 2023-11-09
 */
public interface IDfcfTradeViewService 
{
    /**
     * 查询回测订单分析图
     * 
     * @param id 回测订单分析图主键
     * @return 回测订单分析图
     */
    public DfcfTradeView selectDfcfTradeViewById(Long id);

    /**
     * 查询回测订单分析图列表
     * 
     * @param dfcfTradeView 回测订单分析图
     * @return 回测订单分析图集合
     */
    public List<DfcfTradeView> selectDfcfTradeViewList(DfcfTradeView dfcfTradeView);

    /**
     * 新增回测订单分析图
     * 
     * @param dfcfTradeView 回测订单分析图
     * @return 结果
     */
    public int insertDfcfTradeView(DfcfTradeView dfcfTradeView);

    /**
     * 修改回测订单分析图
     * 
     * @param dfcfTradeView 回测订单分析图
     * @return 结果
     */
    public int updateDfcfTradeView(DfcfTradeView dfcfTradeView);

    /**
     * 批量删除回测订单分析图
     * 
     * @param ids 需要删除的回测订单分析图主键集合
     * @return 结果
     */
    public int deleteDfcfTradeViewByIds(String ids);

    /**
     * 删除回测订单分析图信息
     * 
     * @param id 回测订单分析图主键
     * @return 结果
     */
    public int deleteDfcfTradeViewById(Long id);
}
