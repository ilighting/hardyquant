package com.ruoyi.quant.trade.crud.domain;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 回测订单分析图对象 dfcf_trade_view
 *
 * @author ruoyi
 * @date 2023-11-09
 */
@Data
public class DfcfTradeView extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 代码 */
    @Excel(name = "代码")
    private String code;

    private String name;

    private String market;

    /** 最大收益 */
    @Excel(name = "最大收益")
    private Double maxProfit;

    /** 最大回撤 */
    @Excel(name = "最大回撤")
    private Double maxCut;

    /** 夏普率 */
    @Excel(name = "夏普率")
    private Double sharp;

    /** 交易次数 */
    @Excel(name = "交易次数")
    private Integer tradeTimes;

    /** 手续费 */
    @Excel(name = "手续费")
    private Double feeCost;

    /** 最终余额 */
    @Excel(name = "最终余额")
    private BigDecimal finalBalance;

    /** 起始额度 */
    @Excel(name = "起始资金")
    private Double openBalance;

    /** 最大额度 */
    @Excel(name = "最大余额")
    private BigDecimal maxBalance;

    /** 最小额度 */
    @Excel(name = "最小余额")
    private BigDecimal minBalance;

    /** 订单记录信息 */
    private List<DfcfTradeOrder> dfcfTradeOrderList;
}
