package com.ruoyi.quant.trade.api.request;

public class GetStockListRequest extends BaseTradeRequest {

    public GetStockListRequest(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.GetStockList;
    }

}
