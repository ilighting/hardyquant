package com.ruoyi.quant.trade.api.request;

public class CrQueryCollateralRequest extends BaseQueryRequest {

    public CrQueryCollateralRequest(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return TradeRequestMethod.CrQueryCollateral;
    }

}
