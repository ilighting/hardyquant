package com.ruoyi.quant.trade.crud.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单记录对象 dfcf_trade_order
 * 
 * @author ruoyi
 * @date 2023-11-09
 */
public class DfcfTradeOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 买卖方向 */
    @Excel(name = "买卖方向")
    private String direction;

    /** 账号 */
    @Excel(name = "账号")
    private String accountId;

    /** 代码 */
    @Excel(name = "代码")
    private String code;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 持仓头寸 */
    @Excel(name = "持仓头寸")
    private Long position;

    /** 单位 */
    @Excel(name = "单位")
    private Long unit;

    /** 市场 */
    @Excel(name = "市场")
    private String market;

    /** 版本号 */
    @Excel(name = "版本号")
    private String version;

    /** 交易日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "交易日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date tradeDate;

    /** 父表Id */
    @Excel(name = "父表Id")
    private Long parentId;

    /** 开始仓位 */
    @Excel(name = "开始仓位")
    private BigDecimal openBalance;

    /** 余额 */
    @Excel(name = "余额")
    private BigDecimal balance;

    /** 持仓价值 */
    @Excel(name = "持仓价值")
    private BigDecimal positionBalance;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDirection(String direction) 
    {
        this.direction = direction;
    }

    public String getDirection() 
    {
        return direction;
    }
    public void setAccountId(String accountId) 
    {
        this.accountId = accountId;
    }

    public String getAccountId() 
    {
        return accountId;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setPosition(Long position) 
    {
        this.position = position;
    }

    public Long getPosition() 
    {
        return position;
    }
    public void setUnit(Long unit) 
    {
        this.unit = unit;
    }

    public Long getUnit() 
    {
        return unit;
    }
    public void setMarket(String market) 
    {
        this.market = market;
    }

    public String getMarket() 
    {
        return market;
    }
    public void setVersion(String version) 
    {
        this.version = version;
    }

    public String getVersion() 
    {
        return version;
    }
    public void setTradeDate(Date tradeDate) 
    {
        this.tradeDate = tradeDate;
    }

    public Date getTradeDate() 
    {
        return tradeDate;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setOpenBalance(BigDecimal openBalance) 
    {
        this.openBalance = openBalance;
    }

    public BigDecimal getOpenBalance() 
    {
        return openBalance;
    }
    public void setBalance(BigDecimal balance) 
    {
        this.balance = balance;
    }

    public BigDecimal getBalance() 
    {
        return balance;
    }
    public void setPositionBalance(BigDecimal positionBalance) 
    {
        this.positionBalance = positionBalance;
    }

    public BigDecimal getPositionBalance() 
    {
        return positionBalance;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("direction", getDirection())
            .append("accountId", getAccountId())
            .append("code", getCode())
            .append("name", getName())
            .append("price", getPrice())
            .append("position", getPosition())
            .append("unit", getUnit())
            .append("market", getMarket())
            .append("version", getVersion())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("tradeDate", getTradeDate())
            .append("parentId", getParentId())
            .append("openBalance", getOpenBalance())
            .append("balance", getBalance())
            .append("positionBalance", getPositionBalance())
            .toString();
    }
}
