package com.ruoyi.quant.trade.api.request;

public class CrGetRzrqAssertsRequest extends BaseTradeRequest {

    private String hblx = "RMB";

    public CrGetRzrqAssertsRequest(String userId) {
        super(userId);
    }

    public String getHblx() {
        return hblx;
    }

    public void setHblx(String hblx) {
        this.hblx = hblx;
    }

    @Override
    public TradeRequestMethod getMethod() {
        return TradeRequestMethod.CrGetRzrqAsserts;
    }

    @Override
    public int responseVersion() {
        return VERSION_DATA_OBJ;
    }

}
