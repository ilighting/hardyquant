package com.ruoyi.quant.trade.api.request;

public class CrGetHisOrdersDataRequest extends GetHisOrdersDataRequest {

    public CrGetHisOrdersDataRequest(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.CrGetHisOrdersData;
    }

}
