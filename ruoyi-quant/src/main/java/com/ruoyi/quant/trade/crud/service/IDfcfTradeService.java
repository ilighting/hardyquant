package com.ruoyi.quant.trade.crud.service;

import java.util.List;

import com.ruoyi.quant.trade.api.TradeResultVo;
import com.ruoyi.quant.trade.api.request.BaseTradeRequest;
import com.ruoyi.quant.trade.api.response.*;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeUser;
import com.ruoyi.quant.trade.model.po.TradeMethod;

/**
 * 东方财富交易界面Service接口
 *
 * @author ruoyi
 * @date 2023-10-29
 */
public interface IDfcfTradeService
{
    /**
     * 查询东方财富交易界面
     *
     * @param accountId 东方财富交易界面主键
     * @return 东方财富交易界面
     */
    public DfcfTradeUser selectDfcfTradeByAccountId(String accountId);

    /**
     * 查询东方财富交易界面列表
     *
     * @param dfcfTradeUser 东方财富交易界面
     * @return 东方财富交易界面集合
     */
    public List<DfcfTradeUser> selectDfcfTradeList(DfcfTradeUser dfcfTradeUser);

    /**
     * 新增东方财富交易界面
     *
     * @param dfcfTradeUser 东方财富交易界面
     * @return 结果
     */
    public int insertDfcfTrade(DfcfTradeUser dfcfTradeUser);

    /**
     * 修改东方财富交易界面
     *
     * @param dfcfTradeUser 东方财富交易界面
     * @return 结果
     */
    public int updateDfcfTrade(DfcfTradeUser dfcfTradeUser);

    /**
     * 批量删除东方财富交易界面
     *
     * @param accountIds 需要删除的东方财富交易界面主键集合
     * @return 结果
     */
    public int deleteDfcfTradeByAccountIds(String accountIds);

    /**
     * 删除东方财富交易界面信息
     *
     * @param accountId 东方财富交易界面主键
     * @return 结果
     */
    public int deleteDfcfTradeByAccountId(String accountId);

	String loginDfcfApi(DfcfTradeUser dfcfTradeUser);

    TradeResultVo<RevokeResponse> revoke(DfcfTradeUser dfcfTradeUser);

    TradeResultVo<GetStockListResponse> userPositionList(DfcfTradeUser dfcfTradeUser);

    TradeResultVo<SubmitResponse> submit(DfcfTradeUser dfcfTradeUser);

    TradeResultVo<GetAssetsResponse> userAssetsList(DfcfTradeUser dfcfTradeUser);
    TradeResultVo<GetOrdersDataResponse> userOrdersList(DfcfTradeUser dfcfTradeUser);

    TradeResultVo<GetDealDataResponse> userDealList(DfcfTradeUser dfcfTradeUser);

    TradeResultVo<GetHisDealDataResponse> userHisDealData(DfcfTradeUser dfcfTradeUser);

    TradeResultVo<GetHisOrdersDataResponse> userHisOrders(DfcfTradeUser dfcfTradeUser);

    TradeResultVo<GetCanBuyNewStockListV3Response> newIpoList(DfcfTradeUser dfcfTradeUser);

    TradeResultVo<GetConvertibleBondListV2Response> newIpoList2(DfcfTradeUser dfcfTradeUser);

    TradeResultVo<SubmitBatTradeV2Response> batchIpo(DfcfTradeUser dfcfTradeUser);

    TradeMethod getTradeMethodByName(BaseTradeRequest.TradeRequestMethod value);

    void refreshLogin(String accountId);
}
