package com.ruoyi.quant.trade.crud.mapper;

import java.util.List;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeView;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * 回测订单分析图Mapper接口
 *
 * @author ruoyi
 * @date 2023-11-09
 */
@Mapper

public interface DfcfTradeViewMapper
{
    /**
     * 查询回测订单分析图
     *
     * @param id 回测订单分析图主键
     * @return 回测订单分析图
     */
    public DfcfTradeView selectDfcfTradeViewById(Long id);

    /**
     * 查询回测订单分析图列表
     *
     * @param dfcfTradeView 回测订单分析图
     * @return 回测订单分析图集合
     */
    public List<DfcfTradeView> selectDfcfTradeViewList(DfcfTradeView dfcfTradeView);

    /**
     * 新增回测订单分析图
     *
     * @param dfcfTradeView 回测订单分析图
     * @return 结果
     */
    public int insertDfcfTradeView(DfcfTradeView dfcfTradeView);

    /**
     * 修改回测订单分析图
     *
     * @param dfcfTradeView 回测订单分析图
     * @return 结果
     */
    public int updateDfcfTradeView(DfcfTradeView dfcfTradeView);

    /**
     * 删除回测订单分析图
     *
     * @param id 回测订单分析图主键
     * @return 结果
     */
    public int deleteDfcfTradeViewById(Long id);

    /**
     * 批量删除回测订单分析图
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDfcfTradeViewByIds(String[] ids);

    /**
     * 批量删除订单记录
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDfcfTradeOrderByParentIds(String[] ids);

    /**
     * 批量新增订单记录
     *
     * @param dfcfTradeOrderList 订单记录列表
     * @return 结果
     */
    public int batchDfcfTradeOrder(List<DfcfTradeOrder> dfcfTradeOrderList);


    /**
     * 通过回测订单分析图主键删除订单记录信息
     *
     * @param id 回测订单分析图ID
     * @return 结果
     */
    public int deleteDfcfTradeOrderByParentId(Long id);
}
