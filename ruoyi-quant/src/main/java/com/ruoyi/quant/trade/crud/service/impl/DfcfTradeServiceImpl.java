package com.ruoyi.quant.trade.crud.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.ruoyi.common.events.LoginEvent;
import com.ruoyi.quant.trade.api.TradeResultVo;
import com.ruoyi.quant.trade.api.request.*;
import com.ruoyi.quant.trade.api.response.*;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeUser;
import com.ruoyi.quant.trade.model.po.TradeMethod;
import com.ruoyi.quant.trade.model.vo.CommonResponse;
import com.ruoyi.quant.trade.services.TradeApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.quant.trade.crud.mapper.DfcfTradeMapper;
import com.ruoyi.quant.trade.crud.service.IDfcfTradeService;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.events.EventPublisher;

/**
 * 东方财富交易界面Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-29
 */
@Service
public class DfcfTradeServiceImpl implements IDfcfTradeService {

    private static final Logger log = LoggerFactory.getLogger(DfcfTradeServiceImpl.class);
    @Autowired
    private DfcfTradeMapper dfcfTradeMapper;

    @Autowired
    private TradeApiService tradeApiService;

    @Autowired
    EventPublisher publisher;

    /**
     * 查询东方财富交易界面
     *
     * @param accountId 东方财富交易界面主键
     * @return 东方财富交易界面
     */
    @Override
    public DfcfTradeUser selectDfcfTradeByAccountId(String accountId) {
        return dfcfTradeMapper.selectDfcfTradeByAccountId(accountId);
    }

    /**
     * 查询东方财富交易界面列表
     *
     * @param dfcfTradeUser 东方财富交易界面
     * @return 东方财富交易界面
     */
    @Override
    public List<DfcfTradeUser> selectDfcfTradeList(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeMapper.selectDfcfTradeList(dfcfTradeUser);
    }

    /**
     * 新增东方财富交易界面
     *
     * @param dfcfTradeUser 东方财富交易界面
     * @return 结果
     */
    @Override
    public int insertDfcfTrade(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeMapper.insertDfcfTrade(dfcfTradeUser);
    }

    /**
     * 修改东方财富交易界面
     *
     * @param dfcfTradeUser 东方财富交易界面
     * @return 结果
     */
    @Override
    public int updateDfcfTrade(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeMapper.updateDfcfTrade(dfcfTradeUser);
    }

    public int updateOrSaveDfcfTrade(DfcfTradeUser dfcfTradeUser) {
        Long id = dfcfTradeUser.getId();
        if (Objects.isNull(id)) {
            dfcfTradeUser.setId(IdWorker.getId());
            return insertDfcfTrade(dfcfTradeUser);
        } else {
            return updateDfcfTrade(dfcfTradeUser);
        }
    }

    /**
     * 批量删除东方财富交易界面
     *
     * @param accountIds 需要删除的东方财富交易界面主键
     * @return 结果
     */
    @Override
    public int deleteDfcfTradeByAccountIds(String accountIds) {
        return dfcfTradeMapper.deleteDfcfTradeByAccountIds(Convert.toStrArray(accountIds));
    }

    /**
     * 删除东方财富交易界面信息
     *
     * @param accountId 东方财富交易界面主键
     * @return 结果
     */
    @Override
    public int deleteDfcfTradeByAccountId(String accountId) {
        return dfcfTradeMapper.deleteDfcfTradeByAccountId(accountId);
    }

    @Override
    public String loginDfcfApi(DfcfTradeUser dfcfTradeUser) {
        AuthenticationRequest request = new AuthenticationRequest(dfcfTradeUser.getAccountId());
        request.setPassword(dfcfTradeUser.getPassword());
        request.setIdentifyCode(dfcfTradeUser.getRandomCode());
        request.setRandNumber(dfcfTradeUser.getRandomNum());

        TradeResultVo<AuthenticationResponse> resultVo = tradeApiService.authentication(request, dfcfTradeUser);
        if (resultVo.success()) {
            AuthenticationResponse response = resultVo.getData().get(0);
            dfcfTradeUser.setUserId(request.getUserId());
            dfcfTradeUser.setCookie(response.getCookie());
            dfcfTradeUser.setValidateKey(response.getValidateKey());
            updateOrSaveDfcfTrade(dfcfTradeUser);
            resultVo.setMessage(CommonResponse.DEFAULT_MESSAGE_SUCCESS);
        }
        return resultVo.getMessage();
    }

    @Override
    public TradeResultVo<GetStockListResponse> userPositionList(DfcfTradeUser dfcfTradeUser) {
        GetStockListRequest request = new GetStockListRequest(dfcfTradeUser.getAccountId());
        TradeResultVo<GetStockListResponse> stockList = tradeApiService.getStockList(request);
        if (needReLogin(stockList, dfcfTradeUser)) {
            return userPositionList(dfcfTradeUser);
        }
        return stockList;
    }

    @Override
    public TradeResultVo<RevokeResponse> revoke(DfcfTradeUser dfcfTradeUser) {
        RevokeRequest request = new RevokeRequest(dfcfTradeUser.getAccountId());
        TradeResultVo<RevokeResponse> stockList = tradeApiService.revoke(request);
        if (needReLogin(stockList, dfcfTradeUser)) {
            return revoke(dfcfTradeUser);
        }
        return stockList;
    }

    @Override
    public TradeResultVo<SubmitResponse> submit(DfcfTradeUser dfcfTradeUser) {
        SubmitRequest request = new SubmitRequest(dfcfTradeUser.getAccountId());

        TradeResultVo<SubmitResponse> result = tradeApiService.submit(request);
        if (needReLogin(result, dfcfTradeUser)) {
            return submit(dfcfTradeUser);
        }
        return result;
    }

    @Override
    public TradeResultVo<GetAssetsResponse> userAssetsList(DfcfTradeUser dfcfTradeUser) {
        GetAssetsRequest request = new GetAssetsRequest(dfcfTradeUser.getAccountId());
        TradeResultVo<GetAssetsResponse> asserts = tradeApiService.getAsserts(request);
        if (needReLogin(asserts, dfcfTradeUser)) {
            return userAssetsList(dfcfTradeUser);
        }
        return asserts;
    }

    @Override
    public TradeResultVo<GetOrdersDataResponse> userOrdersList(DfcfTradeUser dfcfTradeUser) {
        GetOrdersDataRequest request = new GetOrdersDataRequest(dfcfTradeUser.getAccountId());
        TradeResultVo<GetOrdersDataResponse> asserts = tradeApiService.getOrdersData(request);
        if (needReLogin(asserts, dfcfTradeUser)) {
            return userOrdersList(dfcfTradeUser);
        }
        return asserts;
    }

    @Override
    public TradeResultVo<GetDealDataResponse> userDealList(DfcfTradeUser dfcfTradeUser) {
        GetDealDataRequest request = new GetDealDataRequest(dfcfTradeUser.getAccountId());
        TradeResultVo<GetDealDataResponse> asserts = tradeApiService.getDealData(request);
        if (needReLogin(asserts, dfcfTradeUser)) {
            return userDealList(dfcfTradeUser);
        }
        return asserts;
    }

    @Override
    public TradeResultVo<GetHisDealDataResponse> userHisDealData(DfcfTradeUser dfcfTradeUser) {
        GetHisDealDataRequest request = new GetHisDealDataRequest(dfcfTradeUser.getAccountId());
        TradeResultVo<GetHisDealDataResponse> asserts = tradeApiService.getHisDealData(request);
        if (needReLogin(asserts, dfcfTradeUser)) {
            return userHisDealData(dfcfTradeUser);
        }
        return asserts;
    }

    @Override
    public TradeResultVo<GetHisOrdersDataResponse> userHisOrders(DfcfTradeUser dfcfTradeUser) {
        GetHisOrdersDataRequest request = new GetHisOrdersDataRequest(dfcfTradeUser.getAccountId());
        TradeResultVo<GetHisOrdersDataResponse> asserts = tradeApiService.getHisOrdersData(request);
        if (needReLogin(asserts, dfcfTradeUser)) {
            return userHisOrders(dfcfTradeUser);
        }
        return asserts;
    }

    @Override
    public TradeResultVo<GetCanBuyNewStockListV3Response> newIpoList(DfcfTradeUser dfcfTradeUser) {
        GetCanBuyNewStockListV3Request request = new GetCanBuyNewStockListV3Request(dfcfTradeUser.getAccountId());
        TradeResultVo<GetCanBuyNewStockListV3Response> asserts = tradeApiService.getCanBuyNewStockListV3(request);
        if (needReLogin(asserts, dfcfTradeUser)) {
            return newIpoList(dfcfTradeUser);
        }
        return asserts;
    }

    @Override
    public TradeResultVo<GetConvertibleBondListV2Response> newIpoList2(DfcfTradeUser dfcfTradeUser) {
        GetConvertibleBondListV2Request request = new GetConvertibleBondListV2Request(dfcfTradeUser.getAccountId());
        TradeResultVo<GetConvertibleBondListV2Response> asserts = tradeApiService.getConvertibleBondListV2(request);
        if (needReLogin(asserts, dfcfTradeUser)) {
            return newIpoList2(dfcfTradeUser);
        }
        return asserts;
    }

    @Override
    public TradeResultVo<SubmitBatTradeV2Response> batchIpo(DfcfTradeUser dfcfTradeUser) {
        SubmitBatTradeV2Request request = new SubmitBatTradeV2Request(dfcfTradeUser.getAccountId());
        List<SubmitBatTradeV2Request.SubmitData> list = new ArrayList<>();
        request.setList(list);
        TradeResultVo<SubmitBatTradeV2Response> asserts = tradeApiService.submitBatTradeV2(request);
        if (needReLogin(asserts, dfcfTradeUser)) {
            return batchIpo(dfcfTradeUser);
        }
        return asserts;
    }

    /**
     * 重新登录job
     */
    public void sendLoginJob() {
        //todo
    }

    @Override
    public TradeMethod getTradeMethodByName(final BaseTradeRequest.TradeRequestMethod value) {
        TradeMethod tradeMethod = new TradeMethod();
        String apiUrl = value.getApiUrl();
        tradeMethod.setUrl(apiUrl);
        tradeMethod.setName(value.getDesc());
        tradeMethod.setState(1);
        return tradeMethod;
    }

    @Override
    public void refreshLogin(String accountId) {

//        DfcfTradeUser dfcfTradeUser = selectDfcfTradeByAccountId(accountId);
//        String message = loginDfcfApi(dfcfTradeUser);
//        log.info(message);


    }

    public Boolean needReLogin(TradeResultVo<?> vo, DfcfTradeUser dfcfTradeUser) {
        if (null != vo.getMessage() && vo.getMessage().contains("会话已超时")) {
            LoginEvent jobEvent = new LoginEvent();
            jobEvent.setJobName("东方财富，重新登录!");
            jobEvent.setContent("东方财富，服务器的" + vo.getMessage());
            jobEvent.setAccountId(dfcfTradeUser.getAccountId());
            publisher.publishEvent(jobEvent);
            return true;
        }
        return false;
    }
}
