package com.ruoyi.quant.trade.crud.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeOrder;
import com.ruoyi.quant.trade.crud.mapper.DfcfTradeViewMapper;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeView;
import com.ruoyi.quant.trade.crud.service.IDfcfTradeViewService;
import com.ruoyi.common.core.text.Convert;

/**
 * 回测订单分析图Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-09
 */
@Service
public class DfcfTradeViewServiceImpl implements IDfcfTradeViewService 
{
    @Autowired
    private DfcfTradeViewMapper dfcfTradeViewMapper;

    /**
     * 查询回测订单分析图
     * 
     * @param id 回测订单分析图主键
     * @return 回测订单分析图
     */
    @Override
    public DfcfTradeView selectDfcfTradeViewById(Long id)
    {
        return dfcfTradeViewMapper.selectDfcfTradeViewById(id);
    }

    /**
     * 查询回测订单分析图列表
     * 
     * @param dfcfTradeView 回测订单分析图
     * @return 回测订单分析图
     */
    @Override
    public List<DfcfTradeView> selectDfcfTradeViewList(DfcfTradeView dfcfTradeView)
    {
        return dfcfTradeViewMapper.selectDfcfTradeViewList(dfcfTradeView);
    }

    /**
     * 新增回测订单分析图
     * 
     * @param dfcfTradeView 回测订单分析图
     * @return 结果
     */
    @Transactional
    @Override
    public int insertDfcfTradeView(DfcfTradeView dfcfTradeView)
    {
        dfcfTradeView.setCreateTime(DateUtils.getNowDate());
        int rows = dfcfTradeViewMapper.insertDfcfTradeView(dfcfTradeView);
        insertDfcfTradeOrder(dfcfTradeView);
        return rows;
    }

    /**
     * 修改回测订单分析图
     * 
     * @param dfcfTradeView 回测订单分析图
     * @return 结果
     */
    @Transactional
    @Override
    public int updateDfcfTradeView(DfcfTradeView dfcfTradeView)
    {
        dfcfTradeView.setUpdateTime(DateUtils.getNowDate());
        dfcfTradeViewMapper.deleteDfcfTradeOrderByParentId(dfcfTradeView.getId());
        insertDfcfTradeOrder(dfcfTradeView);
        return dfcfTradeViewMapper.updateDfcfTradeView(dfcfTradeView);
    }

    /**
     * 批量删除回测订单分析图
     * 
     * @param ids 需要删除的回测订单分析图主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteDfcfTradeViewByIds(String ids)
    {
        dfcfTradeViewMapper.deleteDfcfTradeOrderByParentIds(Convert.toStrArray(ids));
        return dfcfTradeViewMapper.deleteDfcfTradeViewByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除回测订单分析图信息
     * 
     * @param id 回测订单分析图主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteDfcfTradeViewById(Long id)
    {
        dfcfTradeViewMapper.deleteDfcfTradeOrderByParentId(id);
        return dfcfTradeViewMapper.deleteDfcfTradeViewById(id);
    }

    /**
     * 新增订单记录信息
     * 
     * @param dfcfTradeView 回测订单分析图对象
     */
    public void insertDfcfTradeOrder(DfcfTradeView dfcfTradeView)
    {
        List<DfcfTradeOrder> dfcfTradeOrderList = dfcfTradeView.getDfcfTradeOrderList();
        Long id = dfcfTradeView.getId();
        if (StringUtils.isNotNull(dfcfTradeOrderList))
        {
            List<DfcfTradeOrder> list = new ArrayList<DfcfTradeOrder>();
            for (DfcfTradeOrder dfcfTradeOrder : dfcfTradeOrderList)
            {
                dfcfTradeOrder.setParentId(id);
                list.add(dfcfTradeOrder);
            }
            if (list.size() > 0)
            {
                dfcfTradeViewMapper.batchDfcfTradeOrder(list);
            }
        }
    }
}
