package com.ruoyi.quant.trade.domains;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountVo {

    private BigDecimal totalAmount;
    private BigDecimal availableAmount;
    private BigDecimal withdrawableAmount;
    private BigDecimal frozenAmount;
}
