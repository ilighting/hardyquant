package com.ruoyi.quant.trade.services;

import com.ruoyi.quant.mg.service.IMongoService;
import com.ruoyi.quant.trade.model.po.TradeUser;

import java.util.List;

public interface ITradeUserService {
	TradeUser getById(String id);

	void update(TradeUser tradeUser);

	List<TradeUser> getList();
}
