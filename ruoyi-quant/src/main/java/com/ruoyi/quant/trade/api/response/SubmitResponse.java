package com.ruoyi.quant.trade.api.response;

public class SubmitResponse extends BaseTradeResponse {

    private String Wtbh;

    public String getWtbh() {
        return Wtbh;
    }

    public void setWtbh(String wtbh) {
        Wtbh = wtbh;
    }

}
