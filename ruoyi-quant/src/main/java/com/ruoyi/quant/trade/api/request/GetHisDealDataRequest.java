package com.ruoyi.quant.trade.api.request;

public class GetHisDealDataRequest extends BaseTradeRequest {

    private String st;
    private String et;

    public GetHisDealDataRequest(String userId) {
        super(userId);
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public String getEt() {
        return et;
    }

    public void setEt(String et) {
        this.et = et;
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.GetHisDealData;
    }

}
