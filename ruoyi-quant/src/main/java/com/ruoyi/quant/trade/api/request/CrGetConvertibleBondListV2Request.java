package com.ruoyi.quant.trade.api.request;

public class CrGetConvertibleBondListV2Request extends GetConvertibleBondListV2Request {

    public CrGetConvertibleBondListV2Request(String userId) {
        super(userId);
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.CrGetConvertibleBondListV2;
    }

}
