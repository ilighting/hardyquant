package com.ruoyi.quant.trade.api.request;

public class RevokeRequest extends BaseTradeRequest {

    private String revokes;

    public RevokeRequest(String userId) {
        super(userId);
    }

    public String getRevokes() {
        return revokes;
    }

    public void setRevokes(String revokes) {
        this.revokes = revokes;
    }

    @Override
    public TradeRequestMethod getMethod() {
        return BaseTradeRequest.TradeRequestMethod.Revoke;
    }

    @Override
    public int responseVersion() {
        return VERSION_MSG;
    }

}
