package com.ruoyi.quant.trade.api.request;

import java.util.List;

public abstract class BaseTradeListRequest extends BaseTradeRequest {

    protected BaseTradeListRequest(String userId) {
        super(userId);
    }

    public abstract List<?> getList();

}
