package com.ruoyi.quant.basedata.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.quant.basedata.domain.StockCode;
import org.apache.ibatis.annotations.Mapper;

/**
 * 股票列表Mapper接口
 * 
 * @author ruoyi
 * @date 2023-11-28
 */
@Mapper
public interface StockCodeMapper extends BaseMapper<StockCode>
{
    /**
     * 查询股票列表
     * 
     * @param id 股票列表主键
     * @return 股票列表
     */
    public StockCode selectStockCodeById(Long id);

    /**
     * 查询股票列表列表
     * 
     * @param stockCode 股票列表
     * @return 股票列表集合
     */
    public List<StockCode> selectStockCodeList(StockCode stockCode);

    /**
     * 新增股票列表
     * 
     * @param stockCode 股票列表
     * @return 结果
     */
    public int insertStockCode(StockCode stockCode);

    /**
     * 修改股票列表
     * 
     * @param stockCode 股票列表
     * @return 结果
     */
    public int updateStockCode(StockCode stockCode);

    /**
     * 删除股票列表
     * 
     * @param id 股票列表主键
     * @return 结果
     */
    public int deleteStockCodeById(Long id);

    /**
     * 批量删除股票列表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockCodeByIds(String[] ids);
}
