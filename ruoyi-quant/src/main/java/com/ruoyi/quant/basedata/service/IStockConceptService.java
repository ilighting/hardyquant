package com.ruoyi.quant.basedata.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.quant.basedata.domain.StockConcept;

/**
 * 板块信息Service接口
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
public interface IStockConceptService extends IService<StockConcept>
{
    /**
     * 查询板块信息
     * 
     * @param id 板块信息主键
     * @return 板块信息
     */
    public StockConcept selectStockConceptById(Long id);

    /**
     * 查询板块信息列表
     * 
     * @param stockConcept 板块信息
     * @return 板块信息集合
     */
    public List<StockConcept> selectStockConceptList(StockConcept stockConcept);

    /**
     * 新增板块信息
     * 
     * @param stockConcept 板块信息
     * @return 结果
     */
    public int insertStockConcept(StockConcept stockConcept);

    /**
     * 修改板块信息
     * 
     * @param stockConcept 板块信息
     * @return 结果
     */
    public int updateStockConcept(StockConcept stockConcept);

    /**
     * 批量删除板块信息
     * 
     * @param ids 需要删除的板块信息主键集合
     * @return 结果
     */
    public int deleteStockConceptByIds(String ids);

    /**
     * 删除板块信息信息
     * 
     * @param id 板块信息主键
     * @return 结果
     */
    public int deleteStockConceptById(Long id);
}
