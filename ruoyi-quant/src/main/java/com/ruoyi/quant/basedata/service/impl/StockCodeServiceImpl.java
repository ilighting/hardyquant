package com.ruoyi.quant.basedata.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.quant.basedata.mapper.StockCodeMapper;
import com.ruoyi.quant.basedata.domain.StockCode;
import com.ruoyi.quant.basedata.service.IStockCodeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 股票列表Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-28
 */
@Service
public class StockCodeServiceImpl extends ServiceImpl<StockCodeMapper,StockCode> implements IStockCodeService
{
    @Autowired
    private StockCodeMapper stockCodeMapper;

    /**
     * 查询股票列表
     * 
     * @param id 股票列表主键
     * @return 股票列表
     */
    @Override
    public StockCode selectStockCodeById(Long id)
    {
        return stockCodeMapper.selectStockCodeById(id);
    }

    /**
     * 查询股票列表列表
     * 
     * @param stockCode 股票列表
     * @return 股票列表
     */
    @Override
    public List<StockCode> selectStockCodeList(StockCode stockCode)
    {
        return stockCodeMapper.selectStockCodeList(stockCode);
    }

    /**
     * 新增股票列表
     * 
     * @param stockCode 股票列表
     * @return 结果
     */
    @Override
    public int insertStockCode(StockCode stockCode)
    {
        stockCode.setCreateTime(DateUtils.getNowDate());
        return stockCodeMapper.insertStockCode(stockCode);
    }

    /**
     * 修改股票列表
     * 
     * @param stockCode 股票列表
     * @return 结果
     */
    @Override
    public int updateStockCode(StockCode stockCode)
    {
        stockCode.setUpdateTime(DateUtils.getNowDate());
        return stockCodeMapper.updateStockCode(stockCode);
    }

    /**
     * 批量删除股票列表
     * 
     * @param ids 需要删除的股票列表主键
     * @return 结果
     */
    @Override
    public int deleteStockCodeByIds(String ids)
    {
        return stockCodeMapper.deleteStockCodeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除股票列表信息
     * 
     * @param id 股票列表主键
     * @return 结果
     */
    @Override
    public int deleteStockCodeById(Long id)
    {
        return stockCodeMapper.deleteStockCodeById(id);
    }
}
