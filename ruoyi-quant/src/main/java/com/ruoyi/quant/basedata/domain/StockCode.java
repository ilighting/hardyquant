package com.ruoyi.quant.basedata.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 股票列表对象 stock_code
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
public class StockCode extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 编码 */
    @Excel(name = "编码")
    private String code;

    @TableField(exist = false)
    private String codes;
    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 小数点 */
    @Excel(name = "小数点")
    private String decimalPoint;

    /** 市场 */
    @Excel(name = "市场")
    private String market;


    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDecimalPoint(String decimalPoint) 
    {
        this.decimalPoint = decimalPoint;
    }

    public String getDecimalPoint() 
    {
        return decimalPoint;
    }
    public void setMarket(String market) 
    {
        this.market = market;
    }

    public String getMarket() 
    {
        return market;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("price", getPrice())
            .append("name", getName())
            .append("decimalPoint", getDecimalPoint())
            .append("market", getMarket())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
