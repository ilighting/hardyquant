package com.ruoyi.quant.basedata.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 板块信息对象 stock_concept
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
public class StockConcept extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 编码 */
    @Excel(name = "编码")
    private String code;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 等级 */
    @Excel(name = "等级")
    private Integer level;

    /** 代码列表 */
    @Excel(name = "代码列表")
    private String codes;

    /** 分类 */
    @Excel(name = "分类")
    private Integer type;

    /** 股票列表信息 */
    private List<StockCode> stockCodeList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public void setCodes(String codes)
    {
        this.codes = codes;
    }

    public String getCodes() 
    {
        return codes;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<StockCode> getStockCodeList()
    {
        return stockCodeList;
    }

    public void setStockCodeList(List<StockCode> stockCodeList)
    {
        this.stockCodeList = stockCodeList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("level", getLevel())
            .append("codes", getCodes())
            .append("type", getType())
            .append("stockCodeList", getStockCodeList())
            .toString();
    }
}
