package com.ruoyi.quant.basedata.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.quant.basedata.domain.StockConcept;
import com.ruoyi.quant.basedata.domain.StockCode;
import org.apache.ibatis.annotations.Mapper;

/**
 * 板块信息Mapper接口
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
@Mapper
public interface StockConceptMapper extends BaseMapper<StockConcept>
{
    /**
     * 查询板块信息
     * 
     * @param id 板块信息主键
     * @return 板块信息
     */
    public StockConcept selectStockConceptById(Long id);

    /**
     * 查询板块信息列表
     * 
     * @param stockConcept 板块信息
     * @return 板块信息集合
     */
    public List<StockConcept> selectStockConceptList(StockConcept stockConcept);

    /**
     * 新增板块信息
     * 
     * @param stockConcept 板块信息
     * @return 结果
     */
    public int insertStockConcept(StockConcept stockConcept);

    /**
     * 修改板块信息
     * 
     * @param stockConcept 板块信息
     * @return 结果
     */
    public int updateStockConcept(StockConcept stockConcept);

    /**
     * 删除板块信息
     * 
     * @param id 板块信息主键
     * @return 结果
     */
    public int deleteStockConceptById(Long id);

    /**
     * 批量删除板块信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockConceptByIds(String[] ids);

    /**
     * 批量删除股票列表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockCodeByCodes(String[] ids);
    
    /**
     * 批量新增股票列表
     * 
     * @param stockCodeList 股票列表列表
     * @return 结果
     */
    public int batchStockCode(List<StockCode> stockCodeList);
    

    /**
     * 通过板块信息主键删除股票列表信息
     * 
     * @param id 板块信息ID
     * @return 结果
     */
    public int deleteStockCodeByCode(Long id);
}
