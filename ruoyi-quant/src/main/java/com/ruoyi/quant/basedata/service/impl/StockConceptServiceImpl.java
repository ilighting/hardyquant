package com.ruoyi.quant.basedata.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.quant.basedata.domain.StockConcept;
import com.ruoyi.quant.basedata.mapper.StockConceptMapper;
import com.ruoyi.quant.basedata.service.IStockConceptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 板块信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
@Service
public class StockConceptServiceImpl extends ServiceImpl<StockConceptMapper,StockConcept> implements IStockConceptService
{
    @Autowired
    private StockConceptMapper stockConceptMapper;

    /**
     * 查询板块信息
     * 
     * @param id 板块信息主键
     * @return 板块信息
     */
    @Override
    public StockConcept selectStockConceptById(Long id)
    {
        return stockConceptMapper.selectStockConceptById(id);
    }

    /**
     * 查询板块信息列表
     * 
     * @param stockConcept 板块信息
     * @return 板块信息
     */
    @Override
    public List<StockConcept> selectStockConceptList(StockConcept stockConcept)
    {
        return stockConceptMapper.selectStockConceptList(stockConcept);
    }

    /**
     * 新增板块信息
     * 
     * @param stockConcept 板块信息
     * @return 结果
     */
    @Transactional
    @Override
    public int insertStockConcept(StockConcept stockConcept)
    {
        int rows = stockConceptMapper.insertStockConcept(stockConcept);
//        insertStockCode(stockConcept);
        return rows;
    }

    /**
     * 修改板块信息
     * 
     * @param stockConcept 板块信息
     * @return 结果
     */
    @Transactional
    @Override
    public int updateStockConcept(StockConcept stockConcept)
    {
        stockConceptMapper.deleteStockCodeByCode(stockConcept.getId());
//        insertStockCode(stockConcept);
        return stockConceptMapper.updateStockConcept(stockConcept);
    }

    /**
     * 批量删除板块信息
     * 
     * @param ids 需要删除的板块信息主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteStockConceptByIds(String ids)
    {
        stockConceptMapper.deleteStockCodeByCodes(Convert.toStrArray(ids));
        return stockConceptMapper.deleteStockConceptByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除板块信息信息
     * 
     * @param id 板块信息主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteStockConceptById(Long id)
    {
        stockConceptMapper.deleteStockCodeByCode(id);
        return stockConceptMapper.deleteStockConceptById(id);
    }

    /**
     * 新增股票列表信息
     * 
     * @param stockConcept 板块信息对象
     */
//    public void insertStockCode(StockConcept stockConcept)
//    {
//        List<StockCode> stockCodeList = stockConcept.getStockCodeList();
//        Long id = stockConcept.getId();
//        if (StringUtils.isNotNull(stockCodeList))
//        {
//            List<StockCode> list = new ArrayList<StockCode>();
//            for (StockCode stockCode : stockCodeList)
//            {
//                stockCode.setCode(id);
//                list.add(stockCode);
//            }
//            if (list.size() > 0)
//            {
//                stockConceptMapper.batchStockCode(list);
//            }
//        }
//    }
}
