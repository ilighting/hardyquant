package com.ruoyi.quant.basedata.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.quant.basedata.domain.StockCode;

/**
 * 股票列表Service接口
 * 
 * @author ruoyi
 * @date 2023-11-28
 */
public interface IStockCodeService extends IService<StockCode>
{
    /**
     * 查询股票列表
     * 
     * @param id 股票列表主键
     * @return 股票列表
     */
    public StockCode selectStockCodeById(Long id);

    /**
     * 查询股票列表列表
     * 
     * @param stockCode 股票列表
     * @return 股票列表集合
     */
    public List<StockCode> selectStockCodeList(StockCode stockCode);

    /**
     * 新增股票列表
     * 
     * @param stockCode 股票列表
     * @return 结果
     */
    public int insertStockCode(StockCode stockCode);

    /**
     * 修改股票列表
     * 
     * @param stockCode 股票列表
     * @return 结果
     */
    public int updateStockCode(StockCode stockCode);

    /**
     * 批量删除股票列表
     * 
     * @param ids 需要删除的股票列表主键集合
     * @return 结果
     */
    public int deleteStockCodeByIds(String ids);

    /**
     * 删除股票列表信息
     * 
     * @param id 股票列表主键
     * @return 结果
     */
    public int deleteStockCodeById(Long id);
}
