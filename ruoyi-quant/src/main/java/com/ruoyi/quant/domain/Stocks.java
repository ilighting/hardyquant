package com.ruoyi.quant.domain;


import com.ruoyi.common.domain.PageModel;
import lombok.Data;

@Data
public class Stocks extends PageModel {


	private String id;

	private String code;
	private String price;
	private String name;
	private Integer decimalPoint;
	private String market;

	@Override
	protected String setTableDesc() {
		return "股票列表";
	}
}
