package com.ruoyi.quant.domain.models;

import lombok.Data;

@Data
public class OtherTaModel extends BaseModel {


	public String getStockCode(){
		if(null!=this.getCode()){
			String s = this.getCode().split(",")[0];
			return s;
		}
		return "";
	}

	@Override
	protected String setTableDesc() {
		return "其他合集";
	}
}
