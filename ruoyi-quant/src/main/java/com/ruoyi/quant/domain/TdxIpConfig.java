package com.ruoyi.quant.domain;


import lombok.Data;

import java.io.Serializable;
import java.util.StringJoiner;
@Data
public class TdxIpConfig implements Serializable {

		private String id;

		private String ip;
		private Integer port;
		private String name;

		public TdxIpConfig() {
		}

		public TdxIpConfig(final String ip, final Integer port, final String name) {
			this.ip = ip;
			this.port = port;
			this.name = name;
		}

		public String getIp() {
			return ip;
		}

		public void setIp(final String ip) {
			this.ip = ip;
		}

		public Integer getPort() {
			return port;
		}

		public void setPort(final Integer port) {
			this.port = port;
		}

		public String getName() {
			return name;
		}

		public void setName(final String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return new StringJoiner(", ", TdxIpConfig.class.getSimpleName() + "[", "]")
					.add("ip='" + ip + "'")
					.add("port='" + port + "'")
					.add("name='" + name + "'")
					.toString();
		}
	}
