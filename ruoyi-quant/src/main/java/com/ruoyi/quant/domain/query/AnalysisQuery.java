package com.ruoyi.quant.domain.query;


import com.ruoyi.quant.mg.MgSort;
import com.ruoyi.quant.tdx.TdxCategory;
import com.ruoyi.quant.tdx.Market;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AnalysisQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    private String code;
    private String name;
    private Market market;
    private TdxCategory tdxCategory;
    private Integer count=0;
    private String dateFrom;
    private String dateTo;

    private String date;

    private List<String> talibNames;

    private MgSort mgSort;

}
