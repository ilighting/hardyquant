package com.ruoyi.quant.domain;

import lombok.Data;

@Data
public class TradeModel {

	private Integer duration;
	private Integer tradeDays;
	private String code;
	private String modelType;

}
