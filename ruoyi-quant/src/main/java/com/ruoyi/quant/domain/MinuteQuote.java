package com.ruoyi.quant.domain;

import com.ruoyi.common.domain.PageModel;
import lombok.Data;

@Data
public class MinuteQuote extends PageModel {

    private String id;

    private String code;
    //日期
    private String date;
    //收盘
    private Double close;
    //开盘
    private Double open;
    //最低
    private Double low;
    //最高
    private Double high;
    //成交量
    private Integer vol;
    //成交额
    private Double amt;
    //不知
    private Integer tor;

    @Override
    protected String setTableDesc() {
        return "行情表";
    }

    public String getStockCode(){
        if(null!=this.getCode()) {
            String replace = this.getCode().split(",")[0];
            return replace;
        }
        return "";
    }

}
