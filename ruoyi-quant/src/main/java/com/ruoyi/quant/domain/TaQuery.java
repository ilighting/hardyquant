package com.ruoyi.quant.domain;

import lombok.Data;

@Data
public class TaQuery {
	private String type;
	private String code;
	private String name;
}
