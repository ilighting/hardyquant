package com.ruoyi.quant.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 通达信概念
 */
@Data
public class TdxConcept {

	private String code;
	private String name;
	private Integer level;
	private List<String> codes;
	private String type;
	private List<Stocks> stockList =new ArrayList<>();
}
