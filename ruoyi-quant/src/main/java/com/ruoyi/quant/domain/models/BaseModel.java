package com.ruoyi.quant.domain.models;


import com.ruoyi.common.domain.PageModel;
import lombok.Data;

@Data
public class BaseModel extends PageModel {

	private String id;

	private String type;

	private String code;
	private String date;
	private Double close;

	private Double val;

	private Double minVal;
	private Double maxVal;

	@Override
	protected String setTableDesc() {
		return "base";
	}
}
