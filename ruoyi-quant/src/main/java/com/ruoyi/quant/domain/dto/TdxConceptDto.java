package com.ruoyi.quant.domain.dto;

import com.ruoyi.quant.domain.Stocks;
import lombok.Data;

import java.util.List;

/**
 * 通达信概念
 */
@Data
public class TdxConceptDto {

	private String code;
	private String name;
	private Integer level;
	private List<String> codes;
	private String type;
	private List<Stocks> stockList;
}
