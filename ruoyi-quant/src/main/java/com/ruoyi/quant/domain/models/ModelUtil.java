package com.ruoyi.quant.domain.models;


import com.ruoyi.quant.tdx.TdxCategory;

public class ModelUtil {

	public  static <T extends BaseModel> BaseModel  getModel(TdxCategory category){
		if(category.equals(TdxCategory.day)){
				return new DayTaModel();
		}
		if(category.equals(TdxCategory.m1)){
			return new MinuteTaModel();
		}
		return new OtherTaModel();
	}
}
