package com.ruoyi.quant.tdx;

import com.ruoyi.quant.tdx.domain.Quote;
import com.ruoyi.quant.tdx.domain.TickTransactions;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.List;

public interface TdxClient {

    void setTdxRootDir(String tdxRootDir);


    void setSoTimeout(int soTimeout);

    void setConnectTimeout(int connectTimeout);

    void setSocketAddress(InetSocketAddress address);

    void connect() throws IOException;

    boolean isConnected();

    void close();

    List<Quote> getIndexQuotes(
            TdxCategory tdxCategory,
            Market market,
            String code,
            int start,
            int count
    ) throws IOException;

    List<Quote> getQuotes(
            TdxCategory tdxCategory,
            Market market,
            String code,
            int start,
            int count


    ) throws IOException;

    int getCount(Market market) throws IOException;

    List<StockInfo> getStockList(Market market, int start) throws IOException;

    //全部
    List<StockInfo> getStockList() throws IOException;

    Collection<BlockStock> getBlockInfo(BlockType type) throws IOException;


    List<TimePrice> getTimePrice(Market market, String code) throws IOException;

    List<TimePrice> getHistoryTimePrice(Market market, String code, String date) throws IOException;
    List<TickTransactions> getHistoryTicks(Market market, String code, String date) throws IOException;
}
