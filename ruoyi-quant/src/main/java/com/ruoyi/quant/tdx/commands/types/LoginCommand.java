package com.ruoyi.quant.tdx.commands.types;

import com.ruoyi.quant.tdx.commands.BaseCommand;
import com.ruoyi.quant.tdx.impl.TdxInputStream;
import com.ruoyi.quant.tdx.utils.HexUtils;
import com.ruoyi.quant.tdx.utils.TdxDataOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;

public class LoginCommand extends GroupCommand {
    private static final Log log = LogFactory.getLog(LoginCommand.class);

    public LoginCommand() {
        super(new Cmd1(), new Cmd2(), new Cmd3());
    }

    static class Cmd1 extends BaseCommand {

        @Override
        protected void writeCmd(TdxDataOutputStream out) throws IOException {
            out.write(HexUtils.decodeHex("0c0218930001030003000d0001"));
        }

        @Override
        protected Object onMessage(TdxInputStream inputStream) throws IOException {
            return null;
        }
    }

    static class Cmd2 extends BaseCommand {

        @Override
        protected void writeCmd(TdxDataOutputStream out) throws IOException {
            out.write(HexUtils.decodeHex("0c0218940001030003000d0002"));
        }

        @Override
        protected Object onMessage(TdxInputStream inputStream) throws IOException {

            return null;
        }
    }

    static class Cmd3 extends BaseCommand {

        @Override
        protected void writeCmd(TdxDataOutputStream out) throws IOException {
            out.write(HexUtils.decodeHex(("0c031899000120002000db0fd5d0c9ccd6a4a8af0000008fc22540130000d500c9ccbdf0d7ea00000002")));
        }

        @Override
        protected Object onMessage(TdxInputStream inputStream) throws IOException {
            log.info("通达信登录成功，准备发送指令");
            return null;
        }
    }
    class ExSetupCmd1 extends BaseCommand {

        @Override
        protected void writeCmd(final TdxDataOutputStream outputStream) throws IOException {
            outputStream.write(HexUtils.decodeHex((
                            "01 01 48 65 00 01 52 00 52 00 54 24 1f 32 c6 e5"+
                    "d5 3d fb 41 1f 32 c6 e5 d5 3d fb 41 1f 32 c6 e5"+
                    "d5 3d fb 41 1f 32 c6 e5 d5 3d fb 41 1f 32 c6 e5"+
                    "d5 3d fb 41 1f 32 c6 e5 d5 3d fb 41 1f 32 c6 e5"+
                    "d5 3d fb 41 1f 32 c6 e5 d5 3d fb 41 cc e1 6d ff"+
                    "d5 ba 3f b8 cb c5 7a 05 4f 77 48 ea"            )));

        }

        @Override
        protected Object onMessage(final TdxInputStream inputStream) throws IOException {
            return null;
        }
    }





}
