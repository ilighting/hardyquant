package com.ruoyi.quant.tdx.impl;

public class IpInfo {


    String host;
    int port;
    String name;
    //从存储中加载失败历史数据和成功历史数据
    int successCount;

    public IpInfo(String host, int port, String name) {
        this.host = host;
        this.port = port;
        this.name = name;
    }

    public IpInfo(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(final String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(final int port) {
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(final int successCount) {
        this.successCount = successCount;
    }
}
