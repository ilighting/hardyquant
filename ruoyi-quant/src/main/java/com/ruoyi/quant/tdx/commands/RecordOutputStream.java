package com.ruoyi.quant.tdx.commands;

import com.ruoyi.quant.tdx.utils.TdxDataOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class RecordOutputStream extends TdxDataOutputStream {


    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    public RecordOutputStream(OutputStream stream) {
        super(stream);

    }

    @Override
    public void write(int b) throws IOException {
        super.write(b);
        byteArrayOutputStream.write(b);
    }

    public byte[] toByteArray() {
        return byteArrayOutputStream.toByteArray();
    }
}
