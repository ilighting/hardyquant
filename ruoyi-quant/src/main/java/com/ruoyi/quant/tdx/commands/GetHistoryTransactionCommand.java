package com.ruoyi.quant.tdx.commands;

import com.ruoyi.quant.tdx.Market;
import com.ruoyi.quant.tdx.domain.TickTransactions;
import com.ruoyi.quant.tdx.impl.TdxInputStream;
import com.ruoyi.quant.tdx.utils.TdxDataOutputStream;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class GetHistoryTransactionCommand extends BaseCommand<Object> {

    protected String code;
    protected Market market;
    protected int start;
    protected int count;
    private String date;

    private double lastPrice = 0;

    public GetHistoryTransactionCommand(Market market, String code, int start, int count, String date) {
        this.market = market;
        this.code = code;
        this.date=date;
    }

    @Override
    protected void writeCmd(TdxDataOutputStream outputStream) throws IOException {

        outputStream.writeShort(0x10c);
        outputStream.writeInt(0x01016408);
        outputStream.writeShort(0x1c);
        outputStream.writeShort(0x1c);
        outputStream.writeShort(0x052d);

        outputStream.writeShort(market.ordinal());
        outputStream.writeAscii(code);
        outputStream.writeShort(1);
        outputStream.writeShort(start);
        outputStream.writeShort(count);
        outputStream.writeInt(Integer.parseInt(date));

        outputStream.writeInt(0);
        outputStream.writeInt(0);
        outputStream.writeShort(0);


    }

    @Override
    protected List<TickTransactions> onMessage(TdxInputStream inputStream) throws IOException {

        byte[] result = new byte[8];
        inputStream.read(result);
        if (result[1] != 0x01) {
            throw new IOException("接收数据失败");
        }
        int dataSize = result[3] + result[4] * 256;
        byte[] tickData = new byte[dataSize];
        inputStream.read(tickData);
        // 处理Tick行情数据
        System.out.println(new String(tickData));
        return null;
    }

    protected void skip(TdxInputStream inputStream) {
        inputStream.skip(4);
    }

}
