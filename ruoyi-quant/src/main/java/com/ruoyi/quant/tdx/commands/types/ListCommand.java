package com.ruoyi.quant.tdx.commands.types;


import com.ruoyi.quant.tdx.commands.BaseCommand;
import com.ruoyi.quant.tdx.impl.TdxInputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class ListCommand<T> extends BaseCommand<List<T>> {


    @Override
    protected List<T> onMessage(TdxInputStream inputStream) throws IOException {
        int count = inputStream.readShort();

        List<T> list = new ArrayList<T>(count);
        for (int i = 0; i < count; ++i) {
            list.add(parseMessage(inputStream));
        }
        return list;
    }

    protected void skip(TdxInputStream inputStream) {

    }

    protected abstract T parseMessage(TdxInputStream inputStream) throws IOException;

}
