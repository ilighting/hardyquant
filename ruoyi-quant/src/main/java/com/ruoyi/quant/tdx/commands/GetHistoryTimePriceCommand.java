package com.ruoyi.quant.tdx.commands;

import com.ruoyi.quant.tdx.Market;
import com.ruoyi.quant.tdx.impl.TdxInputStream;
import com.ruoyi.quant.tdx.utils.TdxDataOutputStream;

import java.io.IOException;

public class GetHistoryTimePriceCommand extends GetTimePriceCommand {

    /// yyyMMdd
    private String date;

    public GetHistoryTimePriceCommand(Market market, String code, String date) {
        super(market, code);
        this.date = date;
    }

    @Override
    protected void writeCmd(TdxDataOutputStream outputStream) throws IOException {
        outputStream.writeHexString("0c01300001010d000d00b40f");
        outputStream.writeInt(Integer.parseInt(date));
        outputStream.write(market.ordinal());
        outputStream.writeAscii(code);
    }

    @Override
    protected void skip(TdxInputStream inputStream) {
        inputStream.skip(4);
    }
}
