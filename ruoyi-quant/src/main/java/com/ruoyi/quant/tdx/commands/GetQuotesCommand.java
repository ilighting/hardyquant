package com.ruoyi.quant.tdx.commands;


import com.ruoyi.quant.tdx.commands.types.ListCommand;
import com.ruoyi.quant.tdx.domain.Quote;
import com.ruoyi.quant.tdx.impl.TdxInputStream;
import com.ruoyi.quant.tdx.utils.TdxDataOutputStream;

import java.io.IOException;

public class GetQuotesCommand extends ListCommand<Quote> {

    private final String code;
    private final int market;
    private final int start;
    private final int count;
    private final int category;
    double pre_diff_base = 0;

    public GetQuotesCommand(
            int category,
            int market,
            String code,

            int start,
            int count
    ) {
        if (count > 800) {
            count = 800;
        }

        this.code = code;
        this.market = market;
        this.start = start;
        this.count = count;
        this.category = category;
    }

    static String getDateTime(int year, int month, int date, int hour, int minute) {
        return String.format("%d%02d%02d%02d%02d", year, month, date, hour, minute);
    }

    static String getDate(int year, int month, int date) {
        return String.format("%d%02d%02d", year, month, date);
    }

    protected static double getPrice(double base, double diff) {
        return (base + diff) / 1000;
    }

    public static int index_bytes(byte[] data, int pos){

        return data[pos];
    }



    public static int getPrice(byte[] data, int pos) {
        int pos_byte = 6;
        int bdata = index_bytes(data, pos);
        int int_data = bdata & 0x3F;
        boolean sign = (bdata & 0x40) != 0;

        if ((bdata & 0x80) != 0) {
            while (true) {
                pos += 1;
                bdata = index_bytes(data, pos);
                int_data += (bdata & 0x7F) << pos_byte;
                pos_byte += 7;

                if ((bdata & 0x80) != 0) {
                    continue;
                } else {
                    break;
                }
            }
        }

        pos += 1;

        if (sign) {
            int_data = -int_data;
        }

        return int_data;
    }

    public static double getVolumn(int ivol) {
        int logpoint = ivol >> (8 * 3);
        int hheax = ivol >> (8 * 3);
        int hleax = (ivol >> (8 * 2)) & 0xff;
        int lheax = (ivol >> 8) & 0xff;
        int lleax = ivol & 0xff;
        double dbl_1 = 1.0;
        double dbl_2 = 2.0;
        double dbl_128 = 128.0;

        int dwEcx = logpoint * 2 - 0x7f;
        int dwEdx = logpoint * 2 - 0x86;
        int dwEsi = logpoint * 2 - 0x8e;
        int dwEax = logpoint * 2 - 0x96;

        int tmpEax;
        if (dwEcx < 0) {
            tmpEax = -dwEcx;
        } else {
            tmpEax = dwEcx;
        }

        double dbl_xmm6 = 0.0;
        dbl_xmm6 = Math.pow(2.0, tmpEax);
        if (dwEcx < 0) {
            dbl_xmm6 = 1.0 / dbl_xmm6;
        }

        double dbl_xmm4 = 0;
        double tmpdbl_xmm3;
        double tmpdbl_xmm1;
        if (hleax > 0x80) {
            tmpdbl_xmm3 = 0.0;
            tmpdbl_xmm1 = 0.0;
            int dwtmpeax = dwEdx + 1;
            tmpdbl_xmm3 = Math.pow(2.0, dwtmpeax);
            double dbl_xmm0 = Math.pow(2.0, dwEdx) * 128.0;
            dbl_xmm0 += (hleax & 0x7f) * tmpdbl_xmm3;
            dbl_xmm4 = dbl_xmm0;
        } else {
            double dbl_xmm0 = 0.0;
            if (dwEdx >= 0) {
                dbl_xmm0 = Math.pow(2.0, dwEdx) * hleax;
            } else {
                dbl_xmm0 = (1 / Math.pow(2.0, dwEdx)) * hleax;
                dbl_xmm4 = dbl_xmm0;
            }

        }


        double dbl_xmm3 = Math.pow(2.0, dwEsi) * lheax;
        double dbl_xmm1 = Math.pow(2.0, dwEax) * lleax;
        if ((hleax & 0x80) != 0) {
            dbl_xmm3 *= 2.0;
            dbl_xmm1 *= 2.0;
        }


        double dbl_ret = dbl_xmm6 + dbl_xmm4 + dbl_xmm3 + dbl_xmm1;
        return dbl_ret;
    }

    @Override
    protected void writeCmd(TdxDataOutputStream outputStream) throws IOException {

        outputStream.writeShort(0x10c);
        outputStream.writeInt(0x01016408);
        outputStream.writeShort(0x1c);
        outputStream.writeShort(0x1c);
        outputStream.writeShort(0x052d);

        outputStream.writeShort(market);
        outputStream.writeAscii(code);
        outputStream.writeShort(category);
        outputStream.writeShort(1);
        outputStream.writeShort(start);
        outputStream.writeShort(count);

        outputStream.writeInt(0);
        outputStream.writeInt(0);
        outputStream.writeShort(0);

    }

    String getDateTime(TdxInputStream inputStream) throws IOException {
        int year;
        int month;
        int hour;
        int minute;
        int day;
        if (category < 4 || category == 7 || category == 8) {
            int zipday = inputStream.readShort();
            int tminutes = inputStream.readShort();
            year = (zipday >> 11) + 2004;
            month = (int) ((zipday % 2048) / 100);
            day = (zipday % 2048) % 100;

            hour = (tminutes / 60);
            minute = tminutes % 60;
            return getDateTime(year, month, day, hour, minute);
        } else {
            int zipday = inputStream.readInt();
            year = (zipday / 10000);
            month = ((zipday % 10000) / 100);
            day = zipday % 100;
            return getDate(year, month, day);
        }


    }

    @Override
    protected Quote parseMessage(TdxInputStream stream) throws IOException {
        String date = getDateTime(stream);
        double price_open_diff = stream.readPrice();
        double price_close_diff = stream.readPrice();
        double price_high_diff = stream.readPrice();
        double price_low_diff = stream.readPrice();

        int vol_row = stream.readInt();
        double vol = getVolumn(vol_row);
        int dbvol_row = stream.readInt();
        double amt = getVolumn(dbvol_row);

        double open = getPrice(price_open_diff, pre_diff_base);
        price_open_diff = price_open_diff + pre_diff_base;

        double close = getPrice(price_open_diff, price_close_diff);
        double high = getPrice(price_open_diff, price_high_diff);
        double low = getPrice(price_open_diff, price_low_diff);

        pre_diff_base = price_open_diff + price_close_diff;

        Quote quote = createQuote();
        quote.setDate(date);
        quote.setClose(close);
        quote.setOpen(open);
        quote.setHigh(high);
        quote.setLow(low);
        quote.setVol((int) vol);
        quote.setAmt(amt);

        return quote;
    }

    protected Quote createQuote() {
        return new Quote();
    }


}
