package com.ruoyi.quant.tdx;

import com.ruoyi.quant.tdx.impl.TdxInputStream;
import com.ruoyi.quant.tdx.utils.TdxDataOutputStream;

import java.io.IOException;

public interface TdxCommand<R> {
    R process(TdxDataOutputStream outputStream, TdxInputStream inputStream) throws IOException;
}
