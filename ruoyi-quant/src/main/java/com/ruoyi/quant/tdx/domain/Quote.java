package com.ruoyi.quant.tdx.domain;

import java.io.Serializable;

public class Quote implements Serializable {
    private static final long serialVersionUID = 1L;
    //日期
    private String date;
    //收盘
    private double close;
    //开盘
    private double open;
    //最低
    private double low;
    //最高
    private double high;
    //成交量
    private int vol;
    //成交额
    private double amt;
    //不知
    private int tor;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public int getVol() {
        return vol;
    }

    public void setVol(int vol) {
        this.vol = vol;
    }

    public double getAmt() {
        return amt;
    }

    public void setAmt(double amt) {
        this.amt = amt;
    }

    public int getTor() {
        return tor;
    }

    public void setTor(int tor) {
        this.tor = tor;
    }

    @Override
    public String toString() {
        return "Quote{" +
                "date='" + date + '\'' +
                ", close=" + close +
                ", open=" + open +
                ", low=" + low +
                ", high=" + high +
                ", vol=" + vol +
                ", amt=" + amt +
                ", tor=" + tor +
                '}';
    }
}
