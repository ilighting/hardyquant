package com.ruoyi.quant.tdx;

import com.ruoyi.common.domain.IEnum;

/**
 * 时间线
 */
public enum TdxCategory implements IEnum {

    m5,//0，五日均线
    m15,//1，15日均线
    m30,//2，30日均线
    hour,//3，小时内
    day,//4，日内
    week,//5，周内
    month,//6，月内
    m1,//7，一分钟
    _m1,//8 一分钟
    _d,//9 当日
    season,//10，季度
    year,//11，年度


}
