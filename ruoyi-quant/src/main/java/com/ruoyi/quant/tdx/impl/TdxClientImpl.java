package com.ruoyi.quant.tdx.impl;


import com.ruoyi.quant.tdx.*;
import com.ruoyi.quant.tdx.commands.*;
import com.ruoyi.quant.tdx.commands.types.LoginCommand;
import com.ruoyi.quant.tdx.domain.Quote;
import com.ruoyi.quant.tdx.domain.TickTransactions;
import com.ruoyi.quant.tdx.reader.TdxBlockReader;
import com.ruoyi.quant.tdx.utils.HexUtils;
import com.ruoyi.quant.tdx.utils.TdxDataOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * 非线程安全，如要实现多线程，则需要在每一个线程中实例化一个TxdClient
 */
public class TdxClientImpl implements TdxClient {


    public static final int DEFAULT_SO_TIMEOUT = 10000;
    static final int CHUNK_SIZE = 0x7530;
    private static final int DEFAULT_CONNECT_TIMEOUT = 500;
    private static final Log log = LogFactory.getLog(TdxClient.class);
    Socket socket;
    TdxInputStream inputStream;
    private TdxDataOutputStream outputStream;
    private InetSocketAddress address;
    private int soTimeout = DEFAULT_SO_TIMEOUT;
    private int connectTimeout = DEFAULT_CONNECT_TIMEOUT;
    private String tdxRootDir;

    @Override
    public void connect() throws IOException {
        socket = new Socket();
        socket.setSoTimeout(soTimeout);
        socket.connect(address, connectTimeout);
        InputStream inputStream = socket.getInputStream();
        OutputStream out = socket.getOutputStream();
        TdxInputStream txdInput = new TdxInputStream(new BufferedInputStream(inputStream));
        TdxDataOutputStream txdOutput = new TdxDataOutputStream(new BufferedOutputStream(out));
        this.inputStream = txdInput;
        this.outputStream = txdOutput;

        new LoginCommand().process(this.outputStream, this.inputStream);
    }

    @Override
    public boolean isConnected() {
        if (socket == null) {
            return false;
        }
        return socket.isConnected();
    }

    @Override
    public void close() {
        if (socket != null) {
            try {
                socket.close();
            } catch (Throwable e) {

            }
        }
    }

    /**
     * 指数行情
     * @param tdxCategory
     * @param market
     * @param code
     * @param start
     * @param count
     * @return
     * @throws IOException
     */
    @Override
    public List<Quote> getIndexQuotes(
            TdxCategory tdxCategory,
            Market market,
            String code,
            int start,
            int count
    ) throws IOException {

        GetIndexQuotesCommand cmd = new GetIndexQuotesCommand(tdxCategory.ordinal(), market.ordinal(), code, start, count);
        return cmd.process(this.outputStream, this.inputStream);
    }

    @Override
    public List<Quote> getQuotes(
            TdxCategory tdxCategory,
            Market market,
            String code,
            int start,
            int count
    ) throws IOException {
        GetQuotesCommand cmd = new GetQuotesCommand(tdxCategory.ordinal(), market.ordinal(), code, start, count);
        return cmd.process(this.outputStream, this.inputStream);
    }

    @Override
    public List<TimePrice> getTimePrice(Market market, String code) throws IOException {
        GetTimePriceCommand cmd = new GetTimePriceCommand(market, code);
        return cmd.process(this.outputStream, this.inputStream);
    }

    @Override
    public List<TimePrice> getHistoryTimePrice(Market market, String code, String date) throws IOException {
        GetHistoryTimePriceCommand cmd = new GetHistoryTimePriceCommand(market, code, date);
        return cmd.process(this.outputStream, this.inputStream);
    }

    @Override
    public List<TickTransactions> getHistoryTicks(Market market, String code, String date) throws IOException {
        GetHistoryTransactionCommand cmd = new GetHistoryTransactionCommand(market, code,0,10, date);
        return (List<TickTransactions>) cmd.process(this.outputStream, this.inputStream);
    }

    @Override
    public int getCount(Market market) throws IOException {
        outputStream.writeHexString("0c0c186c0001080008004e04");
        outputStream.writeShort(market.ordinal());
        outputStream.writeHexString("75c73301");

        outputStream.flush();

        inputStream.readPack(false);
        return inputStream.readShort();
    }

    @Override
    public List<StockInfo> getStockList(Market market, int start) throws IOException {
        GetStockCommand cmd = new GetStockCommand(market, start);
        return cmd.process(this.outputStream, this.inputStream);
    }

    private void getStockList(Market market, List<StockInfo> result) throws IOException {
        int start = 0;
        while (true) {
            List<StockInfo> list = getStockList(market, start);
            result.addAll(list);
            if (list.size() < 1000) {
                break;
            }
            start += 1000;
        }
    }

    @Override
    public List<StockInfo> getStockList() throws IOException {
        List<StockInfo> result = new ArrayList<StockInfo>();
        getStockList(Market.sh, result);
        getStockList(Market.sz, result);
        return result;
    }

    private BlockInfoMeta getBlockInfoMeta(String type) throws IOException {
        outputStream.write(HexUtils.decodeHex("0C39186900012A002A00C502"));
        outputStream.writeUtf8String(type, 0x2a - 2);
        outputStream.flush();

        inputStream.readPack(false);

        //I1s32s1s
        int size = inputStream.readInt();
        inputStream.skip(1);
        String hash = inputStream.readUtf8String(32);
        inputStream.skip(1);

        return new BlockInfoMeta(size, hash);

    }

    public byte[] getRawBlockInfo(String type, int start, int size) throws IOException {


        outputStream.writeHexString("0c37186a00016e006e00b906");
        outputStream.writeInt(start);
        outputStream.writeInt(size);
        outputStream.writeUtf8String(type, 0x6e - 10);
        outputStream.flush();

        inputStream.readPack(false);
        byte[] bytes = inputStream.toByteArray();
        //取出后面4个字节的
        return bytes;

    }

    @Override
    public Collection<BlockStock> getBlockInfo(BlockType type) throws IOException {
        if (type == BlockType.Concept) {
            return getBlockInfoByList(type);
        } else if (type == BlockType.Style) {
            return getBlockInfoByList(type);
        } else if (type == BlockType.Index) {
            return getBlockInfoByList(type);
        }else if(type==BlockType.TdxIndustry){
            return getBlockInfoByList(type);
        }
        //此时可以加载对应的信息
//        if (type == BlockType.TdxIndustry) {
//            if (tdxRootDir == null) {
//                log.warn("没有设置通达信根目录，不能获取全部信息");
//            }
//            byte[] stockBytes = downFile("tdxhy.cfg");
//            Collection<BlockStock> stocks = TdxBlockReader.getBlockStockInfos(tdxRootDir, new ByteArrayInputStream(stockBytes), type);
//            return stocks;
//        }
        throw new RuntimeException("不支持的type类型" + type);
    }

    private Collection<BlockStock> getBlockInfoByList(BlockType type) throws IOException {
        List<StockInfo> list = getStockList();
        if (type == BlockType.Concept) {
            return TdxBlockReader.fillCode(getBlockInfo(BlockFileType.BLOCK_GN), list,type);
        } else if (type == BlockType.Style) {
            return TdxBlockReader.fillCode(getBlockInfo(BlockFileType.BLOCK_FG), list,type);
        } else if (type == BlockType.Index) {
            return TdxBlockReader.fillCode(getBlockInfo(BlockFileType.BLOCK_ZS), list,type);
        } else {
            throw new RuntimeException("不支持的type类型" + type);
        }
    }

    private Collection<BlockStock> getBlockInfoByFile(BlockType type) throws IOException {

        if (type == BlockType.Concept) {
            return TdxBlockReader.getBlockStockInfos(tdxRootDir, getBlockInfo(BlockFileType.BLOCK_GN), type);
        } else if (type == BlockType.Style) {
            return TdxBlockReader.getBlockStockInfos(tdxRootDir, getBlockInfo(BlockFileType.BLOCK_FG), type);
        } else if (type == BlockType.Index) {
            return TdxBlockReader.getBlockStockInfos(tdxRootDir, getBlockInfo(BlockFileType.BLOCK_ZS), type);
        } else {
            throw new RuntimeException("不支持的type类型" + type);
        }
    }


    public List<BlockStock> getBlockInfo(BlockFileType type) throws IOException {
        byte[] bytes = downFile(type.getName());
        return TdxBlockReader.fromDatFile(bytes, type);
    }


    public byte[] downFile(String name) throws IOException {
        BlockInfoMeta meta = getBlockInfoMeta(name);


        int chuncks = meta.size / CHUNK_SIZE;
        if (meta.size % CHUNK_SIZE != 0) {
            chuncks++;
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        for (int seg = 0; seg < chuncks; ++seg) {
            int start = seg * CHUNK_SIZE;
            byte[] contents = getRawBlockInfo(name, start, meta.size);
            out.write(contents, 4, contents.length - 4);

        }
        return out.toByteArray();
    }


    class BlockInfoMeta {
        int size;
        String hash;

        public BlockInfoMeta(int size, String hash) {
            this.size = size;
            this.hash = hash;
        }
    }


    public String getTdxRootDir() {
        return tdxRootDir;
    }

    @Override
    public void setTdxRootDir(String tdxRootDir) {
        this.tdxRootDir = tdxRootDir;
    }

    public int getSoTimeout() {
        return soTimeout;
    }

    @Override
    public void setSoTimeout(int soTimeout) {
        this.soTimeout = soTimeout;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    @Override
    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public InetSocketAddress getSocketAddress() {
        return this.address;
    }

    @Override
    public void setSocketAddress(InetSocketAddress address) {
        this.address = address;
    }


}
