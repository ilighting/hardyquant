package com.ruoyi.quant.tdx;


import com.alibaba.ttl.TransmittableThreadLocal;
import com.mongodb.client.MongoClient;
import com.ruoyi.quant.domain.TdxIpConfig;
import com.ruoyi.quant.tdx.domain.Quote;
import com.ruoyi.quant.tdx.domain.TickTransactions;
import com.ruoyi.quant.tdx.impl.*;
import com.ruoyi.quant.utils.ConfigUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class TdxClientFactory implements TdxClientService {


    public static final Log log = LogFactory.getLog(TdxClientFactory.class);

    public static final int DEFAULT_RECONNECT_TIMEOUT = 1000;

    private final int threadCount;
    private final BlockingQueue<FutureTask> queue = new LinkedBlockingDeque<FutureTask>();
    private IpInfo[] ipInfos;
    private ServiceThread[] serviceThreads;
    private IpRecord ipRecord = new DefaultIpRecord();
    private AtomicInteger connectedCounter = new AtomicInteger(0);

    private int reconnectTimeout = DEFAULT_RECONNECT_TIMEOUT;
    private List<InetSocketAddress> connectedAddress = new ArrayList<InetSocketAddress>();
    private String tdxRootDir;

    public TdxClientFactory() {
        this(1);
    }

    public TdxClientFactory(int threadCount) {
        this.threadCount = threadCount;
    }

    private static ThreadLocal<Map<String, TdxClientFactory>> cache = new TransmittableThreadLocal<>();


    public static synchronized TdxClientFactory getInstance(int threadCount){

        Map<String, TdxClientFactory> stringTdxClientFactoryMap = cache.get();

        if(null!=stringTdxClientFactoryMap&&null!=stringTdxClientFactoryMap.get("master")){
            return stringTdxClientFactoryMap.get("master");
        }else{
            TdxClientFactory service = new TdxClientFactory(threadCount);
            service.start();
            cache.set(new HashMap<>(){{put("master",service);}});
            return service;
        }
    }

    public String getTdxRootDir() {
        return tdxRootDir;
    }

    @Override
    public void setTdxRootDir(String tdxRootDir) {
        this.tdxRootDir = tdxRootDir;
    }

    public IpRecord getIpRecord() {
        return ipRecord;
    }

    public void setIpRecord(IpRecord ipRecord) {
        this.ipRecord = ipRecord;
    }

    public synchronized void saveIpInfos() {
        ipRecord.save(ipInfos);
    }


    protected void autoSelectHost(TdxClientImpl client) throws IOException {
        List<TdxIpConfig> ip = ConfigUtils.getIp();
        for (int i = 0; i < ip.size(); ++i) {
            TdxIpConfig ipConfig = ip.get(i);

            InetSocketAddress address = new InetSocketAddress(ipConfig.getIp(), ipConfig.getPort());


            try {
                synchronized (connectedAddress) {
                    boolean found = false;
                    for (InetSocketAddress addr : connectedAddress) {
                        if (addr.equals(address)) {
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        continue;
                    }

                    log.info("Try to connect to host:" + ipConfig.getIp());
                    client.setSocketAddress(address);
                    client.connect();
                    connectedAddress.add(client.getSocketAddress());
                }

                break;
            } catch (IOException e) {

                try {
                    Thread.sleep(reconnectTimeout);
                } catch (InterruptedException ex) {
                    return;
                }
                log.warn("Connect to host " + ipConfig.getIp() + " fail ", e);
                continue;
            }
        }

    }

    private void createServiceThreads() {
        serviceThreads = new TxdServiceThread[threadCount];
        for (int i = 0; i < threadCount; ++i) {
            serviceThreads[i] = new TxdServiceThread();
        }


    }

    private void startSeviceThreads() {
        for (int i = 0; i < threadCount; ++i) {
            assert (serviceThreads[i] != null);
            serviceThreads[i].start();
        }
    }

    private void stopServiceThreads() {
        for (int i = 0; i < threadCount; ++i) {
            assert (serviceThreads[i] != null);
            serviceThreads[i].shutdown();
        }
    }

    @Override
    public synchronized void start() {
        ipInfos = Ips.getIpInfos(ipRecord);
        createServiceThreads();
        startSeviceThreads();
    }

    public void  stopIp(String ip){

    }

    @Override
    public synchronized void stop() {
        stopServiceThreads();

    }

    @Override
    public Future<List<Quote>> getQuotes(
            final TdxCategory tdxCategory,
            final Market market,
            final String code,
            final int start,
            final int count


    ) {
        return submit(() -> {
            TdxClientImpl client = getClient();
            return client.getQuotes(tdxCategory, market, code, start, count);
        });
    }

    @Override
    public void setReconnectTimeout(int timeout) {
        reconnectTimeout = timeout;
    }

    private TdxClientImpl getClient() {
        TxdServiceThread thread = (TxdServiceThread) Thread.currentThread();
        return thread.client;
    }

    protected <T> Future<T> submit(Callable<T> callable) {
        FutureTask<T> future = new FutureTask<T>(callable);;
        queue.add(future);
        return future;
    }

    /**
     * 获取股票数量
     * @param market
     * @return
     */
    @Override
    public Future<Integer> getCount(final Market market) {
        return submit(() -> {
            TdxClientImpl client = getClient();
            return client.getCount(market);
        });
    }

    /**
     * 股票列表
     * @param market
     * @param start
     * @return
     */
    @Override
    public Future<List<StockInfo>> getStockList(final Market market, final int start) {
        return submit(new Callable<List<StockInfo>>() {
            @Override
            public List<StockInfo> call() throws Exception {
                TdxClientImpl client = getClient();
                return client.getStockList(market, start);
            }
        });
    }

    /**
     * 板块信息，附带股票代码
     * @param type
     * @return
     */
    @Override
    public Future<Collection<BlockStock>> getBlockInfo(final BlockType type) {
        return submit(() -> {
            TdxClientImpl client = getClient();
            return client.getBlockInfo(type);
        });
    }

    /**
     * 板块文件数量，不附带股票代码
     * @param type
     * @return
     */
    public Future<List<BlockStock>> getBlockInfo(final BlockFileType type) {
        return submit(() -> {
            TdxClientImpl client = getClient();
            return client.getBlockInfo(type);
        });
    }

    /**
     * 指数行情
     * @param tdxCategory
     * @param market
     * @param code
     * @param start
     * @param count
     * @return
     */
    @Override
    public Future<List<Quote>> getIndexQuotes(
            final TdxCategory tdxCategory,
            final Market market,
            final String code,
            final int start,
            final int count
    ) {
        return submit(() -> {
            TdxClientImpl client = getClient();
            return client.getIndexQuotes(tdxCategory, market, code, start, count);
        });
    }

    /**
     *
     * @return
     */
    @Override
    public Future<List<StockInfo>> getStockList() {
        return submit(() -> {
            TdxClientImpl client = getClient();
            return client.getStockList();
        });
    }

    public Future<List<TimePrice>> getTimePrice(Market market, String number) {
        return submit(()->{
            TdxClientImpl client = getClient();
            return client.getTimePrice(market, number);
        });
    }

    public Future<List<TimePrice>> getHistoryTimePrice(Market market, String number, String date) {
        return submit(()->{
            TdxClientImpl client = getClient();
            return client.getHistoryTimePrice(market, number,date);
        });
    }

    public Future<List<TickTransactions>> getHistoryTicks(Market market, String number, String date) {
        return submit(()->{
            TdxClientImpl client = getClient();
            return client.getHistoryTicks(market, number,date);
        });
    }

    class TxdServiceThread extends ServiceThread {
        final TdxClientImpl client;
        private boolean connected = false;
        private int count;


        public TxdServiceThread() {
            client = new TdxClientImpl();
            client.setTdxRootDir(tdxRootDir);
        }

        /// 1分钟一次
        private void sendHeart() throws IOException {
            ++count;
            if (count >= 5 * 30) {
                count = 0;
                client.getCount(Market.sh);
            }
        }


        private void setConnected(boolean connected) {
            this.connected = connected;
            if (connected) {
                int connectCount = connectedCounter.incrementAndGet();
                if (connectCount == threadCount) {
                    saveIpInfos();
                }
            } else {
                connectedCounter.decrementAndGet();
            }
        }

        @Override
        protected boolean repetitionRun() {
            if (!connected) {
                try {
                    connectToHost();
                    setConnected(true);
                } catch (IOException e) {
                    //仍然等待下次连接的机会
                    try {
                        Thread.sleep(1000);
                        return true;
                    } catch (InterruptedException ex) {
                        return false;
                    }
                }
            }


            try {
                FutureTask task = queue.poll(200, TimeUnit.MILLISECONDS);
                try {
                    if (task == null) {
                        //检查心跳
                        sendHeart();
                        return true;
                    }
                    task.run();
                    task.get();
                    return true;
                } catch (Exception e) {
                    log.info("An error has happened, close the socket and reconnect", e);
                    setConnected(false);
                    client.close();
                    return true;

                }

            } catch (InterruptedException e) {
                return false;
            }

        }

        private void connectToHost() throws IOException {
            for (int j = 0; j < 3; ++j) {
                try {
                    autoSelectHost(client);
                    return;
                } catch (IOException ex) {
                    log.warn("服务器连接失败", ex);
                }
            }
            throw new IOException("Connect to host failed");
        }
    }


}
