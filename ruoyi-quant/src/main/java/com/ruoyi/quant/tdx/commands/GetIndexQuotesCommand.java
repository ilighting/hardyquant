package com.ruoyi.quant.tdx.commands;






import com.ruoyi.quant.tdx.domain.IndexQuote;
import com.ruoyi.quant.tdx.domain.Quote;
import com.ruoyi.quant.tdx.impl.TdxInputStream;

import java.io.IOException;

public class GetIndexQuotesCommand extends GetQuotesCommand {

    public GetIndexQuotesCommand(int category, int market, String code, int start, int count) {
        super(category, market, code, start, count);
    }

    @Override
    protected Quote parseMessage(TdxInputStream stream) throws IOException {
        IndexQuote quote = (IndexQuote) super.parseMessage(stream);
        int upCount = stream.readShort();
        int downCount = stream.readShort();
        quote.setUpCount(upCount);
        quote.setDownCount(downCount);
        return quote;
    }


    @Override
    protected Quote createQuote() {
        return new IndexQuote();
    }


}
