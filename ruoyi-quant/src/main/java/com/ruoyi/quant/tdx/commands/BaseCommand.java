package com.ruoyi.quant.tdx.commands;

import com.ruoyi.quant.tdx.TdxCommand;
import com.ruoyi.quant.tdx.impl.TdxInputStream;
import com.ruoyi.quant.tdx.utils.HexUtils;
import com.ruoyi.quant.tdx.utils.TdxDataOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



import java.io.IOException;

public abstract class BaseCommand<R> implements TdxCommand<R> {


    protected static final Log log = LogFactory.getLog(BaseCommand.class);


    @Override
    public R process(TdxDataOutputStream outputStream, TdxInputStream inputStream) throws IOException {
//        log.info("开始写入命令+消息");
        if (log.isDebugEnabled()) {
            RecordOutputStream recordOutputStream = new RecordOutputStream(outputStream);
            writeCmd(recordOutputStream);
            log.debug("Send data " + HexUtils.encodeHexStr(recordOutputStream.toByteArray()));
        } else {
            writeCmd(outputStream);
        }
        outputStream.flush();
        inputStream.readPack(false);
//        log.info("开始解析命令+返回");
        return onMessage(inputStream);
    }

    protected abstract void writeCmd(TdxDataOutputStream outputStream) throws IOException;

    protected abstract R onMessage(TdxInputStream inputStream) throws IOException;

}
