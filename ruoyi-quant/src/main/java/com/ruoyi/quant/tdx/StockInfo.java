package com.ruoyi.quant.tdx;

import java.util.StringJoiner;

public class StockInfo {

    String code;
    double price;
    String name;
    int decimalPoint;
    Market market;

    private int volUnit;

    public int getVolUnit() {
        return volUnit;
    }

    public void setVolUnit(final int volUnit) {
        this.volUnit = volUnit;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", StockInfo.class.getSimpleName() + "[", "]")
                .add("code='" + code + "'")
                .add("price=" + price)
                .add("name='" + name + "'")
                .add("decimalPoint=" + decimalPoint)
                .add("market=" + market)
                .add("volUnit=" + volUnit)
                .toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDecimalPoint() {
        return decimalPoint;
    }

    public void setDecimalPoint(int decimalPoint) {
        this.decimalPoint = decimalPoint;
    }
}
