package com.ruoyi.quant.tdx;

public class TimePrice {

    int vol;
    double close;

    @Override
    public String toString() {
        return "TimePrice{" +
                "vol=" + vol +
                ", close=" + close +
                '}';
    }

    public int getVol() {
        return vol;
    }

    public void setVol(int vol) {
        this.vol = vol;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }
}
