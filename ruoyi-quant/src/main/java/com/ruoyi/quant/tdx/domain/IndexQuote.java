package com.ruoyi.quant.tdx.domain;

public class IndexQuote extends Quote{
    private static final long serialVersionUID = 1L;

    private int upCount;
    private int downCount;

    public int getUpCount() {
        return upCount;
    }

    public void setUpCount(int upCount) {
        this.upCount = upCount;
    }

    public int getDownCount() {
        return downCount;
    }

    public void setDownCount(int downCount) {
        this.downCount = downCount;
    }
}
