package com.ruoyi.quant.tdx;

import com.ruoyi.quant.tdx.domain.Quote;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;

public interface TdxClientService {
    void setTdxRootDir(String tdxRootDir);

    void start();

    void stop();

    Future<Integer> getCount(Market market);

    Future<List<StockInfo>> getStockList(Market market, final int start);

    Future<Collection<BlockStock>> getBlockInfo(BlockType type);

    Future<List<Quote>> getIndexQuotes(
            TdxCategory tdxCategory,
            Market market,
            String code,
            int start,
            int count
    );


    //全部
    Future<List<StockInfo>> getStockList();

    Future<List<Quote>> getQuotes(
            TdxCategory tdxCategory,
            Market market,
            String code,
            int start,
            int count


    );


    // 设置断线之后重连的延时（毫秒)
    void setReconnectTimeout(int timeout);

}
