package com.ruoyi.quant.tdx.commands;

import com.ruoyi.quant.tdx.Market;
import com.ruoyi.quant.tdx.StockInfo;
import com.ruoyi.quant.tdx.commands.types.ListCommand;
import com.ruoyi.quant.tdx.impl.TdxInputStream;
import com.ruoyi.quant.tdx.utils.HexUtils;
import com.ruoyi.quant.tdx.utils.TdxDataOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class GetStockCommand extends ListCommand<StockInfo> {
    Market market;
    int start;

    public GetStockCommand(Market market, int start) {
        this.market = market;
        this.start = start;
    }

    @Override
    protected void writeCmd(TdxDataOutputStream outputStream) throws IOException {
        outputStream.write(HexUtils.decodeHex("0c0118640101060006005004"));
        outputStream.writeShort(market.ordinal());
        outputStream.writeShort(start);
    }

    public byte[] readInputStreamToByteArray(TdxInputStream inputStream) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int bytesRead;
        byte[] data = new byte[1024];

        while ((bytesRead = inputStream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, bytesRead);
        }

        buffer.flush();
        return buffer.toByteArray();
    }

    @lombok.SneakyThrows
    public void otherParse(TdxInputStream tdxInputStream){
        byte[] oneBytes =readInputStreamToByteArray(tdxInputStream);
        // 将one_bytes填充为相应的字节数组

        ByteBuffer buffer = ByteBuffer.wrap(oneBytes);
        buffer.order(ByteOrder.LITTLE_ENDIAN);

        byte[] codeBytes = new byte[6];
        short volunit;
        byte[] nameBytes = new byte[8];
        byte[] reversedBytes1 = new byte[4];
        int decimalPoint;
        int preCloseRaw;
        byte[] reversedBytes2 = new byte[4];

        buffer.get(codeBytes);
        volunit = buffer.getShort();
        buffer.get(nameBytes);
        buffer.get(reversedBytes1);
        decimalPoint = buffer.getInt();
        preCloseRaw = buffer.getInt();
        buffer.get(reversedBytes2);

        String code = new String(codeBytes);
        String name = new String(nameBytes);

        System.out.println("Code: " + code);
        System.out.println("Volunit: " + volunit);
        System.out.println("Name: " + name);
        System.out.println("Decimal Point: " + decimalPoint);
        System.out.println("Pre Close Raw: " + preCloseRaw);

    }

    @Override
    protected StockInfo parseMessage(TdxInputStream inputStream) throws IOException {
        //6sH8s4sBI4s
        String code = inputStream.readUtf8String(6);
        int volunit = inputStream.readShort();
        String name = inputStream.readGbkString(8);
        inputStream.skip(4);
        int decimal_point = inputStream.readByte();
        int pre_close_raw = inputStream.readInt();
        inputStream.skip(4);
        double pre_close = GetQuotesCommand.getVolumn(pre_close_raw);
        StockInfo stockInfo = new StockInfo();
        stockInfo.setCode(code);
        stockInfo.setMarket(market);

        stockInfo.setDecimalPoint(decimal_point);
        stockInfo.setPrice(pre_close);
        stockInfo.setName(name);
        stockInfo.setVolUnit(volunit);
        return stockInfo;
    }
}
