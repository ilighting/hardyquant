package com.ruoyi.quant.tdx.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Utils {

    public static int[] getTime(byte[] buffer, int pos) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        short minutes = byteBuffer.getShort(pos);

        int hour = minutes / 60;
        int minute = minutes % 60;

        pos += 2;

        return new int[]{hour, minute, pos};
    }
}
