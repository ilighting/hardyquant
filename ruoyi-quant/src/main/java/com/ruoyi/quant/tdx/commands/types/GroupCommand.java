package com.ruoyi.quant.tdx.commands.types;


import com.ruoyi.quant.tdx.TdxCommand;
import com.ruoyi.quant.tdx.impl.TdxInputStream;
import com.ruoyi.quant.tdx.utils.TdxDataOutputStream;

import java.io.IOException;

public class GroupCommand implements TdxCommand<Object> {

    private final TdxCommand[] commands;

    public GroupCommand(TdxCommand... commands) {
        this.commands = commands;
    }

    @Override
    public Object process(TdxDataOutputStream outputStream, TdxInputStream inputStream) throws IOException {
        for (TdxCommand c : commands) {
            c.process(outputStream, inputStream);
        }
        return null;
    }
}
