package com.ruoyi.quant.tdx.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class TickTransactions implements Serializable {
    private static final long serialVersionUID = 1L;

    private String time;
    private Integer hour;
    private Integer minute;

    private Double price;
    private Integer vol;
    private Integer buyOrSell;
}
