/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 2023年10月23日 下午5:40:26
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2023 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.ruoyi.quant.demos;

import java.io.Serializable;

public  class GocpB2BProductBatchData  implements Serializable

{

	/** Default serialVersionUID value. */

	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>GocpB2BProductBatchData.catelog</code> property defined at extension <code>gocpfacades</code>. */

	private String catelog;

	/** <i>Generated property</i> for <code>GocpB2BProductBatchData.customerPk</code> property defined at extension <code>gocpfacades</code>. */

	private Long customerPk;

	/** <i>Generated property</i> for <code>GocpB2BProductBatchData.productPk</code> property defined at extension <code>gocpfacades</code>. */

	private Long productPk;

	/** <i>Generated property</i> for <code>GocpB2BProductBatchData.customerCode</code> property defined at extension <code>gocpfacades</code>. */

	private String customerCode;

	/** <i>Generated property</i> for <code>GocpB2BProductBatchData.customerGroup</code> property defined at extension <code>gocpfacades</code>. */

	private String customerGroup;

	/** <i>Generated property</i> for <code>GocpB2BProductBatchData.productType</code> property defined at extension <code>gocpfacades</code>. */

	private String productType;

	/** <i>Generated property</i> for <code>GocpB2BProductBatchData.productCode</code> property defined at extension <code>gocpfacades</code>. */

	private String productCode;

	/** <i>Generated property</i> for <code>GocpB2BProductBatchData.isBundle</code> property defined at extension <code>gocpfacades</code>. */

	private Boolean isBundle;

	/** <i>Generated property</i> for <code>GocpB2BProductBatchData.productLine</code> property defined at extension <code>gocpfacades</code>. */

	private String productLine;

	public GocpB2BProductBatchData()
	{
		// default constructor
	}

	public void setCatelog(final String catelog)
	{
		this.catelog = catelog;
	}

	public String getCatelog()
	{
		return catelog;
	}

	public void setCustomerPk(final Long customerPk)
	{
		this.customerPk = customerPk;
	}

	public Long getCustomerPk()
	{
		return customerPk;
	}

	public void setProductPk(final Long productPk)
	{
		this.productPk = productPk;
	}

	public Long getProductPk()
	{
		return productPk;
	}

	public void setCustomerCode(final String customerCode)
	{
		this.customerCode = customerCode;
	}

	public String getCustomerCode()
	{
		return customerCode;
	}

	public void setCustomerGroup(final String customerGroup)
	{
		this.customerGroup = customerGroup;
	}

	public String getCustomerGroup()
	{
		return customerGroup;
	}

	public void setProductType(final String productType)
	{
		this.productType = productType;
	}

	public String getProductType()
	{
		return productType;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	public String getProductCode()
	{
		return productCode;
	}

	public void setIsBundle(final Boolean isBundle)
	{
		this.isBundle = isBundle;
	}

	public Boolean getIsBundle()
	{
		return isBundle;
	}

	public void setProductLine(final String productLine)
	{
		this.productLine = productLine;
	}

	public String getProductLine()
	{
		return productLine;
	}


}
