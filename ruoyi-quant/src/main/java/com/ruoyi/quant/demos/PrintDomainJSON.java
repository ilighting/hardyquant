package com.ruoyi.quant.demos;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrintDomainJSON {
    public static void main(String[] args) {
        GocpB2BProductBatchData data=new GocpB2BProductBatchData();
        // 使用Hutool获取Domain对象的字段并转换为JSON字符串
        String jsonStr = JSONUtil.toJsonStr(BeanUtil.beanToMap(data));
        System.out.println(jsonStr);

        Map<String, List<GocpB2BProductBatchData>> maps=new HashMap<>();

        List<GocpB2BProductBatchData> dataList=new ArrayList<>();
        data.setCatelog("abc");
        dataList.add(data);
        maps.put("abc",dataList);
        String jsonStr1 = JSONUtil.toJsonStr(maps);
        System.out.println(jsonStr1);

    }
}
