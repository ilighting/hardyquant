package com.ruoyi.quant.demos;

import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;

public class ADexample {
    public static void main(String[] args) {
        // 创建输入变量并赋值
        double[] highPrices = {100, 102, 101, 98, 99};
        double[] lowPrices = {98, 99, 97, 94, 95};
        double[] closePrices = {99, 101, 98, 95, 96};
        double[] volumes = {1000, 2000, 1500, 3000, 2500};

        MInteger outBegIdx=new MInteger();
        MInteger outNBElement=new MInteger();
        // 创建输出变量
        double[] adValues = new double[highPrices.length];

        // 使用TALib4j计算AD指标
        Core core = new Core();
        core.ad(0,highPrices.length-1,highPrices, lowPrices, closePrices, volumes,outBegIdx,outNBElement, adValues);

        // 打印AD指标结果
        for (double value : adValues) {
            System.out.println(value);
        }
    }
}
