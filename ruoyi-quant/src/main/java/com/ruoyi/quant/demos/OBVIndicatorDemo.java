package com.ruoyi.quant.demos;

import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

public class OBVIndicatorDemo {

    public static void main(String[] args) {
        double[] prices = {10.0, 12.0, 11.0, 15.0, 14.0};
        double[] volumes = {1000.0, 1500.0, 2000.0, 1800.0, 2500.0};

        // 创建 Core 实例
        Core core = new Core();

        // 创建输入数据
        double[] inClose = prices;
        double[] inVolume = volumes;

        // 创建输出数据
        double[] outObv = new double[inClose.length];

        // 创建变量以保存计算结果的返回代码
        MInteger outBegIdx = new MInteger();
        MInteger outNbElement = new MInteger();
        RetCode retCode;

        // 计算 OBV
        retCode = core.obv(0, inClose.length - 1, inClose, inVolume, outBegIdx, outNbElement, outObv);
        if (retCode == RetCode.Success) {
            System.out.println("OBV:");
            for (int i = 0; i < outNbElement.value; i++) {
                System.out.println(outObv[i]);
            }
        }
    }
}
