package com.ruoyi.quant;

import tech.tablesaw.api.*;
import tech.tablesaw.columns.Column;

import static tech.tablesaw.api.QuerySupport.and;

public class TableJoinExample {
    public static void main(String[] args) {
        Table studentTable = Table.create("student").addColumns(IntColumn.create("id", 20200001, 20200002, 20200003, 20200004, 20200005),
                StringColumn.create("name", "Jim", "Bob", "John", "lily", "lucy"),
                IntColumn.create("age", 18, 18, 17, 16, 16));

        Table result = studentTable.where(and(t -> t.numberColumn("id").isGreaterThan(20200001),
                t -> t.numberColumn("age").isEqualTo(18)));
        Table sellResult = studentTable.where(and(t -> t.numberColumn("id").isGreaterThan(20200001),
                t -> t.numberColumn("age").isEqualTo(16)));
//        System.out.println(result);
        Table buyTable = makeBuyTag(result);
        Table sellTable = makeSellTag(sellResult);
        Table id = studentTable.joinOn("id").leftOuter(buyTable,sellTable);
        System.out.println(id);
    }

    private static Table makeBuyTag(Table result){
        Column<?> oldCOL = result.column("age");
        result.addColumns(oldCOL.copy().setName("buy"));
        result.removeColumns("name");
        result.removeColumns("age");
        return result;
    }

    private static Table makeSellTag(Table result){
        Column<?> oldCOL = result.column("age");
        result.addColumns(oldCOL.copy().setName("sell"));
        result.removeColumns("name");
        result.removeColumns("age");
        return result;
    }



}
