package com.ruoyi.quant.service.impl;


import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.domain.PageAjax;
import com.ruoyi.quant.domain.Stocks;
import com.ruoyi.quant.mg.MgSort;
import com.ruoyi.quant.mg.service.MongoServiceImpl;
import com.ruoyi.quant.service.StocksService;
import com.ruoyi.quant.tdx.StockInfo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StocksServiceImpl extends MongoServiceImpl<Stocks> implements StocksService {



	@Override
	public void saveBatch(final List<StockInfo> result) {
		List<Stocks> collect = result.stream().map(item ->{
			Stocks stocks=new Stocks();
			 BeanUtil.copyProperties(item,stocks);
			 return stocks;
		} ).collect(Collectors.toList());
		if(collect.size()>0){
			saveAll(collect);
		}
	}


	@Override
	public PageAjax pageList(Stocks stocks) {
		List<Stocks> list = pageList(stocks, stocks.getPageNum(), stocks.getPageSize());
		if(list.isEmpty()){
			return PageAjax.buildSuccess(list,0);
		}
		Long count = this.count(stocks);
		return PageAjax.buildSuccess(list,count);
	}


	@Override
	public int addOrUpdate(final Stocks stocks) {
		//todo 获取字段注解，完成byCol的方法
//		AnnotationUtils.getAnnotation(UniqueCol.class,stocks);
//		QueryWrapper<Stocks> queryWrapper=new QueryWrapper<>();
//		queryWrapper.eq(Stocks::getCode,stocks);

		return 0;
	}


}
