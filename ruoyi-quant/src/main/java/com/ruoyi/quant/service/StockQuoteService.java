package com.ruoyi.quant.service;


import com.ruoyi.common.domain.PageAjax;
import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.mg.service.IMongoService;
import com.ruoyi.quant.tdx.domain.Quote;

import java.util.List;

public interface StockQuoteService<T> extends IMongoService<T> {
    void saveQuotes(List<Quote> quoteList, String code);


    void saveMinute(List<Quote> quoteList, String code);

    PageAjax pageList(StockQuote quote);
}
