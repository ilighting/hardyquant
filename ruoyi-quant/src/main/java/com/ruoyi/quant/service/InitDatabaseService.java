package com.ruoyi.quant.service;

import com.ruoyi.quant.pipe.common.Pipe;
import com.ruoyi.quant.service.pipes.InitStockListService;
import org.springframework.stereotype.Service;

/**
 * 初始化服务器数据库
 */
@Service
public class InitDatabaseService {


    public void init(){
        Pipe line = Pipe.line();
        line.middle(InitStockListService.class);


    }

}
