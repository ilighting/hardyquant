package com.ruoyi.quant.service;



import com.ruoyi.common.domain.PageAjax;
import com.ruoyi.quant.domain.Stocks;
import com.ruoyi.quant.mg.service.IMongoService;
import com.ruoyi.quant.tdx.StockInfo;

import java.util.List;

/**
 * 股票列表
 */
public interface StocksService extends IMongoService<Stocks> {
	void saveBatch(List<StockInfo> result);


	PageAjax pageList(Stocks stocks);


	int addOrUpdate(Stocks stocks);
}
