package com.ruoyi.quant.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.domain.PageAjax;
import com.ruoyi.quant.domain.MinuteQuote;
import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.mg.service.MongoServiceImpl;
import com.ruoyi.quant.service.StockQuoteService;
import com.ruoyi.quant.tdx.domain.Quote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StockQuoteServiceImpl<T> extends MongoServiceImpl<T> implements StockQuoteService<T> {
	private static final Logger log = LoggerFactory.getLogger(StockQuoteServiceImpl.class);

	@Override
	public void saveQuotes(List<Quote> quoteList, String code) {
		List<StockQuote> collect = quoteList.stream().map(item -> {
			StockQuote stockQuote = new StockQuote();
			BeanUtil.copyProperties(item, stockQuote);
			stockQuote.setCode(code);
			return stockQuote;
		}).collect(Collectors.toList());


		saveAll(collect);
		log.info("保存完毕");
	}

	@Override
	public void saveMinute(List<Quote> quoteList, String code) {
		List<MinuteQuote> collect = quoteList.stream().map(item -> {
            MinuteQuote stockQuote = new MinuteQuote();
			BeanUtil.copyProperties(item, stockQuote);
			stockQuote.setCode(code);
			return stockQuote;
		}).collect(Collectors.toList());


		saveAll(collect);
		log.info("保存完毕");
	}

	@Override
	public PageAjax pageList(StockQuote quote) {
		List<StockQuote> list = pageList(quote, quote.getPageNum(), quote.getPageSize());
		if (list.isEmpty()) {
			return PageAjax.buildSuccess(list, 0);
		}
		Long count = this.count(quote);
		return PageAjax.buildSuccess(list, count);
	}
}
