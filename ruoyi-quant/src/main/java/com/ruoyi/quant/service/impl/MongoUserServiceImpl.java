package com.ruoyi.quant.service.impl;

import com.ruoyi.quant.domain.User;
import com.ruoyi.quant.mg.service.IMongoService;
import com.ruoyi.quant.mg.service.MongoServiceImpl;

import org.springframework.stereotype.Service;

@Service
public class MongoUserServiceImpl extends MongoServiceImpl<User> implements IMongoService<User> {

}
