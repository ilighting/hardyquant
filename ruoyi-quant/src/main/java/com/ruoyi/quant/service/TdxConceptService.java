package com.ruoyi.quant.service;



import com.ruoyi.quant.domain.TdxConcept;
import com.ruoyi.quant.mg.service.IMongoService;
import com.ruoyi.quant.tdx.BlockStock;

import java.util.Collection;

public interface TdxConceptService extends IMongoService<TdxConcept> {
	void saveStockBlocks(Collection<BlockStock> blockStocks);
}
