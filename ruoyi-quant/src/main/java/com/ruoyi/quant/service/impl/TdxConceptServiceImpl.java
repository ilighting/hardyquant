package com.ruoyi.quant.service.impl;


import com.ruoyi.quant.domain.TdxConcept;
import com.ruoyi.quant.mg.service.MongoServiceImpl;
import com.ruoyi.quant.service.TdxConceptService;
import com.ruoyi.quant.tdx.BlockStock;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class TdxConceptServiceImpl extends MongoServiceImpl<TdxConcept> implements TdxConceptService {
	@Override
	public void saveStockBlocks(final Collection<BlockStock> blockStocks) {
		List<TdxConcept> collect = blockStocks.stream().map(item -> {
			TdxConcept concept = new TdxConcept();
			concept.setCode(item.getCode());
			concept.setCodes(item.getCodes());
			concept.setLevel(item.getLevel());
			concept.setName(item.getName());
			concept.setType(item.getType().name());
			return concept;
		}).collect(Collectors.toList());
		saveAll(collect);
	}
}
