package com.ruoyi.quant.service;


import com.ruoyi.quant.domain.TdxIpConfig;
import com.ruoyi.quant.mg.service.IMongoService;

public interface IpConfigService extends IMongoService<TdxIpConfig> {
}
