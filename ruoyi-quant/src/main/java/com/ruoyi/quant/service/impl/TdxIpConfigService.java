package com.ruoyi.quant.service.impl;


import com.ruoyi.quant.domain.TdxIpConfig;
import com.ruoyi.quant.mg.service.MongoServiceImpl;
import com.ruoyi.quant.service.IpConfigService;
import org.springframework.stereotype.Service;

@Service(value ="tdxIpConfigService" )
public class TdxIpConfigService  extends MongoServiceImpl<TdxIpConfig> implements IpConfigService {

}
