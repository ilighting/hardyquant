package com.ruoyi.quant.pipe.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class IMiddleware {
    public static final Logger log= LoggerFactory.getLogger(IMiddleware.class);

    public abstract void handle(PipeContext ctx);
}
