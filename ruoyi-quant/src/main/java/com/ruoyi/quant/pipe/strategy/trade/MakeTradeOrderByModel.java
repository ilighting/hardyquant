package com.ruoyi.quant.pipe.strategy.trade;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.math.SharpUtil;
import com.ruoyi.quant.domain.TradeModel;
import com.ruoyi.quant.domain.models.DayTaModel;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.pipe.strategy.commons.BaseTablesaw;
import com.ruoyi.quant.service.CommonTaService;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeOrder;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeView;
import com.ruoyi.quant.trade.crud.service.IDfcfTradeOrderService;
import com.ruoyi.quant.trade.crud.service.IDfcfTradeViewService;
import com.ruoyi.quant.utils.MoneyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.tablesaw.api.*;
import tech.tablesaw.selection.Selection;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static tech.tablesaw.api.QuerySupport.and;

@Service
public class MakeTradeOrderByModel extends BaseTablesaw {


	@Autowired
	CommonTaService<DayTaModel> taModelService;

	private static final Double initialMoney = 30000d;

	@Autowired
	IDfcfTradeOrderService dfcfTradeOrderService;

	@Autowired
	IDfcfTradeViewService tradeViewService;

	@Override
	public void handle(PipeContext ctx) {
		TradeModel tradeModel = (TradeModel) ctx.get("TradeModel");
		DfcfTradeView tradeView = (DfcfTradeView) ctx.get("View");
		String code = tradeModel.getCode();
		String modelType = tradeModel.getModelType();
		DayTaModel dayTaModel = new DayTaModel();
		dayTaModel.setCode(code);
		dayTaModel.setType(modelType);
		List<DayTaModel> dayTaModels = taModelService.listAll(dayTaModel);
		super.before(dayTaModels);
		tradeByTablesaw(code,tradeView);
	}

	public void tradeByTablesaw(String code,DfcfTradeView tradeView) {

		Table cciTable = Table.create("CCI").addColumns(
				DateColumn.create("date", getDates()),
				StringColumn.create("code", getCode()),
				DoubleColumn.create("cci_model", getIval()),
				DoubleColumn.create("close", getIclose())
		);

		//查询条件以及模型不通用

		Table sell = cciTable.where(and(t -> t.numberColumn("cci_model").isGreaterThan(100)));
		Table buy = cciTable.where(and(t -> t.numberColumn("cci_model").isLessThan(-100)));

		//以下内容，通用
		String[] cols = new String[]{"cci_model","code","close"};
		Table buyTable = makeBuyTag(buy,cols);
		Table sellTable = makeSellTag(sell,cols);
		Table result = cciTable.joinOn("date").leftOuter(buyTable, sellTable);

//		System.out.println(result.printAll());
		tradeByTable(result, code,tradeView);
	}


	public void tradeByTable(Table result, String code, DfcfTradeView tradeView) {
		Table rows = result.dropDuplicateRows();
		System.out.println(rows.print(1000));
		int i = result.rowCount();
		// 遍历每一行并打印行数据
		int rowCount = result.rowCount();
		double begin = 20000;
		//余额
		List<DfcfTradeOrder> tradeOrderList=new ArrayList<>();
		Long parentId = tradeView.getId();

		for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
			Double openInitBalance;
			int oldPosition=0;
			if(tradeOrderList.isEmpty()){
				 openInitBalance=initialMoney;
				oldPosition=0;
			}else{
				DfcfTradeOrder tradeOrder = tradeOrderList.get(tradeOrderList.size() - 1);
				openInitBalance=tradeOrder.getBalance().doubleValue();
				oldPosition=tradeOrder.getPosition().intValue();
			}

			DfcfTradeOrder tradeOrder = new DfcfTradeOrder();
			tradeOrder.setId(IdWorker.getId());
			Row row = result.row(rowIndex);

			LocalDate date = row.getDate("date");
			Double close = row.getDouble("close");
			Double buy = row.getDouble("buy");
			Double sell = row.getDouble("sell");

			if (buy.isNaN() && sell.isNaN()) {
				continue;
			}

			if (buy.equals(1d)) {
				tradeOrder.setCode(code);

				tradeOrder.setDirection("B");
				BigDecimal closePrice = BigDecimal.valueOf(close * 100);
				tradeOrder.setPrice(BigDecimal.valueOf(close));

				int sub = MoneyUtil.sub(BigDecimal.valueOf(openInitBalance), closePrice);
				if(sub<=0){
					continue;
				}
				Double cost = closePrice.multiply(BigDecimal.valueOf(sub)).doubleValue();
				BigDecimal balance = BigDecimal.valueOf(openInitBalance).subtract(BigDecimal.valueOf(cost));
				tradeOrder.setPosition(BigDecimal.valueOf(sub+oldPosition).longValue());
				tradeOrder.setOpenBalance(BigDecimal.valueOf(openInitBalance));
				tradeOrder.setUnit(100L);
				tradeOrder.setPositionBalance(BigDecimal.valueOf(cost));
				tradeOrder.setBalance(BigDecimal.valueOf(balance.doubleValue()));
				Date date1 = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());

				tradeOrder.setTradeDate(date1);
				tradeOrder.setParentId(parentId);
				tradeOrderList.add(tradeOrder);
				System.out.println("买入：");
				System.out.println(tradeOrder);
			}

			if (sell.equals(-1d)) {

				if (tradeOrderList.isEmpty()) {
					continue;
				}
				DfcfTradeOrder buyOrder = tradeOrderList.get(tradeOrderList.size() - 1);
				if(Objects.isNull(buyOrder)){
					continue;
				}
				if(buyOrder.getDirection().equals("S")){
					continue;
				}
				Long position = buyOrder.getPosition();

				double canGet = position * close*100;
				Double balance1 = buyOrder.getBalance().doubleValue();
//				if(balance1<initialMoney){
//					continue;
//				}
				double userMoney = canGet + balance1;
				tradeOrder.setPosition(oldPosition-position);
				tradeOrder.setPositionBalance(BigDecimal.valueOf(0));
				tradeOrder.setPrice(BigDecimal.valueOf(close));
				tradeOrder.setOpenBalance(buyOrder.getBalance());
				tradeOrder.setUnit(100L);
				tradeOrder.setCode(code);
				tradeOrder.setBalance(BigDecimal.valueOf(userMoney));
				tradeOrder.setDirection("S");
				Date date2 = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());

				tradeOrder.setTradeDate(date2);
				tradeOrder.setParentId(parentId);
				tradeOrderList.add(tradeOrder);
				System.out.println("卖出：");
				System.out.println(tradeOrder);
			}


		}
		int size = tradeOrderList.size();
		System.out.println("交易模型，余额");
		System.out.println("交易次数"+size);
//		System.out.println("最大回撤:");
		System.out.println(tradeOrderList.get(size-1));

		DfcfTradeOrder maxOrder = tradeOrderList.stream().max(Comparator.comparing(DfcfTradeOrder::getBalance)).get();
		DfcfTradeOrder minOrder = tradeOrderList.stream().min(Comparator.comparing(DfcfTradeOrder::getBalance)).get();

		DfcfTradeOrder lastOrder = tradeOrderList.get(size-1);
		Double costFee = (size - 1)*5d;

		tradeView.setMaxBalance(maxOrder.getBalance());
		tradeView.setMinBalance(minOrder.getBalance());
		tradeView.setTradeTimes(size);
		tradeView.setFeeCost(costFee);
		tradeView.setFinalBalance(lastOrder.getBalance());
		double maxProfit =( maxOrder.getBalance().doubleValue() - initialMoney)/initialMoney;
		double maxCut =( minOrder.getBalance().doubleValue() - initialMoney)/initialMoney;
		tradeView.setMaxCut(maxCut);
		tradeView.setMaxProfit(maxProfit);
		tradeView.setOpenBalance(initialMoney);
		double[] returns = getReturns(tradeOrderList);

		double calculate = SharpUtil.calculate(returns, 0.06);
		tradeView.setSharp(calculate);

		int i1 = tradeViewService.updateDfcfTradeView(tradeView);
		if(i1>0){
			dfcfTradeOrderService.saveBatch(tradeOrderList);
		}
	}

	public double[] getReturns(List<DfcfTradeOrder> tradeOrderList){
		double[] collect = tradeOrderList.stream().map(item -> {
			BigDecimal balance = item.getBalance();
			double v = balance.doubleValue() - initialMoney;
			return v;
		}).mapToDouble(Double::doubleValue).toArray();
		return collect;
	}
	private static Table makeBuyTag(Table result,String... cols) {
		Selection close = result.numberColumn("close").isGreaterThan(0);

		DoubleColumn oldCOL = result.doubleColumn("close");
		// 创建一个Selection对象，并选择所有大于5的行
		DoubleColumn set = oldCOL.set(close, 1d);


		result.addColumns(set.copy().setName("buy"));
		result.removeColumns(cols);
		return result;
	}

	private static Table makeSellTag(Table result, String... cols) {
		Selection close = result.numberColumn("close").isGreaterThan(0);

		DoubleColumn oldCOL = result.doubleColumn("close");
		// 创建一个Selection对象，并选择所有大于5的行
		DoubleColumn set = oldCOL.set(close, -1d);


		result.addColumns(set.copy().setName("sell"));
		result.removeColumns(cols);
		return result;
	}


	public void loadModel(List<DayTaModel> dayTaModels) {
		List<Double> cciVal = dayTaModels.stream().map(item -> item.getVal()).collect(Collectors.toList());
		List<Double> closeVal = dayTaModels.stream().map(item -> item.getClose()).collect(Collectors.toList());
		Stream<LocalDate> dateStream = dayTaModels.stream().map(item -> DateUtils.parseLocalDate(item.getDate()));
		// 创建示例表1
		DateColumn dateCol = DateColumn.create("date", dateStream);
		DoubleColumn cciCol = DoubleColumn.create("cci", cciVal);
		DoubleColumn closeCol = DoubleColumn.create("close", closeVal);
		Table table1 = Table.create("Table1", dateCol, cciCol, closeCol);
		double upper = 120.01d;
		double bottom = -120d;
		Selection sellSelection = table1.numberColumn("cci").isGreaterThan(upper);
		Selection buySelection = table1.numberColumn("cci").isLessThan(bottom);

		Table where = table1.where(sellSelection);

//		// 创建示例表2
//		DateColumn date2 = DateColumn.create("date",new LocalDate[]{LocalDate.parse("20210101", DateTimeFormatter.ISO_LOCAL_DATE), LocalDate.parse("20210102"), LocalDate.parse("2021-01-04")});
//		DoubleColumn price2 = DoubleColumn.create("open", new double[]{102.0, 105.5, 108.2});
//		Table table2 = Table.create("Table2", date2, price2);
//
//		// 按日期合并表格
//		Table mergedTable = table1.joinOn("date").inner(table2);

	}


}
