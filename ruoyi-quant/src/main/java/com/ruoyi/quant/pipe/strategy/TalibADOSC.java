package com.ruoyi.quant.pipe.strategy;


import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.domain.models.DayTaModel;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.common.IMiddleware;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.service.CommonTaService;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("talibadosc")
public class TalibADOSC extends IMiddleware {
	@Autowired
	CommonTaService<DayTaModel> taModelService;

	private static final String type="talib-adsoc";
	@Override
	public void handle(PipeContext ctx) {
		List<StockQuote> list = (List<StockQuote>) ctx.get("quotes");
		AnalysisQuery analysisQuery = (AnalysisQuery) ctx.get("AnalysisQuery");

		List<DayTaModel> adosc = ADOSC(analysisQuery, list);

		Long code = clear(analysisQuery.getCode());
		System.out.println(code);
		taModelService.saveAll(adosc);
		ctx.set("res", adosc);
	}

	public Long clear(String code){
		Map<String,Object> param=new HashMap<>();
		param.put("code",code);
		param.put("type",type);
		Long result = taModelService.deleteByMap(param, DayTaModel.class);
		return result;
	}


	public List<DayTaModel> ADOSC(AnalysisQuery analysisQuery, List<StockQuote> list) {
		String code = analysisQuery.getCode();
		//震荡指标
		int size = list.size();
		//数据存在
		if (list != null) {
			//参数准备
			//收盘价的数组
			double[] closePrice = new double[list.size()];
			double[] highPrice = new double[list.size()];
			double[] lowPrice = new double[list.size()];
			double[] volPack = new double[list.size()];
			//移动平均线数值的数组
			//获取收盘价数组 length = 2580
			for (int i = 0; i < size; i++) {
				double close = list.get(i).getClose();
				double high = list.get(i).getHigh();
				double low = list.get(i).getLow();
				double vol = list.get(i).getVol();

				closePrice[i] = close;
				highPrice[i] = high;
				lowPrice[i] = low;
				volPack[i] = vol;
			}

			int fastPeriod = 3;
			int slowPeriod = 6;

			// Output parameters
			double[] adosc = new double[closePrice.length];

			// Calculate ADOSC
			Core core = new Core();
			MInteger outBegIdx = new MInteger();
			MInteger outNbElement = new MInteger();
			RetCode retCode = core.adOsc(0, closePrice.length - 1, highPrice, lowPrice, closePrice, volPack, fastPeriod, slowPeriod, outBegIdx, outNbElement, adosc);
			List<DayTaModel> taModels = new ArrayList<>();
			// Print the result
			if (retCode == RetCode.Success) {
				for (int i = outBegIdx.value; i < size; i++) {


					String date = list.get(i).getDate();
					//当日收盘价（保留两位小数）

					DayTaModel taModel = new DayTaModel();

					Double tempClose = closePrice[i];
					//移动平均数（保留两位小数）
					Double tempMovAver = adosc[i];
//                    String tempNBE = String.format("%.6f", out[i - outNBElement.value]);
					//拼接字符串
//                    String line = "Period # " +
//                            i +
//                            " 当日收盘价 = " +
//                            tempClose +
//                            " asosc移动平均数 = " +
//                            tempMovAver
////                            "tempNBE="+tempNBE
//                            ;
//                    System.out.println(line);
					taModel.setCode(code);
					taModel.setDate(date);
					taModel.setClose(tempClose);
					taModel.setVal(tempMovAver);
					taModel.setType(type);
					taModels.add(taModel);
				}
				return taModels;
			} else {
				System.out.println("Failed to calculate ADOSC. Error: " + retCode);
			}
		}


		return new ArrayList<>();
	}

}
