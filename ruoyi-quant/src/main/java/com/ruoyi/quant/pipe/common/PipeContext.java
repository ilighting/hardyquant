package com.ruoyi.quant.pipe.common;


import cn.hutool.core.lang.Dict;
import com.alibaba.ttl.TransmittableThreadLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.Objects;

public class PipeContext extends Dict {
    private static final Logger log = LoggerFactory.getLogger(PipeContext.class);
    private static ThreadLocal<PipeContext> threadLocal=new TransmittableThreadLocal<PipeContext>();

    private PipeException exception;


    public PipeException getException() {
        if(!Objects.isNull(exception)){
            log.error("context 异常{}",exception.getMessage());
            log.error("context 异常trade{}",exception.getStackTrace());
        }

        return exception;
    }

    public void setException(PipeException exception) {
        this.exception = exception;
    }

    public static ThreadLocal<PipeContext> getThreadLocal() {
        PipeContext pipeContext = threadLocal.get();
        if(null== pipeContext){
            pipeContext =new PipeContext();
            threadLocal.set(pipeContext);
        }
        return threadLocal;
    }

    public static PipeContext getContext(){
        PipeContext pipeContext = threadLocal.get();
        if(null== pipeContext){
            pipeContext =new PipeContext();
            threadLocal.set(pipeContext);
        }
        return pipeContext;
    }

    public static PipeContext setContext(PipeContext pipeContext){
        PipeContext local = threadLocal.get();
        if(pipeContext !=local||null==local){
            threadLocal.set(pipeContext);
        }
        return pipeContext;
    }

    public static void setThreadLocal(final ThreadLocal<PipeContext> threadLocal) {

        PipeContext.threadLocal = threadLocal;
    }
}
