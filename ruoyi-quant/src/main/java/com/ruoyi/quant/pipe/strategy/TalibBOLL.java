package com.ruoyi.quant.pipe.strategy;

import com.ruoyi.quant.domain.models.DayTaModel;
import com.ruoyi.quant.pipe.common.IMiddleware;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.service.CommonTaService;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MAType;
import com.tictactec.ta.lib.MInteger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TalibBOLL extends IMiddleware {
    @Autowired
    CommonTaService<DayTaModel> taModelService;

    private static final String type="talib-boll";
    @Override
    public void handle(PipeContext ctx) {

    }

    public Long clear(String code){
        Map<String,Object> param=new HashMap<>();
        param.put("code",code);
        param.put("type",type);
        Long result = taModelService.deleteByMap(param, DayTaModel.class);
        return result;
    }


    public void getBoll(){
        double[] closePrice = {100.5, 99.8, 105.2, 102.0, 105.5, 108.2};
        int period = 3;
        double[] upperBand = new double[closePrice.length];
        double[] middleBand = new double[closePrice.length];
        double[] lowerBand = new double[closePrice.length];
//        int[] outNbElement = new int[1];
//        int[] outBegIdx = new int[1];
        double[] outRealUpperBand = new double[closePrice.length];
        double[] outRealMiddleBand = new double[closePrice.length];
        double[] outRealLowerBand = new double[closePrice.length];
        MInteger outBegIdx=new MInteger();
        MInteger outNbElement=new MInteger();

        Core c = new Core();
        c.bbands(0, closePrice.length - 1, closePrice, period, 2.0, 2.0, MAType.Sma, outNbElement, outBegIdx, outRealUpperBand, outRealMiddleBand, outRealLowerBand);

        System.arraycopy(outRealUpperBand, 0, upperBand, outBegIdx.value, outNbElement.value);
        System.arraycopy(outRealMiddleBand, 0, middleBand, outBegIdx.value, outNbElement.value);
        System.arraycopy(outRealLowerBand, 0, lowerBand, outBegIdx.value, outNbElement.value);

        for (int i = outBegIdx.value; i < closePrice.length; i++) {
            System.out.println("Time: " + (i + outBegIdx.value)
                    + ", Upper Band: " + upperBand[i]
                    + ", Middle Band: " + middleBand[i]
                    + ", Lower Band: " + lowerBand[i]);
        }

    }
}
