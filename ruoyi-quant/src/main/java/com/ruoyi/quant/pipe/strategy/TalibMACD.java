package com.ruoyi.quant.pipe.strategy;


import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.domain.models.DayTaModel;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.pipe.strategy.commons.BaseStrategy;
import com.ruoyi.quant.tdx.TdxCategory;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "talib_macd")
public class TalibMACD extends BaseStrategy {

	private static final String type="talib-macd";
	@Override
	public void handle(final PipeContext ctx) {
		List<StockQuote> list = (List<StockQuote>) ctx.get("quotes");
		AnalysisQuery analysisQuery = (AnalysisQuery) ctx.get("AnalysisQuery");

		List<DayTaModel> adosc = macd(analysisQuery, list);
		clear(analysisQuery.getCode());
		taModelService.saveAll(adosc);
		ctx.set("res", adosc);
	}

	public Long clear(String code){
		Map<String,Object> param=new HashMap<>();
		param.put("code",code);
		param.put("type",type);
		Long result = taModelService.deleteByMap(param, DayTaModel.class);
		return result;
	}


	public List<DayTaModel> macd(AnalysisQuery analysisQuery, List<StockQuote> list) {
// 示例数据：收盘价数组
		int size = list.size();
		double[] closePrice = new double[size];

		//获取收盘价数组 length = 2580
		for (int i = 0; i < list.size(); i++) {
			double close = list.get(i).getClose();
			closePrice[i] = close;
		}


		// 创建 Core 对象
		Core core = new Core();

		// 计算 MACD
		int beginIndex = 0; // 起始索引
		int endIndex = closePrice.length - 1; // 结束索引
		int optInFastPeriod = 12; // 快速移动平均线期间数
		int optInSlowPeriod = 26; // 慢速移动平均线期间数
		int optInSignalPeriod = 9; // MACD信号线期间数

		// 分配输出内存
		MInteger outBegIdx = new MInteger();
		MInteger outNbElement = new MInteger();
		double outMACD[] = new double[closePrice.length];
		double outMACDSignal[] = new double[closePrice.length];
		double outMACDHist[] = new double[closePrice.length];

		// 调用 talib4j 的 MACD 方法计算 MACD 指标
		core.macd(beginIndex, endIndex, closePrice, optInFastPeriod, optInSlowPeriod, optInSignalPeriod,
				outBegIdx, outNbElement, outMACD, outMACDSignal, outMACDHist);

		if (analysisQuery.getTdxCategory().equals(TdxCategory.day)) {
			List<DayTaModel> taModels = new ArrayList<>();
			// 打印计算结果

			for (int i = 0; i < closePrice.length; i++) {
				DayTaModel taModel = new DayTaModel();
				StockQuote stockQuote = list.get(i);
				taModel.setCode(analysisQuery.getCode());
				taModel.setDate(stockQuote.getDate());
				taModel.setClose(stockQuote.getClose());
				taModel.setMaxVal(outMACDHist[i]);
				taModel.setMinVal(outMACDSignal[i]);
				taModel.setVal(outMACD[i]);
				taModel.setType(type);
				taModels.add(taModel);
			}

			return taModels;
		}

		return new ArrayList<>();
	}
}
