package com.ruoyi.quant.pipe.common;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author sxq
 */
@Data
@AllArgsConstructor
public class PipeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	/**
	 * 错误码
	 */
	private Integer code;

	/**
	 * 错误提示
	 */
	private String message;

	private String err;

	private PipeErr pipeErr;

	public PipeException(final PipeErr err, final String errMsg) {
		this.setCode(err.getCode());
		this.setMessage(err.getMsg());
		this.setMessage(errMsg);
	}

	public PipeException(final String message) {
		this.setMessage(message);
	}

	public PipeException(final String message, final int error, final String err) {
		this.setCode(error);
		this.setMessage(message);
		this.setErr(err);
	}


	public void error() throws Exception {
		throw this;
	}

}
