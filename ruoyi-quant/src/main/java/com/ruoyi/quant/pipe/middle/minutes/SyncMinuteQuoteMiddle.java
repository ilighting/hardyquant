package com.ruoyi.quant.pipe.middle.minutes;

import cn.hutool.core.date.DateUtil;

import com.ruoyi.quant.domain.MinuteQuote;
import com.ruoyi.quant.domain.Stocks;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.pipe.common.IMiddleware;
import com.ruoyi.quant.service.StockQuoteService;
import com.ruoyi.quant.service.StocksService;
import com.ruoyi.quant.tdx.Market;
import com.ruoyi.quant.tdx.TdxCategory;
import com.ruoyi.quant.tdx.TdxClientFactory;
import com.ruoyi.quant.tdx.domain.Quote;
import com.ruoyi.quant.utils.MarketUtils;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

@Service
public class SyncMinuteQuoteMiddle extends IMiddleware {
    private static final Logger log = LoggerFactory.getLogger(SyncMinuteQuoteMiddle.class);
    @Autowired
    StockQuoteService<MinuteQuote> quoteService;
    @Autowired
    StocksService stocksService;

    @SneakyThrows
    @Override
    public void handle(PipeContext ctx) {
        TdxClientFactory service = TdxClientFactory.getInstance(3);

        Stocks stocks= (Stocks) ctx.get("stocks");
        log.info("开始同步行情代码");
        AnalysisQuery analysisQuery = (AnalysisQuery) ctx.get("AnalysisQuery");
        String date = analysisQuery.getDate();
        Market marketEnum = Market.valueOf(stocks.getMarket());
        TdxCategory anEnum = analysisQuery.getTdxCategory();
        String stockCode = stocks.getCode();
        String stockName = stocks.getName();
        Long quoteCount = count(date,stockCode, stockName);



        if (quoteCount>0) {
            MinuteQuote latest = getLatest(date,stockCode,quoteCount);

            if(MarketUtils.isMarketOpen()){
                Long l2 = MarketUtils.calcMinuteDate(latest.getDate(), DateUtil.now());
                //增量更新
                Future<List<Quote>> quotes = service.getQuotes(anEnum, marketEnum, stockCode,0, l2.intValue());
                List<Quote> quoteList = quotes.get();
                if(quoteList.size()>0){
                    quoteService.saveMinute(quoteList, stockCode);
                }
            }
        } else {
            //全量更新
            int count = 1;
            int pageSize = 480;
            int i = 0;
            List<Quote> total = new ArrayList<>();
            while (i <= count) {
                Future<List<Quote>> quotes = service.getQuotes(anEnum, marketEnum, stockCode, i, pageSize);
                i = i + pageSize;
                List<Quote> quoteList = quotes.get();
                if(quoteList.isEmpty()){
                    continue;
                }
                total.addAll(quoteList);
            }
            if (total.size() > 0) {
                total.sort((o1, o2) -> DateUtil.date(Long.parseLong(o1.getDate())).after(DateUtil.date(Long.parseLong(o2.getDate()))) ? 1 : -1);
                quoteService.saveMinute(total, stockCode);
            }else{
                log.error("沒有行情");
            }
        }

    }

    public Long count(String date,String code, String name) {
        MinuteQuote stocks=new MinuteQuote();
        stocks.setCode(code.trim());
        stocks.setDate("^"+date);
        long count = quoteService.count(stocks);
        return count;
    }

    public MinuteQuote getLatest(String date,String code,Long count){

        MinuteQuote stockQuote = new MinuteQuote();
        stockQuote.setCode(code);
        stockQuote.setDate("^"+date);
        List<MinuteQuote> list = quoteService.pageList(stockQuote,count.intValue(),1);
        if (list.size()>0) {
            return list.get(0);
        }
        return null;
    }

}
