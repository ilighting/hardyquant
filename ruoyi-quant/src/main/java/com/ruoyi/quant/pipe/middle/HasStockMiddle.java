package com.ruoyi.quant.pipe.middle;


import com.ruoyi.quant.domain.Stocks;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.pipe.common.IMiddleware;
import com.ruoyi.quant.service.StocksService;
import com.ruoyi.quant.utils.Err;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class HasStockMiddle extends IMiddleware {
    private static final Logger log= LoggerFactory.getLogger(HasStockMiddle.class);
    @Autowired
    StocksService stocksService;
    @Override
    public void handle(PipeContext ctx) {
        AnalysisQuery analysisQuery = (AnalysisQuery) ctx.get("AnalysisQuery");
        String code = analysisQuery.getCode();
        String name =analysisQuery.getName();

        Stocks stocks=new Stocks();
        stocks.setCode(code.trim());
        if(StringUtils.isNoneEmpty(name)){
            stocks.setName(name.trim());
        }

        Stocks one = stocksService.getOne(stocks);
        if (Objects.isNull(one)) {
            Err.error("股票不存在");
        }else{
            ctx.set("stocks",one);
        }
    }
}
