package com.ruoyi.quant.pipe;


import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.common.Pipe;
import com.ruoyi.quant.pipe.middle.HasStockMiddle;
import com.ruoyi.quant.pipe.middle.LoadQuotesMiddle;
import com.ruoyi.quant.pipe.middle.TalibExcutionMiddle;
import com.ruoyi.quant.pipe.middle.days.SyncQuoteMiddle;
import com.ruoyi.quant.pipe.middle.minutes.LoadMinuteQuotesMiddle;
import com.ruoyi.quant.pipe.middle.minutes.SyncMinuteQuoteMiddle;
import org.springframework.stereotype.Service;

@Service
public class AnalysisPipeService {


    public void workDay(AnalysisQuery query){
        Pipe.line()
                .set("AnalysisQuery",query)
                //加载股票数据
                .middle(LoadQuotesMiddle.class)
                //执行策略代码
                .middle(TalibExcutionMiddle.class)
                .run();
    }


    public void workMinute(AnalysisQuery query){
        Pipe.line()
                .set("AnalysisQuery",query)
                //检查股票数据
                .middle(HasStockMiddle.class)
                //增量更新股票行情数据
                .middle(SyncMinuteQuoteMiddle.class)
                //加载股票数据
                .middle(LoadMinuteQuotesMiddle.class)
                //执行策略代码
                .middle(TalibExcutionMiddle.class)
                .run();
    }


    public void quoteUpdate(AnalysisQuery query){
        Pipe.line()
                .set("AnalysisQuery",query)
                //检查股票数据
                .middle(HasStockMiddle.class)
                //增量更新股票行情数据
                .middle(SyncQuoteMiddle.class)

                .run();
    }

    /**
     * 分钟行情只获取当前日期
     * 如果需要查询特定日期，需要根据当前日期，往前推算n*240
     *
     * @param query
     */
    public void updateMinute(AnalysisQuery query){
        Pipe.line()
                .set("AnalysisQuery",query)
                //检查股票数据
                .middle(HasStockMiddle.class)
                //增量更新股票行情数据
                .middle(SyncMinuteQuoteMiddle.class)

                .run();
    }


}
