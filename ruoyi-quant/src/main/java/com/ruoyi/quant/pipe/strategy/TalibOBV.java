package com.ruoyi.quant.pipe.strategy;

import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.domain.models.DayTaModel;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.pipe.strategy.commons.BaseStrategy;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TalibOBV extends BaseStrategy {

    private static final String type="talib-obv";
    private static final Logger log= LoggerFactory.getLogger(TalibOBV.class);

    @Override
    public void handle(PipeContext ctx) {
        List<StockQuote> list = (List<StockQuote>) ctx.get("quotes");
        AnalysisQuery analysisQuery = (AnalysisQuery) ctx.get("AnalysisQuery");

        List<DayTaModel> obvList = getOBV(analysisQuery, list);
        clear(analysisQuery.getCode());
        taModelService.saveAll(obvList);
        ctx.set("res",obvList);
    }

    public Long clear(String code){
        Map<String,Object> param=new HashMap<>();
        param.put("code",code);
        param.put("type",type);
        Long result = taModelService.deleteByMap(param, DayTaModel.class);
        return result;
    }

    public List<DayTaModel>  getOBV(AnalysisQuery analysisQuery,List<StockQuote> list){

        // 创建 Core 实例
        Core core = new Core();
        int size = list.size();
        // 创建输入数据
        double[] inClose = new double[size];
        double[] inVolume = new double[size];
        //获取收盘价数组 length = 2580
        for (int i = 0; i < list.size(); i++) {
            double close =  list.get(i).getClose();

            double vol =  list.get(i).getVol();

            inClose[i] = close;

            inVolume[i]=vol;
        }
        // 创建输出数据
        double[] outObv = new double[inClose.length];

        // 创建变量以保存计算结果的返回代码
        MInteger outBegIdx = new MInteger();
        MInteger outNbElement = new MInteger();
        RetCode retCode;

        // 计算 OBV
        retCode = core.obv(0, inClose.length - 1, inClose, inVolume, outBegIdx, outNbElement, outObv);
        List<DayTaModel> cciModels=new ArrayList<>();
        if (retCode == RetCode.Success) {
            System.out.println("OBV:");
            for (int i = 0; i < outNbElement.value; i++) {
                StockQuote stockQuote = list.get(i);
                DayTaModel cciModel=new DayTaModel();
                cciModel.setCode(analysisQuery.getCode());
                cciModel.setDate(stockQuote.getDate());
                cciModel.setClose(stockQuote.getClose());
                cciModel.setVal(outObv[i]);
                cciModel.setType("talibobv");
                cciModels.add(cciModel);
                System.out.println(outObv[i]);
            }
        }
        return cciModels;
    }
}
