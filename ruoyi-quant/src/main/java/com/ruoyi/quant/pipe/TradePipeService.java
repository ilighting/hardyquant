package com.ruoyi.quant.pipe;

import com.ruoyi.quant.domain.TradeModel;
import com.ruoyi.quant.pipe.common.Pipe;
import com.ruoyi.quant.pipe.strategy.trade.MakeTradeOrderByModel;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeView;
import org.springframework.stereotype.Service;

@Service
public class TradePipeService {


	public void cciTradeDemo(TradeModel tradeModel, final DfcfTradeView view) {



		Pipe.line()
				.set("TradeModel",tradeModel)
				.set("View",view)
				.middle(MakeTradeOrderByModel.class)
				.run();
	}




}
