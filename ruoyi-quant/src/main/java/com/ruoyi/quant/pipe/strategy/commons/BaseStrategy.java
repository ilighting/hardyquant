package com.ruoyi.quant.pipe.strategy.commons;


import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.pipe.common.IMiddleware;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.service.CommonTaService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class BaseStrategy extends IMiddleware {
    @Autowired
    public CommonTaService taModelService;


    public List<StockQuote> list;

    private double[] close;
    private double[] high;
    private double[] low;
    private double[] open;
    private double[] volume;
    private double[] amount;

    private Integer size;

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }


    public void before(List<StockQuote> stockQuotes ) {
        setList(stockQuotes);
        setSize(stockQuotes.size());
        int size = stockQuotes.size();

        try{
            double close[]  = new double[size];
            double high[]   = new double[size];
            double low[]    = new double[size];
            double open[]   = new double[size];
            double volume[] = new double[size];
            double amount[] = new double[size];

        //移动平均线数值的数组
        //获取收盘价数组 length = 2580
        for (int i = 0; i < size; i++) {


            close[i]  = list.get(i).getClose();
            high[i]   = list.get(i).getHigh();
            low[i]    = list.get(i).getOpen();
            open[i]   = list.get(i).getLow();
            volume[i] = list.get(i).getVol();
            amount[i] = list.get(i).getAmt();

            setClose(close);
            setHigh(high);
            setLow(low);
            setOpen(open);
            setVolume(volume);
            setAmount(amount);
        }
        }catch (Exception e){
            log.error(e.getMessage());
        }
    }

    @Override
    public abstract void handle(final PipeContext ctx);

    public double[] getClose() {
        return close;
    }

    public void setClose(double[] close) {
        this.close = close;
    }

    public double[] getHigh() {
        return high;
    }

    public void setHigh(double[] high) {
        this.high = high;
    }

    public double[] getLow() {
        return low;
    }

    public void setLow(double[] low) {
        this.low = low;
    }

    public double[] getOpen() {
        return open;
    }

    public void setOpen(double[] open) {
        this.open = open;
    }

    public double[] getVolume() {
        return volume;
    }

    public void setVolume(double[] volume) {
        this.volume = volume;
    }

    public double[] getAmount() {
        return amount;
    }

    public void setAmount(double[] amount) {
        this.amount = amount;
    }

    public List<StockQuote> getList() {
        return list;
    }

    public void setList(List<StockQuote> list) {
        this.list = list;
    }
}
