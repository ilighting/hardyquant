package com.ruoyi.quant.pipe.middle.days;

import cn.hutool.core.date.DateUtil;

import com.ruoyi.quant.basedata.service.IStockCodeService;
import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.domain.Stocks;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.pipe.common.IMiddleware;
import com.ruoyi.quant.service.StockQuoteService;
import com.ruoyi.quant.service.StocksService;
import com.ruoyi.quant.tdx.TdxCategory;
import com.ruoyi.quant.tdx.Market;
import com.ruoyi.quant.tdx.TdxClientFactory;
import com.ruoyi.quant.tdx.domain.Quote;
import com.ruoyi.quant.utils.MarketUtils;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

@Service
public class SyncQuoteMiddle extends IMiddleware {
    private static final Logger log = LoggerFactory.getLogger(SyncQuoteMiddle.class);

    @Autowired
    StockQuoteService quoteService;

    @Autowired
    StocksService stocksService;

    @SneakyThrows
    @Override
    public void handle(PipeContext ctx) {
        TdxClientFactory service = TdxClientFactory.getInstance(1);

        Stocks stocks= (Stocks) ctx.get("stocks");
        log.info("开始同步行情代码");
        AnalysisQuery analysisQuery = (AnalysisQuery) ctx.get("AnalysisQuery");
        Market marketEnum = Market.valueOf(stocks.getMarket());
        TdxCategory anEnum = analysisQuery.getTdxCategory();
        String stockCode = stocks.getCode();
        String stockName = stocks.getName();
        Long quoteCount = count(stockCode);

        if (quoteCount>0) {
            StockQuote latest = getLatest(stockCode, stockName,quoteCount);

            Long l = MarketUtils.calcDate(latest.getDate(), DateUtil.now());
            if(MarketUtils.isMarketOpen()) {
                //增量更新
                Future<List<Quote>> quotes = service.getQuotes(anEnum, marketEnum, stockCode, 0, l.intValue());
                List<Quote> quoteList = quotes.get();
                if (quoteList.size() > 0) {
                    quoteService.saveQuotes(quoteList, stockCode);
                }
            }
        } else {
            //全量更新
            int count = 2000;
            int pageSize = 500;
            int i = 0;
            List<Quote> total = new ArrayList<>();
            while (i <= count) {
                Future<List<Quote>> quotes = service.getQuotes(anEnum, marketEnum, stockCode, i, pageSize);
                i = i + pageSize;
                List<Quote> quoteList = quotes.get();
                if(quoteList.isEmpty()){
                    continue;
                }
                total.addAll(quoteList);
            }
            if (total.size() > 0) {
               try{
                   total.sort((o1, o2) -> DateUtil.date(Long.parseLong(o1.getDate())).after(DateUtil.date(Long.parseLong(o2.getDate()))) ? 1 : -1);
                   quoteService.saveQuotes(total, stockCode);
               }catch (Exception e){

               }


            }
        }
    }

    public Long count(String code) {
        StockQuote stocks=new StockQuote();
        stocks.setCode(code.trim());
        long count = quoteService.count(stocks);
        return count;
    }

    public StockQuote getLatest(String code,String name,Long count){

        StockQuote stockQuote = new StockQuote();
        stockQuote.setCode(code);
        List<StockQuote> list = quoteService.pageList(stockQuote,count.intValue(),1);
        if (list.size()>0) {
            return list.get(0);
        }
        return null;
    }

}
