package com.ruoyi.quant.pipe.common;

public enum PipeErr {

	DEMO(101,"测试");

	private Integer code;
	private String msg;


	PipeErr(final Integer pipeCode, final String msg) {
		this.code=pipeCode;
		this.msg=msg;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(final Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(final String msg) {
		this.msg = msg;
	}
}
