package com.ruoyi.quant.pipe.common;


import com.ruoyi.quant.utils.Err;
import com.ruoyi.quant.utils.TdxSpringUtils;
import org.springframework.beans.BeansException;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;

public class Pipe {

	protected List<IMiddleware> middleList = new LinkedList<>();

	protected PipeContext pipeContext;


	public static Pipe line() {
		PipeContext pipeContext = new PipeContext();
		return line(pipeContext);
	}

	public static Pipe line(PipeContext pipeContext) {
		Pipe pipe = new Pipe();
		pipe.pipeContext = pipeContext;
		return pipe;
	}

	public Pipe middle(String bean) {
		try {
			Object clazz = TdxSpringUtils.getBean(bean);
			if (clazz instanceof IMiddleware) {
				if (!middleList.contains(clazz)) {
					middleList.add((IMiddleware) clazz);
				}
			}
		} catch (BeansException ex) {
			return this;
		}
		return this;
	}

	public Pipe middle(Class<?> bean) {
		try {
			Object clazz = TdxSpringUtils.getBean(bean);
			if (clazz instanceof IMiddleware) {
				if (!middleList.contains(clazz)) {
					middleList.add((IMiddleware) clazz);
				}
			}
		} catch (BeansException ex) {
			return this;
		}
		return this;
	}

	public <T> Pipe middle(T bean) {
		if (bean instanceof IMiddleware) {
			if (!middleList.contains(bean)) {
				middleList.add((IMiddleware) bean);
			}
		}
		return this;
	}

	public Pipe middles(Class<?>... obj) {
		Arrays.stream(obj).forEach(this::middle);
		return this;
	}

	public Pipe then(Class<?> bean) {
		try {
			Object clazz = TdxSpringUtils.getBean(bean);
			if (clazz instanceof IMiddleware) {
				((IMiddleware) clazz).handle(pipeContext);
			}
		} catch (BeansException ex) {
			return this;
		}
		return this;
	}

	public Pipe apply(BiConsumer<PipeContext, PipeContext> bean) {
		try {
			bean.accept(pipeContext, pipeContext);
		} catch (BeansException ex) {
			return this;
		}
		return this;
	}


	public Pipe run() {
		try {
			middleList.forEach(middle -> middle.handle(pipeContext));
		} catch (PipeException ex) {
			pipeContext.setException(ex);
		}
		return this;
	}

	public Pipe parallelRun() {
		try {
			middleList.parallelStream().forEachOrdered(middle -> middle.handle(pipeContext));
		} catch (PipeException ex) {
			pipeContext.setException(ex);
			Err.error(ex);
		}
		return this;
	}


	/**
	 * 分支
	 *
	 * @return
	 */
	public static Pipe fork() {
		PipeContext pipeContext = PipeContext.getContext();
		return line(pipeContext);
	}

	/**
	 * 分支
	 *
	 * @return
	 */
	public PipeContext fork(Class<?> bean) {
		PipeContext pipeContext = PipeContext.getContext();
		line(pipeContext).middle(bean).parallelRun();
		return pipeContext;
	}

	public Pipe error() {
		if (this.pipeContext.getException() != null) {
			throw pipeContext.getException();
		}
		return this;
	}

	public PipeContext context() {
		if (Objects.isNull(this.pipeContext)) {
			this.pipeContext = PipeContext.getContext();
		}
		return this.pipeContext;
	}

	public Pipe set(String key, Object value) {
		pipeContext.set(key, value);
		return this;
	}


}
