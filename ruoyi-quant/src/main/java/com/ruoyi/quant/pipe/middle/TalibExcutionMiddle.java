package com.ruoyi.quant.pipe.middle;

import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.common.IMiddleware;
import com.ruoyi.quant.pipe.common.Pipe;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.service.CommonTaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TalibExcutionMiddle extends IMiddleware {

	@Override
	public void handle(PipeContext ctx) {
		AnalysisQuery analysisQuery = (AnalysisQuery) ctx.get("AnalysisQuery");
		List<String> talibName = analysisQuery.getTalibNames();
		Pipe line = Pipe.line(ctx);
		for (String s : talibName) {
			line.middle(s);
		}
		line.run().context();
	}
}
