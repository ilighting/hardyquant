package com.ruoyi.quant.pipe.strategy.commons;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.domain.models.DayTaModel;
import com.ruoyi.quant.pipe.common.IMiddleware;
import com.ruoyi.quant.pipe.common.PipeContext;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class BaseTablesaw extends IMiddleware {



	public List<DayTaModel> list;
	private List<LocalDate> dates=new ArrayList<>();
	private List<String> code=new ArrayList<>();
	private double[] iclose;
	private double[] ival;
	private double[] imin;
	private double[] imax;

	private Integer size;

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}


	public void before(List<DayTaModel> stockQuotes ) {
		setList(stockQuotes);
		int size = stockQuotes.size();
		setSize(size);
		double[] iclose=new double[size];
		double[] ival=new double[size];
		double[] imin=new double[size];
		double[] imax=new double[size];
		List<String> codeList=new ArrayList<>();
		List<LocalDate> dateList=new ArrayList<>();


		//移动平均线数值的数组
		//获取收盘价数组 length = 2580
		for (int i = 0; i < size; i++) {
			iclose[i] = list.get(i).getClose();
			ival[i] = list.get(i).getVal();
			if(!Objects.isNull(list.get(i).getMaxVal())){
				imax[i] = list.get(i).getMaxVal();
			}
			if(!Objects.isNull(list.get(i).getMinVal())){
				imin[i] = list.get(i).getMinVal();
			}
			codeList.add(list.get(i).getCode());
			dateList.add(DateUtils.parseLocalDate(list.get(i).getDate()));
		}
		setIclose(iclose);
		setImax(imax);
		setImin(imin);
		setIval(ival);
		setDates(dateList);
		setCode(codeList);
	}

	@Override
	public abstract void handle(final PipeContext ctx);

	public List<String> getCode() {
		return code;
	}

	public void setCode(final List<String> code) {
		this.code = code;
	}

	public List<LocalDate> getDates() {
		return dates;
	}

	public void setDates(final List<LocalDate> dates) {
		this.dates = dates;
	}

	public List<DayTaModel> getList() {
		return list;
	}

	public void setList(List<DayTaModel> list) {
		this.list = list;
	}

	public double[] getIclose() {
		return iclose;
	}

	public void setIclose(final double[] iclose) {
		this.iclose = iclose;
	}

	public double[] getIval() {
		return ival;
	}

	public void setIval(final double[] ival) {
		this.ival = ival;
	}

	public double[] getImin() {
		return imin;
	}

	public void setImin(final double[] imin) {
		this.imin = imin;
	}

	public double[] getImax() {
		return imax;
	}

	public void setImax(final double[] imax) {
		this.imax = imax;
	}
}
