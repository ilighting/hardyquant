package com.ruoyi.quant.pipe.strategy;

import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.domain.models.DayTaModel;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.pipe.strategy.commons.BaseStrategy;
import com.ruoyi.quant.service.CommonTaService;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("talib_ad")
public class TalibAD extends BaseStrategy {

    private static final String type="talib-ad";
    @Autowired
    CommonTaService<DayTaModel> taModelService;

    @Override
    public void handle(PipeContext ctx) {
        List<StockQuote> list = (List<StockQuote>) ctx.get("quotes");
        super.before(list);

        AnalysisQuery analysisQuery = (AnalysisQuery) ctx.get("AnalysisQuery");

        List<DayTaModel> adosc = getAD(analysisQuery);
        clear(analysisQuery.getCode());
        taModelService.saveAll(adosc);
        ctx.set("res", adosc);
    }
    public Long clear(String code){
        Map<String,Object> param=new HashMap<>();
        param.put("code",code);
        param.put("type",type);
        Long result = taModelService.deleteByMap(param, DayTaModel.class);
        return result;
    }

    public List<DayTaModel> getAD(AnalysisQuery analysisQuery){


        // 创建 Core 实例
        Core core = new Core();

        // 创建输入数据
        double[] inClose = getClose();
        double[] inVolume = getVolume();

        // 创建输出数据
        double[] outObv = new double[getSize()];

        // 创建变量以保存计算结果的返回代码
        MInteger outBegIdx = new MInteger();
        MInteger outNbElement = new MInteger();
        RetCode retCode;

        // 计算 OBV
        retCode = core.obv(0, getSize() - 1, inClose, inVolume, outBegIdx, outNbElement, outObv);
        List<DayTaModel> taModels=new ArrayList<>();
        if (retCode == RetCode.Success) {
            System.out.println("ad:");
            for (int i = 0; i < outNbElement.value; i++) {
                System.out.println(outObv[i]);
                DayTaModel taModel = new DayTaModel();
                StockQuote stockQuote = list.get(i);
                taModel.setCode(analysisQuery.getCode());
                taModel.setDate(stockQuote.getDate());
                taModel.setClose(stockQuote.getClose());

                taModel.setVal(outObv[i]);
                taModel.setType(type);
                taModels.add(taModel);
            }
        }
        return taModels;
    }
}
