package com.ruoyi.quant.pipe.strategy;


import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.domain.models.DayTaModel;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.pipe.strategy.commons.BaseStrategy;
import com.ruoyi.quant.service.CommonTaService;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("talibcci")
public class TalibCCI  extends BaseStrategy {

	private static final String type="talib-cci";
	@Autowired
	CommonTaService<DayTaModel> taModelService;

	@Override
	public void handle(final PipeContext ctx) {
		List<StockQuote> list = (List<StockQuote>) ctx.get("quotes");
		AnalysisQuery analysisQuery = (AnalysisQuery) ctx.get("AnalysisQuery");

		List<DayTaModel> adosc = cci(analysisQuery, list);
		clear(analysisQuery.getCode());
		taModelService.saveAll(adosc);
		ctx.set("res",adosc);
	}
	public Long clear(String code){
		Map<String,Object> param=new HashMap<>();
		param.put("code",code);
		param.put("type",type);
		Long result = taModelService.deleteByMap(param, DayTaModel.class);
		return result;
	}



	public List<DayTaModel> cci(AnalysisQuery analysisQuery , List<StockQuote> list){
		super.before(list);
		Integer size = getSize();
		int PERIODS_AVERAGE=20;
		//数据存在
		if (size>0){
			//参数准备
			//收盘价的数组
			double[] closePrice =getClose();
			double[] highPrice = getHigh();
			double[] lowPrice = getLow();
			double[] volPack = getVolume();
			//移动平均线数值的数组
			double[] out = new double[getSize()];
			//MInteger类: mutable integer，可变整数
			MInteger begin = new MInteger();
			MInteger length = new MInteger();


			//Talib的核心类
			Core core = new Core();
			//调用核心类
			RetCode retCode = core.cci(
					//int startIdx：开始的索引
					0,
					//int endIdx：结束的索引
					closePrice.length - 1,
					//高价集合
					highPrice,

					//低价集合
					lowPrice,
					//收盘价集合
					closePrice,
					//移动平均的周期数，如 MA20
					PERIODS_AVERAGE,
					//如 MA20 线开始的索引值
					begin,
					//如 MA20 线的长度
					length,
					//当日的 MA20 移动平均线的数值
					out
			);

			//打印信息
			if (retCode == RetCode.Success) {

				//数值上 = PERIODS_AVERAGE-1
				System.out.println("输出开始的周期数: " + begin.value);
				//总周期数
				System.out.println("输出结束的周期数: " + (begin.value + length.value - 1));
				List<DayTaModel> cciModels=new ArrayList<>();
				//遍历有线
				for (int i = begin.value; i < size; i++) {
					//检验当前记录对应的id
					String date = list.get(i).getDate();
					//当日收盘价（保留两位小数）
					Double tempClose = closePrice[i];
					//移动平均数（保留两位小数）
					Double tempMovAver =  out[i - begin.value];


					DayTaModel cciModel=new DayTaModel();
					cciModel.setCode(analysisQuery.getCode());
					cciModel.setDate(date);
					cciModel.setClose(tempClose);
					cciModel.setVal(tempMovAver);
					cciModel.setType("CCI");
					cciModels.add(cciModel);
				}
				return cciModels;
			}
			else {
				System.out.println("Error");
			}

		}
		return new ArrayList<>();
	}

}
