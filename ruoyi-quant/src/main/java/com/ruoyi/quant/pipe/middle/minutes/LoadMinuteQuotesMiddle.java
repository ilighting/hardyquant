package com.ruoyi.quant.pipe.middle.minutes;

import com.ruoyi.quant.domain.MinuteQuote;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.common.IMiddleware;
import com.ruoyi.quant.pipe.common.PipeContext;
import com.ruoyi.quant.service.StockQuoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoadMinuteQuotesMiddle extends IMiddleware {
    private static final Logger log= LoggerFactory.getLogger(LoadMinuteQuotesMiddle.class);


    @Autowired
    StockQuoteService<MinuteQuote> quoteService;

    @Override
    public void handle(PipeContext ctx) {
        log.info("开始同步行情代码");
        AnalysisQuery analysisQuery = (AnalysisQuery) ctx.get("AnalysisQuery");
        String stockCode = analysisQuery.getCode();
        String stockName = analysisQuery.getName();
        Long quoteCount = count(stockCode, stockName);



        if(quoteCount>0){
            //行情
            List<MinuteQuote> list = quoteService.listAll(new MinuteQuote(),analysisQuery.getMgSort());

            ctx.set("quotes",list);
        }
    }

    public Long count(String code, String name) {
        MinuteQuote stocks=new MinuteQuote();
        stocks.setCode(code.trim());
        long count = quoteService.count(stocks);
        return count;
    }
}
