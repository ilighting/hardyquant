package com.ruoyi.quant.utils;


import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.quant.domain.TdxIpConfig;
import com.ruoyi.quant.service.IpConfigService;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class ConfigUtils {

	public static IpConfigService ipConfigService;

	static{
		ipConfigService= SpringUtils.getBean("tdxIpConfigService");
	}
	public static List<TdxIpConfig> getIp(){
		List<TdxIpConfig> list = ipConfigService.listAll(new TdxIpConfig());
		return list;
	}
//
//	public static File getNiceIp(){
//		String property = System.getProperty("user.dir");
//
//		Path config = Paths.get(property, "config", "niceConfig.json");
//		if (!config.toFile().exists()) {
//			try {
//				config.toFile().createNewFile();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//		return config.toFile();
//	}
//	public static File getBadIp(){
//		String property = System.getProperty("user.dir");
//		Path config = Paths.get(property, "config", "badConfig.json");
//
//		return config.toFile();
//	}
//
//	public static List<ConfigUtils.IpConfig> readNiceConfig(){
//		File configFile = getNiceIp();
//		try {
//			// 读取JSON文件并将其映射到Java对象
//			List<ConfigUtils.IpConfig> yourObject = objectMapper.readValue(configFile, new TypeReference<>() {
//			});
//			// 使用yourObject进行后续操作
//			return yourObject;
//		} catch (IOException e) {
//
//		}
//		return new ArrayList<>();
//	}
//	public static List<ConfigUtils.IpConfig> writeNiceConfig(List<ConfigUtils.IpConfig> configs){
//		List<IpConfig> old = readNiceConfig();
//		configs.addAll(old);
//		List<IpConfig> collect = configs.stream().collect(
//				collectingAndThen(
//						toCollection(() -> new TreeSet<>(Comparator.comparing(IpConfig::getIp))), ArrayList::new)
//		);
//		try {
//			File configFile = getNiceIp();
//			objectMapper.writeValue(configFile, collect);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	public static List<ConfigUtils.IpConfig> readBadConfig() {
//		File configFile = getBadIp();
//		try {
//			// 读取JSON文件并将其映射到Java对象
//			List<ConfigUtils.IpConfig> yourObject = objectMapper.readValue(configFile, new TypeReference<>() {
//			});
//			// 使用yourObject进行后续操作
//			return yourObject;
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return new ArrayList<>();
//	}
//
//	public static void writeBadConfig(List<ConfigUtils.IpConfig> configs){
//		List<IpConfig> old = readBadConfig();
//		configs.addAll(old);
//		List<IpConfig> collect = configs.stream().collect(
//				collectingAndThen(
//						toCollection(() -> new TreeSet<>(Comparator.comparing(IpConfig::getIp))), ArrayList::new)
//		);
//		try {
//			File configFile = getBadIp();
//			objectMapper.writeValue(configFile, collect);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}


}
