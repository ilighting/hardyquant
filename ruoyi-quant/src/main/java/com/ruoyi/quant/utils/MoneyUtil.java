package com.ruoyi.quant.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MoneyUtil {

	public static int sub(BigDecimal a,BigDecimal b){
		return a.divide(b, 0, RoundingMode.DOWN).intValue();
	}
}
