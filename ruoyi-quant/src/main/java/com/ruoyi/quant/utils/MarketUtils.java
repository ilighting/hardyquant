package com.ruoyi.quant.utils;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import lombok.SneakyThrows;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MarketUtils {

    public static boolean isMarketOpen() {
        LocalTime currentTime = LocalTime.now();
        LocalTime time15 = LocalTime.of(15, 1);
        LocalTime time9 = LocalTime.of(9, 0);
        Date date = new Date();
        // 判断是否是工作日
        int isWeekend = DateUtil.thisDayOfWeek();

        return currentTime.isAfter(time9) && currentTime.isBefore(time15)&&isWeekend<6;
    }



    @SneakyThrows
    public static Long calcDate(String dateString1, String dateString2) {
//        String dateString1 = "2022-01-01";
//        String dateString2 = "2022-01-10";

        Date date1 = DateUtil.parse(dateString1);
        Date date2 = DateUtil.parse(dateString2);

        long diffInMillies = Math.abs(date2.getTime() - date1.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

        return diff;
    }
    @SneakyThrows
    public static Long calcMinuteDate(String dateString1, String dateString2) {
//        String dateString1 = "2022-01-01";
//        String dateString2 = "2022-01-10";


        Date date1 = parseMinute(dateString1);
        Date date2 = parseMinute(dateString2);



        long diffInMillies = Math.abs(date2.getTime() - date1.getTime());
        long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);

        return diff;
    }
 @SneakyThrows
    public static Long calcMinuteDayDate(String dateString1) {

        Date date1 = parseMinute(dateString1);
        Date date2 = new Date();

        long diffInMillies = Math.abs(date2.getTime() - date1.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        return diff;
    }

    public static Date parseMinute(String dateStr){
        String pattern = "yyyyMMddHHmm";

        // 使用Hutool解析时间字符串
        Date date = DateUtil.parse(dateStr, pattern);

        // 输出结果
        return date;
    }

    public static void main(String[] args) {
        DateTime v1 = DateUtil.parse("2020-01-13");
        DateTime v2 = DateUtil.parse("2020-01-11");
        DateTime v3 = DateUtil.parse("2020-01-12");
        List<DateTime> dateTimeList=new ArrayList<>();
        dateTimeList.add(v1);
        dateTimeList.add(v2);
        dateTimeList.add(v3);
        dateTimeList.add(v3);
        dateTimeList.sort((o1, o2) -> o1.after(o2)?-1:1);
        System.out.println(dateTimeList);
    }
}
