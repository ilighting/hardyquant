package com.ruoyi.quant.utils;

public class Main {
    public static void main(String[] args) {
        double[] priceData = {1.0, 2.0, 3.0, 4.0, 5.0}; // 价格数据
        int period = 3; // 移动平均线周期

        double[] ddeValues = calculateDde(priceData, period);

        // 打印 DDE 值
        for (double dde : ddeValues) {
            System.out.println("DDE 值: " + dde);
        }
    }

    // 计算 DDE 指标
    public static double[] calculateDde(double[] priceData, int period) {
        double[] ddeValues = new double[priceData.length];

        for (int i = period; i < priceData.length; i++) {
            double sum = 0;

            // 计算移动平均线
            for (int j = 0; j < period; j++) {
                sum += priceData[i - j];
            }
            double ma = sum / period;

            // 计算 DDE
            ddeValues[i] = priceData[i] - ma;
        }

        return ddeValues;
    }
}
