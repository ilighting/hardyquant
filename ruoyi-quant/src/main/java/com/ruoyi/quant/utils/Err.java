package com.ruoyi.quant.utils;


import com.ruoyi.quant.enums.HttpStatus;
import com.ruoyi.quant.enums.ICodeEnum;
import com.ruoyi.quant.pipe.common.PipeException;

public class Err {
	public static void error(String message) {
		throw new PipeException(message);
	}

	public static void error(PipeException exception) {
		throw exception;
	}

	public static void error(ICodeEnum errCode) {
		error(errCode.getMsg(), errCode.getCode());
	}

	public static void error(String message, String err) {
		throw new PipeException(message, HttpStatus.ERROR, err);
	}
}
