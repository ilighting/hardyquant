package com.ruoyi.quant.enums;

public interface ICodeEnum {
	String getCode();
	String getMsg();

}
