package com.ruoyi.quant.mg.mgcrud;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.InsertManyResult;
import com.mongodb.client.result.InsertOneResult;
import org.bson.BsonObjectId;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Map;

public class CreateService<T> extends BaseCrudService{


	public <T> ObjectId insertEntity(T entity){
		String name  = getTableName(entity);
		MongoCollection<Document> collection = getCollection(name);
		Document document = getDocument(entity);
		InsertOneResult insertOneResult = collection.insertOne(document);
		document.clear();
		BsonValue insertedId = insertOneResult.getInsertedId();
		return ((BsonObjectId) insertedId).getValue();
	}
	public <T> Map<Integer, BsonValue> insertManyEntity(List<T> entity){
		if(entity.size()<=0){
			return null;
		}
		String name  = getListTableName(entity);
		MongoCollection<Document> collection = getCollection(name);
		List<Document> documents = getDocuments(entity);
		InsertManyResult insertManyResult = collection.insertMany(documents);
		Map<Integer, BsonValue> insertedIds = insertManyResult.getInsertedIds();
		return insertedIds;
	}


}
