package com.ruoyi.quant.mg.mgcrud;

import com.mongodb.client.MongoCollection;

import com.ruoyi.quant.mg.MongoLocalConn;
import com.ruoyi.quant.mg.constants.MgConfig;
import lombok.SneakyThrows;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

public class BaseCrudService<T> {

    MongoCollection<Document> getCollection(String tableName) {
        MongoLocalConn build = MongoLocalConn.build();
        MongoCollection<Document> collection = build.getCollection(MgConfig.dbName, tableName);
        return collection;
    }

    // Cache to store BeanInfo for each entity class
    private static final Map<Class<?>, BeanInfo> beanInfoCache = new ConcurrentHashMap<>();

    @SneakyThrows
    private <T> BeanInfo getBeanInfo(T entity) {
        // Check if BeanInfo is already cached for this entity class
        Class<?> entityClass = entity.getClass();
        BeanInfo beanInfo = beanInfoCache.get(entityClass);

        if (beanInfo != null) {
            return beanInfo;
        }

        // If not cached, retrieve BeanInfo using Introspector
        beanInfo = Introspector.getBeanInfo(entityClass);

        // Cache the BeanInfo for future use
        beanInfoCache.put(entityClass, beanInfo);

        return beanInfo;
    }

    String getListTableName(List<?> list) {
        if (list.size() > 0) {
            Object o = list.get(0);
            return getTableName(o);
        }
        return null;
    }

    @SneakyThrows
    private <T> String getTableName(Class<T> tClass) {
        Class<? extends Class> entityClass = tClass.getClass();
        BeanInfo beanInfo = beanInfoCache.get(entityClass);

        if (beanInfo != null) {
            String lowerCase = beanInfo.getBeanDescriptor().getBeanClass().getSimpleName().toLowerCase(Locale.ROOT);

            return lowerCase;
        }

        // If not cached, retrieve BeanInfo using Introspector
        beanInfo = Introspector.getBeanInfo(entityClass);

        // Cache the BeanInfo for future use
        beanInfoCache.put(entityClass, beanInfo);

        return beanInfo.getBeanDescriptor().getBeanClass().getSimpleName().toLowerCase(Locale.ROOT);

    }

    @SneakyThrows
    <T> String getTableName(T entity) {
        // Check if BeanInfo is already cached for this entity class
        Class<?> entityClass = entity.getClass();
        BeanInfo beanInfo = beanInfoCache.get(entityClass);

        if (beanInfo != null) {
            String lowerCase = beanInfo.getBeanDescriptor().getBeanClass().getSimpleName().toLowerCase(Locale.ROOT);

            return lowerCase;
        }

        // If not cached, retrieve BeanInfo using Introspector
        beanInfo = Introspector.getBeanInfo(entityClass);

        // Cache the BeanInfo for future use
        beanInfoCache.put(entityClass, beanInfo);

        return beanInfo.getBeanDescriptor().getBeanClass().getSimpleName().toLowerCase(Locale.ROOT);
    }

    static Class<?> getGenericClassByIndex(Object obj, int index) {
        Type type = obj.getClass().getGenericSuperclass();
        ParameterizedType parameterizedType = (ParameterizedType) type;
        return (Class<?>) parameterizedType.getActualTypeArguments()[index];
    }


    @SneakyThrows
    <T> Document getDocument(T instance) {
        Document document = new Document();
        BeanInfo beanInfo = Introspector.getBeanInfo(instance.getClass());

        for (PropertyDescriptor propertyDescriptor : beanInfo.getPropertyDescriptors()) {
            String propertyName = propertyDescriptor.getName();
            if ("pageNum".equals(propertyName)||"pageSize".equals(propertyName)||"class".equals(propertyName) || "serialVersionUID".equals(propertyName)) {
                continue;
            }
            Method getter = propertyDescriptor.getReadMethod();
            Object propertyValue = getter.invoke(instance);
            if(null!=propertyValue&& StringUtils.isNotEmpty(propertyValue.toString())){
                String property = String.valueOf(propertyValue);
                if(propertyName.equals("id")){
                    document.append("_id", new ObjectId(property));
                }else{
                    if(property.trim().contains("^")||property.trim().contains("*")||property.trim().contains("%")){
                        Pattern pattern = Pattern.compile(property);
                        document.append(propertyName, pattern);
                    }else{
                        document.append(propertyName, property.trim());
                    }

                }

            }

        }
        return document;
    }

    @SneakyThrows
    <T> Document getDocument(String column, Object colVal) {
        Document document = new Document();

        document.append(column, colVal);
        return document;
    }

    @SneakyThrows
    <T> Document getDocument(String column, Collection colVal) {
        Document document = new Document();
        for (Object o : colVal) {
            if(column.equals("id")){
                document.append("_id",new ObjectId(o.toString()));
            }else{
                document.append(column, colVal);
            }
        }
        return document;
    }

    @SneakyThrows
    <T> Document getUniqueDocument(T instance) {
        Document document = new Document();
        BeanInfo beanInfo = Introspector.getBeanInfo(instance.getClass());

        for (PropertyDescriptor propertyDescriptor : beanInfo.getPropertyDescriptors()) {
            String propertyName = propertyDescriptor.getName();
            if ("code".equals(propertyName)) {
                Method getter = propertyDescriptor.getReadMethod();
                Object propertyValue = getter.invoke(instance);
                document.append(propertyName, propertyValue);
            }

        }
        return document;
    }

    @SneakyThrows
    <T> Document getUpdateDocument(T instance) {
        Document document = new Document();
        BeanInfo beanInfo = Introspector.getBeanInfo(instance.getClass());

        for (PropertyDescriptor propertyDescriptor : beanInfo.getPropertyDescriptors()) {
            String propertyName = propertyDescriptor.getName();
            if ("class".equals(propertyName) || "serialVersionUID".equals(propertyName) || "code".equals(propertyName)) {
                continue;
            }
            Method getter = propertyDescriptor.getReadMethod();
            Object propertyValue = getter.invoke(instance);
            document.append(propertyName, propertyValue);
        }
        Document update = new Document("$set", document);
        return update;
    }

    @SneakyThrows
    <T> List<Document> getDocuments(List<T> entities) {
        List<Document> documents = new ArrayList<>();
        List<List<T>> partition = ListUtils.partition(entities, MgConfig.batchSize);
        partition.parallelStream().forEachOrdered(item -> {
            item.stream().forEach(entity -> {
                Document document = getDocument(entity);
                documents.add(document);
            });

        });

        return documents;
    }


    public <T> Class<T> getGenericTypeClass() {
        T clazz= null;
        clazz.getClass();

        return null;
    }

    public <T> Class<T> getGenericTypeClass(Class<T> tClass) {
        Type type = tClass.getGenericSuperclass();
        ParameterizedType paramType = (ParameterizedType) type;
        Type[] typeArguments = paramType.getActualTypeArguments();
        Class<T> genericTypeClass = (Class<T>) typeArguments[0];
        return genericTypeClass;
    }


}
