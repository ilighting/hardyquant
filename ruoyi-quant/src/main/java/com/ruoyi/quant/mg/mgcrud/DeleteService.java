package com.ruoyi.quant.mg.mgcrud;

import com.alibaba.fastjson.JSON;
import com.mongodb.client.ClientSession;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;

public class DeleteService<T> extends BaseCrudService<T> {


	public <T> T deleteOne(T entity) {
		String name = getTableName(entity);
		MongoCollection<Document> collection = getCollection(name);
		Document query = getDocument(entity);
		Document documentFindIterable = collection.findOneAndDelete(query);
		System.out.println("---------");
		T parseObject = (T) JSON.parseObject(JSON.toJSONString(documentFindIterable), entity.getClass());
		return parseObject;
	}


	public <T> T deleteOne(T entity, ClientSession clientSession) {
		String name = getTableName(entity);
		MongoCollection<Document> collection = getCollection(name);
		Document query = getDocument(entity);
		Document documentFindIterable = collection.findOneAndDelete(clientSession,query);
		System.out.println("---------");
		T parseObject = (T) JSON.parseObject(JSON.toJSONString(documentFindIterable), entity.getClass());
		return parseObject;
	}


	public <T> Long deleteList(String colName,Object val,Class<T> tClass) {

		String simpleName = tClass.getSimpleName().toLowerCase(Locale.ROOT);
		MongoCollection<Document> collection = getCollection(simpleName);
		Document query = getDocument(colName,val);
		DeleteResult deleteResult = collection.deleteMany(query);
		return deleteResult.getDeletedCount();
	}

	public <T> Long deleteList(Map<String,Object> params, Class<T> tClass) {

		String simpleName = tClass.getSimpleName().toLowerCase(Locale.ROOT);
		MongoCollection<Document> collection = getCollection(simpleName);
		Document query =new Document();
		for (Map.Entry<String, Object> stringObjectEntry : params.entrySet()) {
			query=getDocument(stringObjectEntry.getKey(),stringObjectEntry.getValue());
		}

		DeleteResult deleteResult = collection.deleteMany(query);
		return deleteResult.getDeletedCount();
	}

	public <T> Long deleteList(String colName, Collection vals, Class<T> tClass) {

		String simpleName = tClass.getSimpleName().toLowerCase(Locale.ROOT);
		MongoCollection<Document> collection = getCollection(simpleName);
		Document query = getDocument(colName,vals);
		DeleteResult deleteResult = collection.deleteMany(query);
		return deleteResult.getDeletedCount();
	}


	public <T> Long deleteList(T entity) {
		String name = getTableName(entity);
		MongoCollection<Document> collection = getCollection(name);
		Document query = getDocument(entity);
		DeleteResult deleteResult = collection.deleteMany(query);
		return deleteResult.getDeletedCount();
	}

	public <T> Long deleteList(T entity, ClientSession clientSession) {
		String name = getTableName(entity);
		MongoCollection<Document> collection = getCollection(name);
		Document query = getDocument(entity);
		FindIterable<Document> documentFindIterable = collection.find(clientSession,query);
		DeleteResult deleteResult = collection.deleteMany(query);
		return deleteResult.getDeletedCount();
	}

}
