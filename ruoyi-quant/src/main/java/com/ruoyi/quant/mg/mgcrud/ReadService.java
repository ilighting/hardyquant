package com.ruoyi.quant.mg.mgcrud;

import cn.hutool.core.exceptions.ExceptionUtil;
import com.alibaba.fastjson.JSON;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.ruoyi.quant.mg.MgSort;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ReadService<T> extends BaseCrudService<T> {

    public <T> T getOne(T entity) {
        String name = getTableName(entity);
        MongoCollection<Document> collection = getCollection(name);
        Document query = getDocument(entity);
        Document documentFindIterable = collection.find(query).first();
        System.out.println("---------");
        T parseObject = (T) JSON.parseObject(JSON.toJSONString(documentFindIterable), entity.getClass());
        return parseObject;
    }

    public <T> List<T> listByEntity(T entity, MgSort sort) {
        String name = getTableName(entity);
        MongoCollection<Document> collection = getCollection(name);
        Document query = getDocument(entity);
        List<T> resultMapList = new ArrayList<T>();
        FindIterable<Document> documentFindIterable = collection.find(query);
        System.out.println("---------");
        if (sort != null) {
            documentFindIterable.sort(sort.getDocument());
        }
        MongoCursor<Document> cursor = documentFindIterable.iterator();
        try {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                T parseObject = (T) JSON.parseObject(JSON.toJSONString(document), entity.getClass());
                resultMapList.add(parseObject);
            }
        } finally {
            cursor.close();
        }

        return resultMapList;
    }

    public <T> List<T> pageList(T entity, Integer pageNum, Integer pageSize) {
        String name = getTableName(entity);
        MongoCollection<Document> collection = getCollection(name);
        Document query = getDocument(entity);
        List<T> resultMapList = new ArrayList<T>();
        // 定义分页参数
//		int pageSize = 10;  // 每页的数据量
//		int pageNumber = 2;  // 页码数，从1开始

//		// 计算跳过的文档数
        int skipDocs = (pageNum - 1) * pageSize;
        FindIterable<Document> cursor = null;

        FindIterable<Document> findIterable = collection.find(query);

        cursor = findIterable.skip(skipDocs).limit(pageSize);

        MongoCursor<Document> iterator = cursor.iterator();
        try {
            // 遍历查询结果
            while (iterator.hasNext()) {
                Document document = iterator.next();
                ObjectId id = document.getObjectId("_id");
                document.append("id", id.toString());
                T parseObject = (T) JSON.parseObject(JSON.toJSONString(document), entity.getClass());
                resultMapList.add(parseObject);

            }
        } catch (Exception e) {
            System.out.println(ExceptionUtil.getMessage(e));
        }
        return resultMapList;
    }


    public <T> List<T> listByColumn(String column, Object colVal, MgSort sort, Class<T> tClass) {
        String name = tClass.getSimpleName().toLowerCase(Locale.ROOT);
        MongoCollection<Document> collection = getCollection(name);
        Document query = getDocument(column, colVal);
        List<T> resultMapList = new ArrayList<T>();
        FindIterable<Document> documentFindIterable = collection.find(query);
        System.out.println("---------");
        if (sort != null) {
            documentFindIterable.sort(sort.getDocument());
        }
        MongoCursor<Document> cursor = documentFindIterable.iterator();
        try {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                T parseObject = (T) JSON.parseObject(JSON.toJSONString(document), tClass);
                resultMapList.add(parseObject);
            }
        } finally {
            cursor.close();
        }
        return resultMapList;
    }
    public <T> List<T> listByColumn(Map<String,Object> params, MgSort sort, Class<T> tClass) {
        String name = tClass.getSimpleName().toLowerCase(Locale.ROOT);
        MongoCollection<Document> collection = getCollection(name);
        Document query=new Document();
        for (Map.Entry<String, Object> stringObjectEntry : params.entrySet()) {
            query = getDocument(stringObjectEntry.getKey(), stringObjectEntry.getValue());
        }

        List<T> resultMapList = new ArrayList<T>();
        FindIterable<Document> documentFindIterable = collection.find(query);
        System.out.println("---------");
        if (sort != null) {
            documentFindIterable.sort(sort.getDocument());
        }
        MongoCursor<Document> cursor = documentFindIterable.iterator();
        try {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                T parseObject = (T) JSON.parseObject(JSON.toJSONString(document), tClass);
                resultMapList.add(parseObject);
            }
        } finally {
            cursor.close();
        }
        return resultMapList;
    }

    public <T> Long count(final T entity) {
        String name = getTableName(entity);
        MongoCollection<Document> collection = getCollection(name);
        Document query = getDocument(entity);
        long l = collection.countDocuments(query);
        return l;
    }
}
