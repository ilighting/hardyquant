package com.ruoyi.quant.mg.builders;

import org.bson.Document;

public class FromDoc extends Document {

    public synchronized static FromDoc getInstance(){
        return new FromDoc();
    }
    public FromDoc build(String joinTable){
        this.append("from",joinTable);
        return this;
    }



}
