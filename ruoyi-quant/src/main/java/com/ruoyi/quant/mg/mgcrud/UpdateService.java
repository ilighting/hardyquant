package com.ruoyi.quant.mg.mgcrud;


import com.alibaba.fastjson.JSON;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.ruoyi.quant.mg.constants.MgConfig;
import org.apache.commons.collections4.ListUtils;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class UpdateService<T> extends BaseCrudService<T>{



	public <T> T updateOne(T entity){
		String name  = getTableName(entity);
		MongoCollection<Document> collection = getCollection(name);
		Document uniqueDocument = getUniqueDocument(entity);
		Document updateDocument = getUpdateDocument(entity);
		// 执行findOneAndUpdate操作
		Document resultDocument = collection.findOneAndUpdate(uniqueDocument, updateDocument);
		T parseObject = (T) JSON.parseObject(JSON.toJSONString(resultDocument), entity.getClass());
		return parseObject;
	}

	public <T> List<T> updateOneBatch(List<T> entities){
		List<T> newList=new ArrayList<>();
		List<List<T>> partition = ListUtils.partition(entities, MgConfig.batchSize);
		partition.parallelStream().forEach(item->{
			for (T t : item) {
				T t1 = updateOne(t);
				newList.add(t1);
			}
		});
		return newList;
	}

	public <T> Long updateAllBatch(List<T> entities){
		final Long[] count = {0L};
		List<List<T>> partition = ListUtils.partition(entities, MgConfig.batchSize);
		partition.parallelStream().forEach(item->{
			for (T t : item) {
				Long aLong = updateAll(t);
				count[0] = count[0] +aLong;

			}
		});
		return count[0];
	}
	public <T> Long updateAll(T entity){
		Long i=0L;
		String name  = getTableName(entity);
		MongoCollection<Document> collection = getCollection(name);
		Document uniqueDocument = getUniqueDocument(entity);


		// 创建更新操作
		Document updateDocument = getUpdateDocument(entity);
		// 执行findOneAndUpdate操作
		FindIterable<Document> documentFindIterable = collection.find(uniqueDocument);

		MongoCursor<Document> cursor = documentFindIterable.batchSize(MgConfig.batchSize).iterator();
		try {
			while (cursor.hasNext()) {
				Document document = cursor.next();
				collection.findOneAndUpdate(document,updateDocument);
				i = i + 1;

			}
		} finally {
			cursor.close();
		}


		return i;
	}

}
