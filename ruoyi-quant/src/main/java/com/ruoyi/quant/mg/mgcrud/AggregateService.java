package com.ruoyi.quant.mg.mgcrud;


import com.alibaba.fastjson.JSON;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.ruoyi.quant.mg.builders.LookupDoc;
import com.ruoyi.quant.mg.builders.MatchDoc;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AggregateService<T> extends BaseCrudService<T> {
    List<Bson> docList = new ArrayList<>();

    public AggregateService add(Bson document) {
        docList.add(document);
        return this;
    }

    public <T> List<T> excute(Class<T> tClass) {
        String name = getTableName(tClass);
        MongoCollection<Document> collection = getCollection(name);
        AggregateIterable<Document> aggregate = collection.aggregate(this.docList);
        // Process the aggregation result
        List<T> list = new ArrayList<>();
        for (Document document : aggregate) {
            System.out.println(document.toJson());
            T parseObject = (T) JSON.parseObject(JSON.toJSONString(document), tClass);
            list.add(parseObject);
        }
        return list;
    }

    /**
     * @param tableName   B的表名
     * @param tableAFiled A 的外键
     * @param tableBFiled B 的主键
     * @param tableResult 结果集 字段
     * @param <T>
     * @return
     */
    public <T> List<T> excuteJoin(Class<T> tableA, String tableName, String tableAFiled, String tableBFiled, String tableResult) {
        Class<T> tableA1 = tableA;
        String name = tableA1.getSimpleName().toLowerCase(Locale.ROOT);
        List<Bson> queryList=new ArrayList<>();
        if(null!=getDoc()){
            queryList.add(getDoc());
        }
        if(null!=getLookupDoc()){
            queryList.add(getLookupDoc());
        }

        MongoCollection<Document> collection = getCollection(name);
        AggregateIterable<Document> aggregate = collection.aggregate(queryList);
        // Process the aggregation result
        List<T> list = new ArrayList<>();
        for (Document document : aggregate) {
            System.out.println(document.toJson());
            T parseObject = (T) JSON.parseObject(JSON.toJSONString(document), tableA1);
            list.add(parseObject);
        }
        return list;
    }

    /**
     * @param tableA   B的表名
     * @param <T>
     * @return
     */
    public <T> List<T> excuteJoin(Class<T> tableA) {
        Class<T> tableA1 = tableA;
        String name = tableA1.getSimpleName().toLowerCase(Locale.ROOT);
        List<Bson> queryList=new ArrayList<>();
        if(null!=getDoc()){
            queryList.add(getDoc());
        }
        if(null!=getLookupDoc()){
            queryList.add(getLookupDoc());
        }

        MongoCollection<Document> collection = getCollection(name);
        AggregateIterable<Document> aggregate = collection.aggregate(queryList);
        // Process the aggregation result
        List<T> list = new ArrayList<>();
        for (Document document : aggregate) {
            System.out.println(document.toJson());
            T parseObject = (T) JSON.parseObject(JSON.toJSONString(document), tableA1);
            list.add(parseObject);
        }
        return list;
    }

    private MatchDoc doc;
    private LookupDoc lookupDoc;

    public LookupDoc getLookupDoc() {
        return lookupDoc;
    }

    public void setLookupDoc(LookupDoc lookupDoc) {
        this.lookupDoc = lookupDoc;
    }

    public void buildMatch(MatchDoc doc) {
        setDoc(doc);
    }
  public void buildLookup(LookupDoc doc) {
        setLookupDoc(doc);
    }

    public MatchDoc getDoc() {
        return doc;
    }

    public void setDoc(MatchDoc doc) {
        this.doc = doc;
    }
}
