package com.ruoyi.quant.mg.builders;

import org.bson.Document;

public class GroupDoc extends Document{

    public synchronized static GroupDoc getInstance(){
        return new GroupDoc();
    }
    public GroupDoc build(String col,Object val){
        this.append("$group",new Document(col,val));
        return this;
    }
}
