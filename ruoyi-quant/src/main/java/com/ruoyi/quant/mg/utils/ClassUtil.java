package com.ruoyi.quant.mg.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author sxq
 */
public class ClassUtil {
	/**
	 * 获取对象类上的第index个泛型参数
	 * 比如一个类的声明为  class User extends Entity<String, Integer> {}
	 * 则 getParameterizedType(user, 0) 返回  String.class
	 * @param obj
	 * @param index
	 * @return
	 */
	public static Class<?> getGenericClassByIndex(Object obj, int index) {
		Type type = obj.getClass().getGenericSuperclass();
		ParameterizedType parameterizedType = (ParameterizedType) type;
		return (Class<?>) parameterizedType.getActualTypeArguments()[index];
	}


}
