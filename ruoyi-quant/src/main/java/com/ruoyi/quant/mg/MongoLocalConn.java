package com.ruoyi.quant.mg;

import com.alibaba.ttl.TransmittableThreadLocal;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.HashMap;
import java.util.Map;

public class MongoLocalConn {

	private static ThreadLocal<Map<String, MongoClient>> cache = new TransmittableThreadLocal<>();

	public static void put(String key, MongoClient value) {
		Map<String, MongoClient> map = cache.get();
		if (map == null) {
			map = new HashMap<>();
			cache.set(map);
		}
		map.put(key, value);
	}

	public static MongoClient get(String key) {
		Map<String, MongoClient> map = cache.get();
		if (map != null) {
			return map.get(key);
		}
		return null;
	}

	public static void remove(Thread key) {
		Map<String, MongoClient> map = cache.get();
		if (map != null) {
			map.remove(key);
		}
	}

	public static void clear() {
		cache.remove();
	}
	public static synchronized MongoLocalConn build(){
		return new MongoLocalConn();
	}
	public MongoLocalConn(){
		buildConnection();
	}
	private static void buildConnection(){
		String thread = "master";
// 创建MongoClient实例
		MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017");
		put(thread,mongoClient);
	}

	public MongoDatabase getDb(String dbName){
		MongoClient mongoClient = get("master");
		MongoDatabase database = mongoClient.getDatabase(dbName);
		return database;
	}
	public MongoCollection<Document> getCollection(String dbName, String tableName){
// 选择集合
		MongoCollection<Document> collection = getDb(dbName).getCollection(tableName);
		return collection;
	}

}

