package com.ruoyi.quant.mg.builders;

import org.bson.Document;

public class LookupDoc extends Document {
    public static synchronized LookupDoc getInstance() {
        return new LookupDoc();
    }

    private FromDoc doc;

    private LookupDoc addFrom(FromDoc fromDoc) {
        this.append("$lookup", fromDoc);
        return this;
    }

    public LookupDoc buildJoinTable(String joinTable) {
        FromDoc instance = FromDoc.getInstance();
        instance.build(joinTable);
        this.doc=instance;
        return addFrom(instance);
    }

    public LookupDoc buildMatch(String col, Object val) {
        this.append("$match", new Document(col, val));
        return this;
    }

    private LookupDoc addA(String tableAFiled) {
        this.doc.append("localField", tableAFiled);
        return this;
    }

    public LookupDoc buildtableAFiled(String tableAFiled) {
        return addA(tableAFiled);
    }

    public LookupDoc addB(String tableBFiled) {
        this.doc.append("foreignField", tableBFiled);
        return this;
    }

    public LookupDoc buildtableBFiled(String tableBFiled) {
        return addB(tableBFiled);
    }

    private LookupDoc addResult(String resultName) {
        this.doc.append("as", resultName);
        return this;
    }

    public LookupDoc buildResult(String resultName) {
        return addResult(resultName);
    }

}
