package com.ruoyi.quant.mg.builders;

import org.bson.Document;

public class MatchDoc extends Document{

    public synchronized static MatchDoc getInstance(){
        return new MatchDoc();
    }
    public MatchDoc build(String col, Object val){
        this.append("$match",new Document(col,val));
        return this;
    }
}
