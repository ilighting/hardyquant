package com.ruoyi.quant.mg.service;



import com.ruoyi.quant.mg.MgSort;

import java.util.List;
import java.util.Map;

public interface IMongoService<T> {
    <T> Long deleteAllByCol(String code, String analysisQueryCode,Class<T> tClass);
    <T> Long deleteByMap(Map<String,Object> params, Class<T> tClass);

    <T> Integer saveAll(List<T> adosc);


    List<T> listAll(String code, Object val, MgSort sort, Class<T> tClass);
    List<T> listAll(Map<String,Object> params, MgSort sort, Class<T> tClass);
    List<T> listAll(T entity);


	List<T> listAll(T entity, MgSort mgSort);

	<T> List<T> pageList(T entity, int pageNum, int pageSize);

	<T> Long count(T entity);


	<T> T getOne(T entity);

	<T> Long removeBath(List<String> ids,Class<T> tClass);
}
