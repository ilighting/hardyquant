package com.ruoyi.quant.mg.service;

import com.ruoyi.quant.mg.MgSort;
import com.ruoyi.quant.mg.mgcrud.*;
import org.bson.BsonValue;

import java.util.List;
import java.util.Map;

public class MongoServiceImpl<T> extends BaseCrudService<T> implements IMongoService<T> {

    CreateService<T> c = new CreateService();
    ReadService<T> r = new ReadService();
    UpdateService<T> u = new UpdateService();
    DeleteService<T> d = new DeleteService();
    AggregateService<T>ag=new AggregateService<>();

    @Override
    public  <T> Long  deleteAllByCol(String code, String analysisQueryCode,Class<T> tClass) {

        Long l = d.deleteList(code, analysisQueryCode,tClass);
        return l;
    }

    @Override
    public <T1> Long deleteByMap(Map<String, Object> params, Class<T1> t1Class) {
        Long l = d.deleteList(params, t1Class);
        return l;
    }

    @Override
    public <T> Integer saveAll(List<T> adosc) {
        if(adosc.size()>0){
            Map<Integer, BsonValue> integerBsonValueMap = c.insertManyEntity(adosc);
            return integerBsonValueMap.size();
        }
        return 0;
    }

    @Override
    public List<T> listAll(String code, Object val, MgSort mgSort, Class<T> tClass) {
        return r.listByColumn(code,val,mgSort,tClass);
    }

    @Override
    public List<T> listAll(Map<String, Object> params, MgSort sort, Class<T> tClass) {
        return r.listByColumn(params,sort,tClass);
    }

    @Override
    public List<T> listAll(final T entity) {
        return r.listByEntity(entity,new MgSort());
    }

    @Override
    public List<T> listAll(T entity, MgSort mgSort) {
        return r.listByEntity(entity,mgSort);
    }
//    public Map<String,Object> pageList(int pageNum,int pageSize){
//        ag.excuteJoin()
//    }

    @Override
    public <T> List<T> pageList(T entity, int pageNum, int pageSize){
        List<T> tList = r.pageList(entity, pageNum, pageSize);
        return tList;
    }

    @Override
    public <T> Long count(T entity) {
        Long count= r.count(entity);
       return count;
    }
    @Override
    public <T> T getOne(T entity){
        T one = r.getOne(entity);
        return one;
    }

    @Override
    public <T> Long removeBath(final List<String> ids,Class<T> tClass) {
        return d.deleteList("id",ids,tClass);
    }
}
