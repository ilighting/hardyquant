-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('股票列表', '2008', '1', '/quant/stockCode', 'C', '0', 'quant:stockCode:view', '#', 'admin', sysdate(), '', null, '股票列表菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('股票列表查询', @parentId, '1',  '#',  'F', '0', 'quant:stockCode:list',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('股票列表新增', @parentId, '2',  '#',  'F', '0', 'quant:stockCode:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('股票列表修改', @parentId, '3',  '#',  'F', '0', 'quant:stockCode:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('股票列表删除', @parentId, '4',  '#',  'F', '0', 'quant:stockCode:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('股票列表导出', @parentId, '5',  '#',  'F', '0', 'quant:stockCode:export',       '#', 'admin', sysdate(), '', null, '');
