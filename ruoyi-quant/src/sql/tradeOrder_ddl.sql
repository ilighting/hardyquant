create table if not exists ry.dfcf_trade_order
(
    id          bigint         null,
    direction   varchar(2)     null comment '买卖方向',
    account_id  varchar(200)   null comment '账号',
    code        varchar(10)    null comment '代码',
    name        varchar(256)   null comment '名称',
    price       decimal(10, 6) null comment '价格',
    position    int            null comment '持仓头寸',
    unit        int            null comment '单位',
    market      varchar(10)    null comment '市场',
    version     varchar(200)   null comment '版本号',
    create_by   varchar(200)   null,
    create_time datetime       null comment '创建时间',
    update_by   varchar(200)   null comment '更新人',
    update_time datetime       null comment '更新时间',
    remark      varchar(200)   null comment '备注',
    trade_date  datetime       null comment '交易日',
    parent_id   bigint         null comment '父表Id'
    )
    comment '订单记录' collate = utf8mb4_general_ci;

