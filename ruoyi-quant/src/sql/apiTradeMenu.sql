-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('东方财富交易界面', '2000', '1', '/trade/apiTrade', 'C', '0', 'trade:apiTrade:view', '#', 'admin', sysdate(), '', null, '东方财富交易界面菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('东方财富交易界面查询', @parentId, '1',  '#',  'F', '0', 'trade:apiTrade:list',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('东方财富交易界面新增', @parentId, '2',  '#',  'F', '0', 'trade:apiTrade:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('东方财富交易界面修改', @parentId, '3',  '#',  'F', '0', 'trade:apiTrade:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('东方财富交易界面删除', @parentId, '4',  '#',  'F', '0', 'trade:apiTrade:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('东方财富交易界面导出', @parentId, '5',  '#',  'F', '0', 'trade:apiTrade:export',       '#', 'admin', sysdate(), '', null, '');
