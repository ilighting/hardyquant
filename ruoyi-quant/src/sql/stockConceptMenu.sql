-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('板块信息', '2008', '1', '/quant/stockConcept', 'C', '0', 'quant:stockConcept:view', '#', 'admin', sysdate(), '', null, '板块信息菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('板块信息查询', @parentId, '1',  '#',  'F', '0', 'quant:stockConcept:list',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('板块信息新增', @parentId, '2',  '#',  'F', '0', 'quant:stockConcept:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('板块信息修改', @parentId, '3',  '#',  'F', '0', 'quant:stockConcept:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('板块信息删除', @parentId, '4',  '#',  'F', '0', 'quant:stockConcept:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('板块信息导出', @parentId, '5',  '#',  'F', '0', 'quant:stockConcept:export',       '#', 'admin', sysdate(), '', null, '');
