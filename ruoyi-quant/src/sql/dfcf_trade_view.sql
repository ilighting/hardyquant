    /*
     Navicat Premium Data Transfer

     Source Server         : 海信本地
     Source Server Type    : MySQL
     Source Server Version : 80028
     Source Host           : localhost:3306
     Source Schema         : ry

     Target Server Type    : MySQL
     Target Server Version : 80028
     File Encoding         : 65001

     Date: 09/11/2023 17:51:05
    */

    SET NAMES utf8mb4;
    SET FOREIGN_KEY_CHECKS = 0;

    -- ----------------------------
    -- Table structure for dfcf_trade_view
    -- ----------------------------
    DROP TABLE IF EXISTS `dfcf_trade_view`;
    CREATE TABLE `dfcf_trade_view`  (
      `id` bigint NOT NULL COMMENT '主键id',
      `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '代码',
      `max_profit` decimal(10, 6) NULL DEFAULT NULL COMMENT '最大收益',
      `max_cut` decimal(10, 6) NULL DEFAULT NULL COMMENT '最大回撤',
      `sharp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '夏普率',
      `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
      `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
      `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
      `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
      `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
      `trade_times` int NULL DEFAULT NULL COMMENT '交易次数',
      `fee_cost` decimal(10, 6) NULL DEFAULT NULL COMMENT '手续费',
      `final_balance` decimal(10, 6) NULL DEFAULT NULL COMMENT '最终余额',
      `open_balance` decimal(10, 6) NULL DEFAULT NULL COMMENT '起始额度',
      `max_balance` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最大额度',
      `min_balance` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最小额度',
      PRIMARY KEY (`id`) USING BTREE,
      INDEX `idx`(`id` ASC) USING BTREE,
      INDEX `code_idx`(`code` ASC, `create_time` ASC) USING BTREE
    ) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '回测订单分析图' ROW_FORMAT = Dynamic;

    -- ----------------------------
    -- Records of dfcf_trade_view
    -- ----------------------------

    SET FOREIGN_KEY_CHECKS = 1;
