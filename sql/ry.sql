/*
 Navicat Premium Data Transfer

 Source Server         : 127
 Source Server Type    : MySQL
 Source Server Version : 80027 (8.0.27)
 Source Host           : 127.0.0.1:3306
 Source Schema         : ry

 Target Server Type    : MySQL
 Target Server Version : 80027 (8.0.27)
 File Encoding         : 65001

 Date: 13/11/2023 22:41:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dfcf_trade_order
-- ----------------------------
DROP TABLE IF EXISTS `dfcf_trade_order`;
CREATE TABLE `dfcf_trade_order` (
  `id` bigint DEFAULT NULL,
  `direction` varchar(2) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '买卖方向',
  `account_id` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号',
  `code` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '代码',
  `name` varchar(256) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `price` decimal(10,6) DEFAULT NULL COMMENT '价格',
  `position` int DEFAULT NULL COMMENT '持仓头寸',
  `unit` int DEFAULT NULL COMMENT '单位',
  `market` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '市场',
  `version` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '版本号',
  `create_by` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `trade_date` datetime DEFAULT NULL COMMENT '交易日',
  `parent_id` bigint DEFAULT NULL COMMENT '父表Id',
  `open_balance` double DEFAULT NULL COMMENT '开始仓位',
  `balance` double DEFAULT NULL COMMENT '余额',
  `position_balance` double DEFAULT NULL COMMENT '持仓价值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='订单记录';

-- ----------------------------
-- Records of dfcf_trade_order
-- ----------------------------
BEGIN;
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006049054722, 'B', NULL, '002176', NULL, 30.150000, 6, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2007-10-25 00:00:00', NULL, 20000, 1910, 18090);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006074220583, 'S', NULL, '002176', NULL, 30.970000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2007-11-19 00:00:00', NULL, 1910, 20492, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006078414875, 'B', NULL, '002176', NULL, 28.650000, 7, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2007-12-03 00:00:00', NULL, 20492, 437, 20055);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006078414889, 'S', NULL, '002176', NULL, 31.700000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2007-12-11 00:00:00', NULL, 437, 22627, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006078414969, 'B', NULL, '002176', NULL, 32.400000, 6, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-01-22 00:00:00', NULL, 22627, 3187, 19440);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006078414981, 'B', NULL, '002176', NULL, 31.660000, 7, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-01-25 00:00:00', NULL, 3187, 21, 3166);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006078415015, 'S', NULL, '002176', NULL, 27.990000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-02-18 00:00:00', NULL, 21, 19613.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006082609189, 'B', NULL, '002176', NULL, 27.600000, 7, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-03-13 00:00:00', NULL, 19613.999999999996, 293.999999999996, 19320);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006082609215, 'S', NULL, '002176', NULL, 28.950000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-03-26 00:00:00', NULL, 293.999999999996, 20558.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006082609221, 'B', NULL, '002176', NULL, 25.230000, 8, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-03-28 00:00:00', NULL, 20558.999999999996, 374.999999999996, 20184);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006082609281, 'S', NULL, '002176', NULL, 19.490000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-05-05 00:00:00', NULL, 374.999999999996, 15966.999999999995, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006082609309, 'B', NULL, '002176', NULL, 18.050000, 8, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-05-21 00:00:00', NULL, 15966.999999999995, 1526.999999999995, 14440);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006082609333, 'S', NULL, '002176', NULL, 19.230000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-06-06 00:00:00', NULL, 1526.999999999995, 16910.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006086803487, 'B', NULL, '002176', NULL, 15.820000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-06-23 00:00:00', NULL, 16910.999999999996, 1090.999999999996, 15820);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006086803515, 'S', NULL, '002176', NULL, 16.480000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-07-09 00:00:00', NULL, 1090.999999999996, 17570.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006086803529, 'B', NULL, '002176', NULL, 14.800000, 11, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-07-16 00:00:00', NULL, 17570.999999999996, 1290.999999999996, 16280);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006086803551, 'S', NULL, '002176', NULL, 16.060000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-07-28 00:00:00', NULL, 1290.999999999996, 18956.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006086803561, 'B', NULL, '002176', NULL, 15.250000, 12, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-08-01 00:00:00', NULL, 18956.999999999996, 656.999999999996, 18300);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006090997770, 'S', NULL, '002176', NULL, 16.660000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-08-12 00:00:00', NULL, 656.999999999996, 20648.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006090997812, 'B', NULL, '002176', NULL, 15.390000, 13, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-09-03 00:00:00', NULL, 20648.999999999996, 641.999999999996, 20007);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006090997886, 'B', NULL, '002176', NULL, 6.250000, 14, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-10-16 00:00:00', NULL, 641.999999999996, 16.999999999996, 625);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006090997940, 'S', NULL, '002176', NULL, 6.270000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-11-12 00:00:00', NULL, 16.999999999996, 8794.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006095192114, 'B', NULL, '002176', NULL, 7.290000, 12, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-12-12 00:00:00', NULL, 8794.999999999996, 46.999999999996, 8748);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006095192127, 'S', NULL, '002176', NULL, 8.440000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2008-12-31 00:00:00', NULL, 46.999999999996, 10174.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006095192163, 'B', NULL, '002176', NULL, 10.100000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-03-02 00:00:00', NULL, 10174.999999999996, 74.999999999996, 10100);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006095192182, 'S', NULL, '002176', NULL, 11.550000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-03-30 00:00:00', NULL, 74.999999999996, 11624.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006095192201, 'B', NULL, '002176', NULL, 10.410000, 11, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-04-27 00:00:00', NULL, 11624.999999999996, 173.999999999996, 11451);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006095192231, 'S', NULL, '002176', NULL, 12.030000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-06-12 00:00:00', NULL, 173.999999999996, 13406.999999999995, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006095192277, 'B', NULL, '002176', NULL, 11.680000, 11, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-08-17 00:00:00', NULL, 13406.999999999995, 558.999999999995, 12848);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006095192298, 'S', NULL, '002176', NULL, 11.990000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-09-15 00:00:00', NULL, 558.999999999995, 13747.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006095192307, 'B', NULL, '002176', NULL, 10.590000, 12, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-09-28 00:00:00', NULL, 13747.999999999996, 1039.999999999996, 12708);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006095192308, 'B', NULL, '002176', NULL, 10.100000, 13, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-09-29 00:00:00', NULL, 1039.999999999996, 29.999999999996, 1010);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006099386386, 'S', NULL, '002176', NULL, 11.680000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-10-23 00:00:00', NULL, 29.999999999996, 15213.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006099386460, 'B', NULL, '002176', NULL, 12.440000, 12, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-11-26 00:00:00', NULL, 15213.999999999996, 285.999999999996, 14928);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006099386472, 'S', NULL, '002176', NULL, 13.840000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-12-02 00:00:00', NULL, 285.999999999996, 16893.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006099386494, 'B', NULL, '002176', NULL, 13.000000, 12, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-12-17 00:00:00', NULL, 16893.999999999996, 1293.999999999996, 15600);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006099386498, 'B', NULL, '002176', NULL, 12.610000, 13, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2009-12-18 00:00:00', NULL, 1293.999999999996, 32.999999999996, 1261);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006099386524, 'S', NULL, '002176', NULL, 13.650000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-04 00:00:00', NULL, 32.999999999996, 17777.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006103580686, 'B', NULL, '002176', NULL, 13.110000, 13, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-01-22 00:00:00', NULL, 17777.999999999996, 734.999999999996, 17043);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006103580736, 'S', NULL, '002176', NULL, 13.040000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-02-25 00:00:00', NULL, 734.999999999996, 17686.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006103580770, 'B', NULL, '002176', NULL, 12.720000, 13, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-03-15 00:00:00', NULL, 17686.999999999996, 1150.999999999996, 16536);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006103580780, 'S', NULL, '002176', NULL, 13.680000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-03-18 00:00:00', NULL, 1150.999999999996, 18934.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006103580880, 'B', NULL, '002176', NULL, 16.760000, 11, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-05-07 00:00:00', NULL, 18934.999999999996, 498.999999999992, 18436.000000000004);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006107774979, 'S', NULL, '002176', NULL, 14.840000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-06-17 00:00:00', NULL, 498.999999999992, 16822.999999999993, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006107775003, 'B', NULL, '002176', NULL, 13.950000, 12, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-06-30 00:00:00', NULL, 16822.999999999993, 82.999999999993, 16740);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006107775015, 'S', NULL, '002176', NULL, 15.420000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-07-07 00:00:00', NULL, 82.999999999993, 18586.999999999993, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006107775131, 'B', NULL, '002176', NULL, 28.220000, 6, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-09-07 00:00:00', NULL, 18586.999999999993, 1654.999999999993, 16932);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006107775169, 'S', NULL, '002176', NULL, 29.950000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-09-30 00:00:00', NULL, 1654.999999999993, 19624.999999999993, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006111969283, 'B', NULL, '002176', NULL, 32.000000, 6, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-11-12 00:00:00', NULL, 19624.999999999993, 424.999999999993, 19200);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006111969319, 'S', NULL, '002176', NULL, 33.850000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-12-16 00:00:00', NULL, 424.999999999993, 20734.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006111969325, 'B', NULL, '002176', NULL, 28.920000, 7, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-12-24 00:00:00', NULL, 20734.999999999996, 490.999999999996, 20244);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006111969341, 'S', NULL, '002176', NULL, 33.200000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-01-19 00:00:00', NULL, 490.999999999996, 23731, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006111969362, 'B', NULL, '002176', NULL, 20.900000, 11, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-02-25 00:00:00', NULL, 23731, 741, 22990);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006111969399, 'S', NULL, '002176', NULL, 24.810000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-04-25 00:00:00', NULL, 741, 28031.999999999996, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006116163599, 'B', NULL, '002176', NULL, 22.820000, 12, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-05-16 00:00:00', NULL, 28031.999999999996, 647.999999999996, 27384);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006116163624, 'S', NULL, '002176', NULL, 22.450000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-06-24 00:00:00', NULL, 647.999999999996, 27587.999999999993, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006116163654, 'B', NULL, '002176', NULL, 25.100000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-08-05 00:00:00', NULL, 27587.999999999993, 2487.999999999993, 25100);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006116163655, 'B', NULL, '002176', NULL, 23.000000, 11, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-08-08 00:00:00', NULL, 2487.999999999993, 187.999999999993, 2300);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006120357937, 'S', NULL, '002176', NULL, 20.520000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-10-28 00:00:00', NULL, 187.999999999993, 22759.999999999993, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006120357991, 'B', NULL, '002176', NULL, 21.870000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-11-25 00:00:00', NULL, 22759.999999999993, 889.999999999993, 21870);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006120358113, 'S', NULL, '002176', NULL, 15.990000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-01-30 00:00:00', NULL, 889.999999999993, 16879.999999999993, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006124552265, 'B', NULL, '002176', NULL, 18.350000, 9, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-03-28 00:00:00', NULL, 16879.999999999993, 364.999999999993, 16515);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006124552285, 'S', NULL, '002176', NULL, 20.130000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-04-11 00:00:00', NULL, 364.999999999993, 18481.999999999993, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006124552307, 'B', NULL, '002176', NULL, 19.140000, 9, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-04-23 00:00:00', NULL, 18481.999999999993, 1255.999999999993, 17226);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006124552331, 'S', NULL, '002176', NULL, 20.360000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-05-07 00:00:00', NULL, 1255.999999999993, 19579.999999999993, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006124552345, 'B', NULL, '002176', NULL, 19.270000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-05-15 00:00:00', NULL, 19579.999999999993, 309.999999999993, 19270);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006124552391, 'S', NULL, '002176', NULL, 10.800000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-06-11 00:00:00', NULL, 309.999999999993, 11109.999999999993, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006128746508, 'B', NULL, '002176', NULL, 11.930000, 9, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-07-11 00:00:00', NULL, 11109.999999999993, 372.999999999993, 10737);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006128746626, 'S', NULL, '002176', NULL, 9.540000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-10 00:00:00', NULL, 372.999999999993, 8958.99999999999, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006128746644, 'B', NULL, '002176', NULL, 8.560000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-09-20 00:00:00', NULL, 8958.99999999999, 398.99999999999, 8560);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006128746666, 'S', NULL, '002176', NULL, 8.910000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-10-09 00:00:00', NULL, 398.99999999999, 9308.999999999989, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006128746678, 'B', NULL, '002176', NULL, 8.330000, 11, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-10-15 00:00:00', NULL, 9308.999999999989, 145.999999999989, 9163);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006132940807, 'S', NULL, '002176', NULL, 9.170000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-10-18 00:00:00', NULL, 145.999999999989, 10232.999999999989, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006132940837, 'B', NULL, '002176', NULL, 8.540000, 11, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-11-05 00:00:00', NULL, 10232.999999999989, 838.999999999991, 9393.999999999998);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006132940849, 'B', NULL, '002176', NULL, 8.150000, 12, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-11-08 00:00:00', NULL, 838.999999999991, 23.999999999991, 815);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006132940927, 'S', NULL, '002176', NULL, 7.220000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-12-17 00:00:00', NULL, 23.999999999991, 8687.99999999999, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006132940997, 'B', NULL, '002176', NULL, 7.950000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-01-23 00:00:00', NULL, 8687.99999999999, 737.99999999999, 7950);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006132941019, 'S', NULL, '002176', NULL, 8.270000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-02-04 00:00:00', NULL, 737.99999999999, 9007.999999999989, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006132941063, 'B', NULL, '002176', NULL, 8.100000, 11, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-03-04 00:00:00', NULL, 9007.999999999989, 97.999999999989, 8910);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006137135106, 'S', NULL, '002176', NULL, 8.690000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-03-06 00:00:00', NULL, 97.999999999989, 9656.999999999987, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006137135141, 'B', NULL, '002176', NULL, 8.200000, 11, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-04-26 00:00:00', NULL, 9656.999999999987, 636.999999999989, 9019.999999999998);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006137135144, 'S', NULL, '002176', NULL, 9.200000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-06 00:00:00', NULL, 636.999999999989, 10756.999999999987, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006137135210, 'B', NULL, '002176', NULL, 10.720000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-08-16 00:00:00', NULL, 10756.999999999987, 36.999999999987, 10720);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006137135226, 'S', NULL, '002176', NULL, 11.360000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-09-10 00:00:00', NULL, 36.999999999987, 11396.999999999987, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006137135245, 'B', NULL, '002176', NULL, 10.420000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-10-16 00:00:00', NULL, 11396.999999999987, 976.999999999987, 10420);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006137135253, 'S', NULL, '002176', NULL, 11.030000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-10-28 00:00:00', NULL, 976.999999999987, 12006.999999999987, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006141329410, 'B', NULL, '002176', NULL, 11.050000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-12-06 00:00:00', NULL, 12006.999999999987, 956.999999999987, 11050);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006141329464, 'S', NULL, '002176', NULL, 10.930000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-01-24 00:00:00', NULL, 956.999999999987, 11886.999999999987, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006141329520, 'B', NULL, '002176', NULL, 12.530000, 9, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-02-26 00:00:00', NULL, 11886.999999999987, 609.999999999987, 11277);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006141329528, 'S', NULL, '002176', NULL, 12.980000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-03-03 00:00:00', NULL, 609.999999999987, 12291.999999999987, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006141329544, 'B', NULL, '002176', NULL, 12.230000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-03-11 00:00:00', NULL, 12291.999999999987, 61.999999999987, 12230);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006141329576, 'S', NULL, '002176', NULL, 12.230000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-03-27 00:00:00', NULL, 61.999999999987, 12291.999999999989, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006141329580, 'B', NULL, '002176', NULL, 11.740000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-03-28 00:00:00', NULL, 12291.999999999989, 551.999999999989, 11740);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006145523719, 'S', NULL, '002176', NULL, 10.790000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-05-13 00:00:00', NULL, 551.999999999989, 11341.999999999989, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006145523771, 'B', NULL, '002176', NULL, 10.700000, 10, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-09 00:00:00', NULL, 11341.999999999989, 641.999999999989, 10700);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006145523785, 'S', NULL, '002176', NULL, 11.450000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-06-16 00:00:00', NULL, 641.999999999989, 12091.999999999989, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006145523911, 'B', NULL, '002176', NULL, 14.450000, 8, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-08-14 00:00:00', NULL, 12091.999999999989, 531.999999999989, 11560);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006145523953, 'S', NULL, '002176', NULL, 14.980000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-09-04 00:00:00', NULL, 531.999999999989, 12515.999999999989, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006145523991, 'B', NULL, '002176', NULL, 15.190000, 8, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-09-26 00:00:00', NULL, 12515.999999999989, 363.999999999989, 12152);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006149718063, 'S', NULL, '002176', NULL, 14.930000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-10-28 00:00:00', NULL, 363.999999999989, 12307.999999999989, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006149718087, 'B', NULL, '002176', NULL, 14.320000, 8, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-11-07 00:00:00', NULL, 12307.999999999989, 851.999999999989, 11456);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006149718125, 'S', NULL, '002176', NULL, 15.870000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-11-27 00:00:00', NULL, 851.999999999989, 13547.999999999989, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006149718141, 'B', NULL, '002176', NULL, 14.110000, 9, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-12-05 00:00:00', NULL, 13547.999999999989, 848.999999999989, 12699);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006149718185, 'S', NULL, '002176', NULL, 14.700000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-12-30 00:00:00', NULL, 848.999999999989, 14078.999999999987, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006149718269, 'B', NULL, '002176', NULL, 19.800000, 7, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-05-06 00:00:00', NULL, 14078.999999999987, 218.999999999987, 13860);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006149718273, 'S', NULL, '002176', NULL, 23.470000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-05-12 00:00:00', NULL, 218.999999999987, 16647.999999999985, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006153912336, 'B', NULL, '002176', NULL, 25.730000, 6, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-06-29 00:00:00', NULL, 16647.999999999985, 1209.999999999985, 15438);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006153912361, 'S', NULL, '002176', NULL, 25.450000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-08-10 00:00:00', NULL, 1209.999999999985, 16479.99999999998, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006153912362, 'B', NULL, '002176', NULL, 12.720000, 12, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-11-10 00:00:00', NULL, 16479.99999999998, 1215.99999999998, 15264);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006153912388, 'S', NULL, '002176', NULL, 18.100000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-12-16 00:00:00', NULL, 1215.99999999998, 22935.999999999978, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006153912401, 'B', NULL, '002176', NULL, 15.000000, 15, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-01-05 00:00:00', NULL, 22935.999999999978, 435.999999999978, 22500);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006153912445, 'S', NULL, '002176', NULL, 11.910000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-03-21 00:00:00', NULL, 435.999999999978, 18300.999999999978, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006158106637, 'B', NULL, '002176', NULL, 11.730000, 15, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-04-21 00:00:00', NULL, 18300.999999999978, 705.999999999978, 17595);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006158106654, 'S', NULL, '002176', NULL, 13.120000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-05-17 00:00:00', NULL, 705.999999999978, 20385.999999999978, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006158106743, 'B', NULL, '002176', NULL, 14.770000, 13, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-18 00:00:00', NULL, 20385.999999999978, 1184.999999999978, 19201);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006158106793, 'S', NULL, '002176', NULL, 13.510000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-08-10 00:00:00', NULL, 1184.999999999978, 18747.999999999978, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006158106823, 'B', NULL, '002176', NULL, 13.240000, 14, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-08-29 00:00:00', NULL, 18747.999999999978, 211.999999999978, 18536);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006158106851, 'S', NULL, '002176', NULL, 13.350000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-09-09 00:00:00', NULL, 211.999999999978, 18901.999999999978, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006158106855, 'B', NULL, '002176', NULL, 12.930000, 14, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-09-12 00:00:00', NULL, 18901.999999999978, 799.999999999978, 18102);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006158106861, 'S', NULL, '002176', NULL, 13.410000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-09-14 00:00:00', NULL, 799.999999999978, 19573.999999999978, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006158106875, 'B', NULL, '002176', NULL, 13.020000, 15, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-09-23 00:00:00', NULL, 19573.999999999978, 43.999999999978, 19530);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006162300961, 'S', NULL, '002176', NULL, 12.790000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-18 00:00:00', NULL, 43.999999999978, 19228.999999999978, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006162300979, 'B', NULL, '002176', NULL, 12.370000, 15, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-10-26 00:00:00', NULL, 19228.999999999978, 673.999999999978, 18555);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006162301001, 'S', NULL, '002176', NULL, 12.610000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-08 00:00:00', NULL, 673.999999999978, 19588.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006162301021, 'B', NULL, '002176', NULL, 12.260000, 15, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-21 00:00:00', NULL, 19588.999999999975, 1198.999999999975, 18390);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006162301029, 'S', NULL, '002176', NULL, 12.590000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-11-24 00:00:00', NULL, 1198.999999999975, 20083.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006162301061, 'B', NULL, '002176', NULL, 12.860000, 15, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-12-09 00:00:00', NULL, 20083.999999999975, 793.999999999975, 19290);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006162301155, 'S', NULL, '002176', NULL, 11.050000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-03 00:00:00', NULL, 793.999999999975, 17368.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006162301175, 'B', NULL, '002176', NULL, 10.930000, 15, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-15 00:00:00', NULL, 17368.999999999975, 973.999999999975, 16395);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006162301191, 'S', NULL, '002176', NULL, 11.210000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-22 00:00:00', NULL, 973.999999999975, 17788.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006166495266, 'B', NULL, '002176', NULL, 11.700000, 15, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-20 00:00:00', NULL, 17788.999999999975, 238.999999999975, 17550);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006166495300, 'S', NULL, '002176', NULL, 11.440000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 00:00:00', NULL, 238.999999999975, 17398.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006166495314, 'B', NULL, '002176', NULL, 10.620000, 16, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-14 00:00:00', NULL, 17398.999999999975, 406.999999999975, 16992);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006166495354, 'S', NULL, '002176', NULL, 10.010000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-05-04 00:00:00', NULL, 406.999999999975, 16422.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006166495366, 'B', NULL, '002176', NULL, 9.040000, 18, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-05-11 00:00:00', NULL, 16422.999999999975, 150.999999999977, 16271.999999999998);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006166495422, 'S', NULL, '002176', NULL, 8.930000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-14 00:00:00', NULL, 150.999999999977, 16224.999999999976, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006166495450, 'B', NULL, '002176', NULL, 8.600000, 18, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-28 00:00:00', NULL, 16224.999999999976, 744.999999999976, 15480);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006166495464, 'S', NULL, '002176', NULL, 9.200000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-05 00:00:00', NULL, 744.999999999976, 17304.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006170689560, 'B', NULL, '002176', NULL, 11.010000, 15, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-11 00:00:00', NULL, 17304.999999999975, 789.999999999975, 16515);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006170689574, 'S', NULL, '002176', NULL, 12.560000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-21 00:00:00', NULL, 789.999999999975, 19629.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006170689652, 'B', NULL, '002176', NULL, 14.990000, 13, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-22 00:00:00', NULL, 19629.999999999975, 142.999999999975, 19487);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006170689739, 'S', NULL, '002176', NULL, 11.740000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-03 00:00:00', NULL, 142.999999999975, 15404.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006170689747, 'B', NULL, '002176', NULL, 10.600000, 14, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-15 00:00:00', NULL, 15404.999999999975, 564.999999999975, 14840);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006170689770, 'S', NULL, '002176', NULL, 10.730000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-22 00:00:00', NULL, 564.999999999975, 15586.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006174883861, 'B', NULL, '002176', NULL, 10.560000, 14, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-23 00:00:00', NULL, 15586.999999999975, 802.999999999975, 14784);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006174883886, 'S', NULL, '002176', NULL, 11.120000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-03 00:00:00', NULL, 802.999999999975, 16370.999999999973, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006174883902, 'B', NULL, '002176', NULL, 10.530000, 15, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-25 00:00:00', NULL, 16370.999999999973, 575.999999999973, 15795);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006174883935, 'S', NULL, '002176', NULL, 9.980000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-12 00:00:00', NULL, 575.999999999973, 15545.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006174883950, 'B', NULL, '002176', NULL, 9.300000, 16, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-02 00:00:00', NULL, 15545.999999999975, 665.999999999973, 14880.000000000002);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006174884044, 'S', NULL, '002176', NULL, 7.710000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-21 00:00:00', NULL, 665.999999999973, 13001.999999999973, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006174884058, 'B', NULL, '002176', NULL, 7.110000, 18, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-08 00:00:00', NULL, 13001.999999999973, 203.999999999973, 12798);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006174884110, 'S', NULL, '002176', NULL, 6.290000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-02 00:00:00', NULL, 203.999999999973, 11525.999999999973, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006179078156, 'B', NULL, '002176', NULL, 6.000000, 19, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-23 00:00:00', NULL, 11525.999999999973, 125.999999999973, 11400);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006179078174, 'S', NULL, '002176', NULL, 6.550000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-03 00:00:00', NULL, 125.999999999973, 12570.999999999973, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006179078212, 'B', NULL, '002176', NULL, 6.270000, 20, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-24 00:00:00', NULL, 12570.999999999973, 30.999999999973, 12540);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006179078242, 'S', NULL, '002176', NULL, 6.230000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-09 00:00:00', NULL, 30.999999999973, 12490.999999999973, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006179078264, 'B', NULL, '002176', NULL, 6.040000, 20, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-22 00:00:00', NULL, 12490.999999999973, 410.999999999973, 12080);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006179078316, 'S', NULL, '002176', NULL, 5.500000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-20 00:00:00', NULL, 410.999999999973, 11410.999999999973, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006179078390, 'B', NULL, '002176', NULL, 5.920000, 19, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-28 00:00:00', NULL, 11410.999999999973, 162.999999999973, 11248);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006179078408, 'S', NULL, '002176', NULL, 6.390000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-10 00:00:00', NULL, 162.999999999973, 12303.999999999973, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006179078446, 'B', NULL, '002176', NULL, 6.360000, 19, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-26 00:00:00', NULL, 12303.999999999973, 219.999999999973, 12084);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006183272547, 'S', NULL, '002176', NULL, 5.090000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-21 00:00:00', NULL, 219.999999999973, 9890.999999999973, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006183272557, 'B', NULL, '002176', NULL, 4.870000, 20, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-28 00:00:00', NULL, 9890.999999999973, 150.999999999973, 9740);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006183272667, 'S', NULL, '002176', NULL, 4.100000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-19 00:00:00', NULL, 150.999999999973, 8350.999999999973, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006183272724, 'B', NULL, '002176', NULL, 4.020000, 20, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-26 00:00:00', NULL, 8350.999999999973, 310.999999999974, 8039.999999999999);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006183272755, 'B', NULL, '002176', NULL, 3.100000, 21, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-15 00:00:00', NULL, 310.999999999974, 0.999999999974, 310);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006183272771, 'S', NULL, '002176', NULL, 3.380000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-09 00:00:00', NULL, 0.999999999974, 7098.999999999974, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006187466753, 'B', NULL, '002176', NULL, 3.420000, 20, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-23 00:00:00', NULL, 7098.999999999974, 258.999999999974, 6840);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006187466785, 'B', NULL, '002176', NULL, 2.520000, 21, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-17 00:00:00', NULL, 258.999999999974, 6.999999999974, 252);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006187466836, 'S', NULL, '002176', NULL, 1.530000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 00:00:00', NULL, 6.999999999974, 3219.9999999999745, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006187466879, 'B', NULL, '002176', NULL, 1.760000, 18, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-23 00:00:00', NULL, 3219.9999999999745, 51.9999999999745, 3168);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006187466907, 'S', NULL, '002176', NULL, 1.880000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-07 00:00:00', NULL, 51.9999999999745, 3435.999999999974, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006187466951, 'B', NULL, '002176', NULL, 2.110000, 16, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-04 00:00:00', NULL, 3435.999999999974, 59.999999999974, 3376);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006187466979, 'S', NULL, '002176', NULL, 2.120000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-18 00:00:00', NULL, 59.999999999974, 3451.999999999974, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006187467027, 'B', NULL, '002176', NULL, 2.040000, 16, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-26 00:00:00', NULL, 3451.999999999974, 187.999999999974, 3264);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006187467039, 'B', NULL, '002176', NULL, 1.830000, 17, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-29 00:00:00', NULL, 187.999999999974, 4.999999999974, 183);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006187467061, 'S', NULL, '002176', NULL, 2.070000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-10 00:00:00', NULL, 4.999999999974, 3523.999999999974, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006191661152, 'B', NULL, '002176', NULL, 5.690000, 6, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-03 00:00:00', NULL, 3523.999999999974, 109.999999999974, 3414);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006191661176, 'S', NULL, '002176', NULL, 6.180000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-02-22 00:00:00', NULL, 109.999999999974, 3817.999999999974, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006191661196, 'B', NULL, '002176', NULL, 5.600000, 6, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-05 00:00:00', NULL, 3817.999999999974, 457.999999999974, 3360);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006191661220, 'S', NULL, '002176', NULL, 6.300000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-18 00:00:00', NULL, 457.999999999974, 4237.999999999974, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006191661270, 'B', NULL, '002176', NULL, 6.640000, 6, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-15 00:00:00', NULL, 4237.999999999974, 253.999999999974, 3984);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006191661278, 'S', NULL, '002176', NULL, 7.570000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-19 00:00:00', NULL, 253.999999999974, 4795.999999999974, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006191661290, 'B', NULL, '002176', NULL, 6.700000, 7, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-26 00:00:00', NULL, 4795.999999999974, 105.999999999974, 4690);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006191661304, 'S', NULL, '002176', NULL, 8.060000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-06 00:00:00', NULL, 105.999999999974, 5747.999999999974, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006191661342, 'B', NULL, '002176', NULL, 8.440000, 6, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-26 00:00:00', NULL, 5747.999999999974, 683.999999999974, 5064);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006191661406, 'S', NULL, '002176', NULL, 8.460000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-01 00:00:00', NULL, 683.999999999974, 5759.9999999999745, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006195855539, 'B', NULL, '002176', NULL, 25.920000, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-27 00:00:00', NULL, 5759.9999999999745, 575.9999999999745, 5184);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006195855579, 'S', NULL, '002176', NULL, 23.780000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-10-22 00:00:00', NULL, 575.9999999999745, 5331.9999999999745, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006195855595, 'B', NULL, '002176', NULL, 21.390000, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-02 00:00:00', NULL, 5331.9999999999745, 1053.9999999999745, 4278);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006195855633, 'S', NULL, '002176', NULL, 22.450000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-18 00:00:00', NULL, 1053.9999999999745, 5543.9999999999745, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006195855679, 'B', NULL, '002176', NULL, 21.550000, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-10 00:00:00', NULL, 5543.9999999999745, 1233.9999999999745, 4310);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006200049677, 'S', NULL, '002176', NULL, 19.300000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-10 00:00:00', NULL, 1233.9999999999745, 5093.9999999999745, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006200049710, 'B', NULL, '002176', NULL, 21.570000, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-03-29 00:00:00', NULL, 5093.9999999999745, 779.9999999999745, 4314);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006200049736, 'S', NULL, '002176', NULL, 20.640000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-11 00:00:00', NULL, 779.9999999999745, 4907.9999999999745, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006200049786, 'B', NULL, '002176', NULL, 24.710000, 1, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-21 00:00:00', NULL, 4907.9999999999745, 2436.9999999999745, 2471);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006200049794, 'B', NULL, '002176', NULL, 23.100000, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-02 00:00:00', NULL, 2436.9999999999745, 126.9999999999745, 2310);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006200049854, 'S', NULL, '002176', NULL, 19.980000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-11-02 00:00:00', NULL, 126.9999999999745, 4122.9999999999745, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006200049872, 'B', NULL, '002176', NULL, 19.580000, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-11-28 00:00:00', NULL, 4122.9999999999745, 206.999999999975, 3915.9999999999995);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006200049908, 'S', NULL, '002176', NULL, 18.570000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-01-18 00:00:00', NULL, 206.999999999975, 3920.999999999975, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006200049924, 'B', NULL, '002176', NULL, 18.260000, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-02-16 00:00:00', NULL, 3920.999999999975, 268.9999999999745, 3652.0000000000005);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006200050020, 'S', NULL, '002176', NULL, 12.580000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-07-10 00:00:00', NULL, 268.9999999999745, 2784.9999999999745, 0);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006204243978, 'B', NULL, '002176', NULL, 11.480000, 2, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-07-24 00:00:00', NULL, 2784.9999999999745, 488.9999999999745, 2296);
INSERT INTO `dfcf_trade_order` (`id`, `direction`, `account_id`, `code`, `name`, `price`, `position`, `unit`, `market`, `version`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `trade_date`, `parent_id`, `open_balance`, `balance`, `position_balance`) VALUES (1722637006204244030, 'S', NULL, '002176', NULL, 9.990000, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-10-12 00:00:00', NULL, 488.9999999999745, 2486.9999999999745, 0);
COMMIT;

-- ----------------------------
-- Table structure for dfcf_trade_user
-- ----------------------------
DROP TABLE IF EXISTS `dfcf_trade_user`;
CREATE TABLE `dfcf_trade_user` (
  `account_id` mediumtext COLLATE utf8mb4_general_ci COMMENT '账号id',
  `password` varchar(256) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  `validate_key` varchar(256) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '验证秘钥',
  `cookie` varchar(256) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'cookie缓存',
  `total_amount` decimal(12,6) DEFAULT NULL COMMENT '账户总额',
  `able_amount` decimal(12,6) DEFAULT NULL COMMENT '可用余额',
  `wd_able_amount` decimal(12,6) DEFAULT NULL COMMENT '可提账户',
  `frozen_amount` decimal(12,6) DEFAULT NULL COMMENT '冻结账户',
  `user_id` varchar(256) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户Id',
  `id` bigint DEFAULT NULL COMMENT '主键id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='东方财富交易界面';

-- ----------------------------
-- Records of dfcf_trade_user
-- ----------------------------
BEGIN;
INSERT INTO `dfcf_trade_user` (`account_id`, `password`, `validate_key`, `cookie`, `total_amount`, `able_amount`, `wd_able_amount`, `frozen_amount`, `user_id`, `id`) VALUES ('540850175188', '199505', '780d821b-e961-45f5-be10-634c1559697b', 'Khmc=%e7%94%9f%e8%a5%bf%e7%90%aa; Uid=EexK6lUXLWzdK1ArYcsX2Q%3d%3d; Uuid=2c5bd041f676451a8dbe43f8f61941cf; Yybdm=5408; mobileimei=91cff1db-4c6b-417b-a1e1-dacf1bda2290', NULL, NULL, NULL, NULL, '540850175188', 1720092042152779777);
COMMIT;

-- ----------------------------
-- Table structure for dfcf_trade_view
-- ----------------------------
DROP TABLE IF EXISTS `dfcf_trade_view`;
CREATE TABLE `dfcf_trade_view` (
  `id` bigint NOT NULL COMMENT '主键id',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '代码',
  `max_profit` decimal(10,6) DEFAULT NULL COMMENT '最大收益',
  `max_cut` decimal(10,6) DEFAULT NULL COMMENT '最大回撤',
  `sharp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '夏普率',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `trade_times` int DEFAULT NULL COMMENT '交易次数',
  `fee_cost` decimal(10,6) DEFAULT NULL COMMENT '手续费',
  `final_balance` decimal(10,6) DEFAULT NULL COMMENT '最终余额',
  `open_balance` decimal(10,6) DEFAULT NULL COMMENT '起始额度',
  `max_balance` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最大额度',
  `min_balance` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最小额度',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx` (`id`) USING BTREE,
  KEY `code_idx` (`code`,`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='回测订单分析图';

-- ----------------------------
-- Records of dfcf_trade_view
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------
BEGIN;
INSERT INTO `gen_table` (`table_id`, `table_name`, `table_comment`, `sub_table_name`, `sub_table_fk_name`, `class_name`, `tpl_category`, `package_name`, `module_name`, `business_name`, `function_name`, `function_author`, `gen_type`, `gen_path`, `options`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1, 'dfcf_trade', '东方财富交易界面', '', NULL, 'DfcfTrade', 'crud', 'com.ruoyi.quant.trade.crud', 'trade', 'apiTrade', '东方财富交易界面', 'ruoyi', '1', '/Users/sxq/Desktop/job5/hardyquant/ruoyi-quant/src', '{\"parentMenuId\":\"2000\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"东方财富\",\"treeCode\":\"\"}', 'admin', '2023-10-29 11:34:35', '', '2023-10-29 11:45:34', '');
INSERT INTO `gen_table` (`table_id`, `table_name`, `table_comment`, `sub_table_name`, `sub_table_fk_name`, `class_name`, `tpl_category`, `package_name`, `module_name`, `business_name`, `function_name`, `function_author`, `gen_type`, `gen_path`, `options`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2, 'dfcf_trade_order', '订单记录', '', NULL, 'DfcfTradeOrder', 'crud', 'com.ruoyi.quant.trade.crud', 'crud', 'tradeOrder', '订单记录', 'ruoyi', '1', '/Users/sxq/Desktop/job5/hardyquant/ruoyi-quant/src', '{\"parentMenuId\":\"2008\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"股票管理\",\"treeCode\":\"\"}', 'admin', '2023-11-09 22:46:36', '', '2023-11-09 23:12:36', '');
INSERT INTO `gen_table` (`table_id`, `table_name`, `table_comment`, `sub_table_name`, `sub_table_fk_name`, `class_name`, `tpl_category`, `package_name`, `module_name`, `business_name`, `function_name`, `function_author`, `gen_type`, `gen_path`, `options`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (3, 'dfcf_trade_view', '回测订单分析图', 'dfcf_trade_order', 'parent_id', 'DfcfTradeView', 'sub', 'com.ruoyi.quant.trade.crud', 'curd', 'tradeView', '回测订单分析图', 'ruoyi', '1', '/Users/sxq/Desktop/job5/hardyquant/ruoyi-quant/src', '{\"parentMenuId\":\"2008\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"股票管理\",\"treeCode\":\"\"}', 'admin', '2023-11-09 22:46:58', '', '2023-11-09 23:12:47', '');
COMMIT;

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` bigint DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb3 COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
BEGIN;
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (1, 1, 'account_id', '账号id', 'mediumtext', 'String', 'accountId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 1, 'admin', '2023-10-29 11:34:35', NULL, '2023-10-29 11:45:34');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (2, 1, 'password', '密码', 'varchar(256)', 'String', 'password', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-10-29 11:34:35', NULL, '2023-10-29 11:45:34');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (3, 1, 'validate_key', '验证秘钥', 'varchar(256)', 'String', 'validateKey', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-10-29 11:34:35', NULL, '2023-10-29 11:45:34');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (4, 1, 'cookie', 'cookie缓存', 'varchar(256)', 'String', 'cookie', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-10-29 11:34:35', NULL, '2023-10-29 11:45:34');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (5, 1, 'total_amount', '账户总额', 'decimal(12,6)', 'BigDecimal', 'totalAmount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-10-29 11:34:35', NULL, '2023-10-29 11:45:34');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (6, 1, 'able_amount', '可用余额', 'decimal(12,6)', 'BigDecimal', 'ableAmount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-10-29 11:34:35', NULL, '2023-10-29 11:45:34');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (7, 1, 'wd_able_amount', '可提账户', 'decimal(12,6)', 'BigDecimal', 'wdAbleAmount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-10-29 11:34:35', NULL, '2023-10-29 11:45:34');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (8, 1, 'frozen_amount', '冻结账户', 'decimal(12,6)', 'BigDecimal', 'frozenAmount', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2023-10-29 11:34:35', NULL, '2023-10-29 11:45:34');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (9, 1, 'user_id', '用户Id', 'varchar(256)', 'String', 'userId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, '', '2023-10-29 21:55:28', '', NULL);
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (10, 2, 'id', '主键id', 'bigint', 'Long', 'id', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (11, 2, 'direction', '买卖方向', 'varchar(2)', 'String', 'direction', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (12, 2, 'account_id', '账号', 'varchar(200)', 'String', 'accountId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (13, 2, 'code', '代码', 'varchar(10)', 'String', 'code', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (14, 2, 'name', '名称', 'varchar(256)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (15, 2, 'price', '价格', 'decimal(10,6)', 'BigDecimal', 'price', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (16, 2, 'position', '持仓头寸', 'int', 'Long', 'position', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (17, 2, 'unit', '单位', 'int', 'Long', 'unit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (18, 2, 'market', '市场', 'varchar(10)', 'String', 'market', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (19, 2, 'version', '版本号', 'varchar(200)', 'String', 'version', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (20, 2, 'create_by', '创建人', 'varchar(200)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (21, 2, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (22, 2, 'update_by', '更新人', 'varchar(200)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 13, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (23, 2, 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 14, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (24, 2, 'remark', '备注', 'varchar(200)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 15, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (25, 2, 'trade_date', '交易日', 'datetime', 'Date', 'tradeDate', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 16, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (26, 2, 'parent_id', '父表Id', 'bigint', 'Long', 'parentId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 17, 'admin', '2023-11-09 22:46:36', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (27, 3, 'id', '主键id', 'bigint', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (28, 3, 'code', '代码', 'varchar(255)', 'String', 'code', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (29, 3, 'max_profit', '最大收益', 'decimal(10,6)', 'BigDecimal', 'maxProfit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (30, 3, 'max_cut', '最大回撤', 'decimal(10,6)', 'BigDecimal', 'maxCut', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (31, 3, 'sharp', '夏普率', 'varchar(255)', 'String', 'sharp', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (32, 3, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (33, 3, 'create_by', '创建人', 'varchar(255)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 7, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (34, 3, 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (35, 3, 'update_by', '更新人', 'varchar(255)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 9, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (36, 3, 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 10, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (37, 3, 'trade_times', '交易次数', 'int', 'Long', 'tradeTimes', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (38, 3, 'fee_cost', '手续费', 'decimal(10,6)', 'BigDecimal', 'feeCost', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (39, 3, 'final_balance', '最终余额', 'decimal(10,6)', 'BigDecimal', 'finalBalance', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (40, 3, 'open_balance', '起始额度', 'decimal(10,6)', 'BigDecimal', 'openBalance', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (41, 3, 'max_balance', '最大额度', 'varchar(10)', 'String', 'maxBalance', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (42, 3, 'min_balance', '最小额度', 'varchar(10)', 'String', 'minBalance', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 16, 'admin', '2023-11-09 22:46:58', NULL, '2023-11-09 23:12:47');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (43, 2, 'open_balance', '开始仓位', 'double(10,6)', 'BigDecimal', 'openBalance', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 18, '', '2023-11-09 23:12:27', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (44, 2, 'balance', '余额', 'double(10,6)', 'BigDecimal', 'balance', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 19, '', '2023-11-09 23:12:27', NULL, '2023-11-09 23:12:36');
INSERT INTO `gen_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `is_increment`, `is_required`, `is_insert`, `is_edit`, `is_list`, `is_query`, `query_type`, `html_type`, `dict_type`, `sort`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (45, 2, 'position_balance', '持仓价值', 'double(10,6)', 'BigDecimal', 'positionBalance', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 20, '', '2023-11-09 23:12:27', NULL, '2023-11-09 23:12:36');
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_BLOB_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
CREATE TABLE `QRTZ_BLOB_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Blob类型的触发器表';

-- ----------------------------
-- Records of QRTZ_BLOB_TRIGGERS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_CALENDARS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
CREATE TABLE `QRTZ_CALENDARS` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`,`calendar_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='日历信息表';

-- ----------------------------
-- Records of QRTZ_CALENDARS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_CRON_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
CREATE TABLE `QRTZ_CRON_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Cron类型的触发器表';

-- ----------------------------
-- Records of QRTZ_CRON_TRIGGERS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_FIRED_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
CREATE TABLE `QRTZ_FIRED_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint NOT NULL COMMENT '触发的时间',
  `sched_time` bigint NOT NULL COMMENT '定时器制定的时间',
  `priority` int NOT NULL COMMENT '优先级',
  `state` varchar(16) NOT NULL COMMENT '状态',
  `job_name` varchar(200) DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='已触发的触发器表';

-- ----------------------------
-- Records of QRTZ_FIRED_TRIGGERS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_JOB_DETAILS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
CREATE TABLE `QRTZ_JOB_DETAILS` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) NOT NULL COMMENT '任务组名',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='任务详细信息表';

-- ----------------------------
-- Records of QRTZ_JOB_DETAILS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_LOCKS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
CREATE TABLE `QRTZ_LOCKS` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`,`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='存储的悲观锁信息表';

-- ----------------------------
-- Records of QRTZ_LOCKS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`,`trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='暂停的触发器表';

-- ----------------------------
-- Records of QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_SCHEDULER_STATE
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
CREATE TABLE `QRTZ_SCHEDULER_STATE` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`,`instance_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='调度器状态表';

-- ----------------------------
-- Records of QRTZ_SCHEDULER_STATE
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='简单触发器的信息表';

-- ----------------------------
-- Records of QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13,4) DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13,4) DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='同步机制的行锁表';

-- ----------------------------
-- Records of QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
CREATE TABLE `QRTZ_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) NOT NULL COMMENT '触发器的类型',
  `start_time` bigint NOT NULL COMMENT '开始时间',
  `end_time` bigint DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `QRTZ_JOB_DETAILS` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='触发器详细信息表';

-- ----------------------------
-- Records of QRTZ_TRIGGERS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb3 COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
BEGIN;
INSERT INTO `sys_config` (`config_id`, `config_name`, `config_key`, `config_value`, `config_type`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2023-10-28 18:28:39', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` (`config_id`, `config_name`, `config_key`, `config_value`, `config_type`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2023-10-28 18:28:39', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` (`config_id`, `config_name`, `config_key`, `config_value`, `config_type`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2023-10-28 18:28:39', '', NULL, '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `sys_config` (`config_id`, `config_name`, `config_key`, `config_value`, `config_type`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2023-10-28 18:28:39', '', NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` (`config_id`, `config_name`, `config_key`, `config_value`, `config_type`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (5, '用户管理-密码字符范围', 'sys.account.chrtype', '0', 'Y', 'admin', '2023-10-28 18:28:39', '', NULL, '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）');
INSERT INTO `sys_config` (`config_id`, `config_name`, `config_key`, `config_value`, `config_type`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (6, '用户管理-初始密码修改策略', 'sys.account.initPasswordModify', '0', 'Y', 'admin', '2023-10-28 18:28:39', '', NULL, '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` (`config_id`, `config_name`, `config_key`, `config_value`, `config_type`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (7, '用户管理-账号密码更新周期', 'sys.account.passwordValidateDays', '0', 'Y', 'admin', '2023-10-28 18:28:39', '', NULL, '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` (`config_id`, `config_name`, `config_key`, `config_value`, `config_type`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (8, '主框架页-菜单导航显示风格', 'sys.index.menuStyle', 'default', 'Y', 'admin', '2023-10-28 18:28:39', '', NULL, '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）');
INSERT INTO `sys_config` (`config_id`, `config_name`, `config_key`, `config_value`, `config_type`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (9, '主框架页-是否开启页脚', 'sys.index.footer', 'true', 'Y', 'admin', '2023-10-28 18:28:39', '', NULL, '是否开启底部页脚显示（true显示，false隐藏）');
INSERT INTO `sys_config` (`config_id`, `config_name`, `config_key`, `config_value`, `config_type`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (10, '主框架页-是否开启页签', 'sys.index.tagsView', 'true', 'Y', 'admin', '2023-10-28 18:28:39', '', NULL, '是否开启菜单多页签显示（true显示，false隐藏）');
INSERT INTO `sys_config` (`config_id`, `config_name`, `config_key`, `config_value`, `config_type`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (11, '用户登录-黑名单列表', 'sys.login.blackIPList', '', 'Y', 'admin', '2023-10-28 18:28:39', '', NULL, '设置登录IP黑名单限制，多个匹配项以;分隔，支持匹配（*通配、网段）');
COMMIT;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8mb3 COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
BEGIN;
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL);
INSERT INTO `sys_dept` (`dept_id`, `parent_id`, `ancestors`, `dept_name`, `order_num`, `leader`, `phone`, `email`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb3 COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '性别男');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '性别女');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '通知');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '公告');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '停用状态');
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb3 COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '登录状态列表');
COMMIT;

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb3 COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
BEGIN;
INSERT INTO `sys_job` (`job_id`, `job_name`, `job_group`, `invoke_target`, `cron_expression`, `misfire_policy`, `concurrent`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_job` (`job_id`, `job_name`, `job_group`, `invoke_target`, `cron_expression`, `misfire_policy`, `concurrent`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_job` (`job_id`, `job_name`, `job_group`, `invoke_target`, `cron_expression`, `misfire_policy`, `concurrent`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2023-10-28 18:28:39', '', NULL, '');
COMMIT;

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`),
  KEY `idx_sys_logininfor_s` (`status`),
  KEY `idx_sys_logininfor_lt` (`login_time`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8mb3 COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
BEGIN;
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-28 19:05:26');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '1', '验证码错误', '2023-10-28 19:10:52');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-28 19:10:57');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '1', '验证码错误', '2023-10-28 22:17:49');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '1', '验证码错误', '2023-10-28 22:17:51');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-28 22:17:54');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 11:34:22');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 11:42:18');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 11:44:47');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 11:47:59');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '1', '验证码错误', '2023-10-29 19:35:28');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 19:35:35');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 19:36:55');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 19:45:29');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 20:23:17');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 20:24:53');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 20:34:06');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '1', '验证码错误', '2023-10-29 20:35:27');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 20:35:30');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 20:38:07');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 20:39:00');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 20:41:32');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 20:43:11');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 20:44:35');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 20:45:37');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '1', '验证码错误', '2023-10-29 20:46:48');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 20:46:51');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 21:52:27');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 21:55:18');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 22:44:59');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 22:45:59');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (131, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 22:50:12');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 22:52:01');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 22:53:44');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Safari', 'Mac OS X', '0', '登录成功', '2023-10-29 22:55:11');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (135, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 22:56:11');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (136, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 22:56:54');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 22:58:03');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (138, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 22:59:16');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (139, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:01:18');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (140, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:04:11');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:13:14');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:14:27');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (143, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:17:43');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (144, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:27:28');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (145, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:28:14');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (146, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:31:32');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (147, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:35:09');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (148, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:35:48');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (149, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:40:33');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (150, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:44:04');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (151, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-10-29 23:46:49');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (152, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '1', '验证码错误', '2023-11-02 22:52:51');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (153, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-02 22:52:55');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (154, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 19:20:30');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (155, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 19:29:06');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (156, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 19:34:52');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (157, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 19:35:40');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (158, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 19:50:46');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (159, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 19:57:08');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (160, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 19:59:56');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (161, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 20:08:05');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (162, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 22:46:24');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (163, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 23:12:09');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (164, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 23:27:43');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (165, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 23:30:43');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (166, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 23:31:36');
INSERT INTO `sys_logininfor` (`info_id`, `login_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `msg`, `login_time`) VALUES (167, 'admin', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', '0', '登录成功', '2023-11-09 23:33:43');
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int DEFAULT '0' COMMENT '显示顺序',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `is_refresh` char(1) DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2029 DEFAULT CHARSET=utf8mb3 COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1, '系统管理', 0, 1, '#', '', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2023-10-28 18:28:39', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2, '系统监控', 0, 2, '#', '', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2023-10-28 18:28:39', '', NULL, '系统监控目录');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (3, '系统工具', 0, 3, '#', '', 'M', '0', '1', '', 'fa fa-bars', 'admin', '2023-10-28 18:28:39', '', NULL, '系统工具目录');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', 'menuBlank', 'C', '1', '1', '', 'fa fa-location-arrow', 'admin', '2023-10-28 18:28:39', 'admin', '2023-10-28 19:06:34', '若依官网地址');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (100, '用户管理', 1, 1, '/system/user', '', 'C', '0', '1', 'system:user:view', 'fa fa-user-o', 'admin', '2023-10-28 18:28:39', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', '1', 'system:role:view', 'fa fa-user-secret', 'admin', '2023-10-28 18:28:39', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', '1', 'system:menu:view', 'fa fa-th-list', 'admin', '2023-10-28 18:28:39', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', '1', 'system:dept:view', 'fa fa-outdent', 'admin', '2023-10-28 18:28:39', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', '1', 'system:post:view', 'fa fa-address-card-o', 'admin', '2023-10-28 18:28:39', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', '1', 'system:dict:view', 'fa fa-bookmark-o', 'admin', '2023-10-28 18:28:39', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', '1', 'system:config:view', 'fa fa-sun-o', 'admin', '2023-10-28 18:28:39', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (107, '通知公告', 1, 8, '/system/notice', '', 'C', '0', '1', 'system:notice:view', 'fa fa-bullhorn', 'admin', '2023-10-28 18:28:39', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2023-10-28 18:28:39', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', '1', 'monitor:online:view', 'fa fa-user-circle', 'admin', '2023-10-28 18:28:39', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', '1', 'monitor:job:view', 'fa fa-tasks', 'admin', '2023-10-28 18:28:39', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (111, '数据监控', 2, 3, '/monitor/data', 'menuItem', 'C', '1', '1', 'monitor:data:view', 'fa fa-bug', 'admin', '2023-10-28 18:28:39', 'admin', '2023-10-28 19:21:02', '数据监控菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (112, '服务监控', 2, 4, '/monitor/server', '', 'C', '0', '1', 'monitor:server:view', 'fa fa-server', 'admin', '2023-10-28 18:28:39', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (113, '缓存监控', 2, 5, '/monitor/cache', '', 'C', '0', '1', 'monitor:cache:view', 'fa fa-cube', 'admin', '2023-10-28 18:28:39', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (114, '表单构建', 3, 1, '/tool/build', '', 'C', '0', '1', 'tool:build:view', 'fa fa-wpforms', 'admin', '2023-10-28 18:28:39', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (115, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', '1', 'tool:gen:view', 'fa fa-code', 'admin', '2023-10-28 18:28:39', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (116, '系统接口', 3, 3, '/tool/swagger', 'menuItem', 'C', '1', '1', 'tool:swagger:view', 'fa fa-gg', 'admin', '2023-10-28 18:28:39', 'admin', '2023-10-28 19:06:24', '系统接口菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book', 'admin', '2023-10-28 18:28:39', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2023-10-28 18:28:39', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', '1', 'system:user:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', '1', 'system:user:add', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', '1', 'system:user:edit', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', '1', 'system:user:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', '1', 'system:user:export', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', '1', 'system:user:import', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', '1', 'system:user:resetPwd', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', '1', 'system:role:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', '1', 'system:role:add', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', '1', 'system:role:edit', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', '1', 'system:role:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', '1', 'system:role:export', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', '1', 'system:menu:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', '1', 'system:menu:add', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', '1', 'system:menu:edit', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', '1', 'system:menu:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', '1', 'system:dept:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', '1', 'system:dept:add', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', '1', 'system:dept:edit', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', '1', 'system:dept:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', '1', 'system:post:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', '1', 'system:post:add', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', '1', 'system:post:edit', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', '1', 'system:post:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', '1', 'system:post:export', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', '1', 'system:dict:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', '1', 'system:dict:add', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', '1', 'system:dict:edit', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', '1', 'system:dict:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', '1', 'system:dict:export', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', '1', 'system:config:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', '1', 'system:config:add', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', '1', 'system:config:edit', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', '1', 'system:config:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', '1', 'system:config:export', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', '1', 'system:notice:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', '1', 'system:notice:add', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', '1', 'system:notice:edit', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', '1', 'system:notice:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', '1', 'monitor:job:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', '1', 'monitor:job:add', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', '1', 'monitor:job:edit', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', '1', 'monitor:job:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', '1', 'monitor:job:changeStatus', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', '1', 'monitor:job:detail', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', '1', 'monitor:job:export', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1057, '生成查询', 115, 1, '#', '', 'F', '0', '1', 'tool:gen:list', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1058, '生成修改', 115, 2, '#', '', 'F', '0', '1', 'tool:gen:edit', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1059, '生成删除', 115, 3, '#', '', 'F', '0', '1', 'tool:gen:remove', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1060, '预览代码', 115, 4, '#', '', 'F', '0', '1', 'tool:gen:preview', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1061, '生成代码', 115, 5, '#', '', 'F', '0', '1', 'tool:gen:code', '#', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2000, '东方财富', 0, 10, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-bank', 'admin', '2023-10-29 11:36:05', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2001, '东方财富交易界面导出', NULL, 5, '#', '', 'F', '0', '1', 'trade:apiTrade:export', '#', 'admin', '2023-10-29 11:47:14', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2002, '东方财富交易界面', 2000, 1, '/trade/apiTrade', '', 'C', '0', '1', 'trade:apiTrade:view', '#', 'admin', '2023-10-29 11:47:20', '', NULL, '东方财富交易界面菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2003, '东方财富交易界面查询', 2002, 1, '#', '', 'F', '0', '1', 'trade:apiTrade:list', '#', 'admin', '2023-10-29 11:47:20', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2004, '东方财富交易界面新增', 2002, 2, '#', '', 'F', '0', '1', 'trade:apiTrade:add', '#', 'admin', '2023-10-29 11:47:20', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2005, '东方财富交易界面修改', 2002, 3, '#', '', 'F', '0', '1', 'trade:apiTrade:edit', '#', 'admin', '2023-10-29 11:47:20', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2006, '东方财富交易界面删除', 2002, 4, '#', '', 'F', '0', '1', 'trade:apiTrade:remove', '#', 'admin', '2023-10-29 11:47:20', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2007, '东方财富交易界面导出', 2002, 5, '#', '', 'F', '0', '1', 'trade:apiTrade:export', '#', 'admin', '2023-10-29 11:47:20', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2008, '股票管理', 0, 200, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-database', 'admin', '2023-11-09 19:27:14', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2009, '股票列表', 2008, 10, '/quant/stocks', 'menuItem', 'C', '0', '1', NULL, '#', 'admin', '2023-11-09 19:27:36', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2010, '行情面板', 2008, 20, '/panel', 'menuItem', 'C', '0', '1', NULL, '#', 'admin', '2023-11-09 19:28:18', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2017, '回测订单分析图', 2008, 1, '/quant/tradeView', '', 'C', '0', '1', 'quant:tradeView:view', '#', 'admin', '2023-11-09 23:08:31', '', NULL, '回测订单分析图菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2018, '回测订单分析图查询', 2017, 1, '#', '', 'F', '0', '1', 'quant:tradeView:list', '#', 'admin', '2023-11-09 23:08:31', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2019, '回测订单分析图新增', 2017, 2, '#', '', 'F', '0', '1', 'quant:tradeView:add', '#', 'admin', '2023-11-09 23:08:31', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2020, '回测订单分析图修改', 2017, 3, '#', '', 'F', '0', '1', 'quant:tradeView:edit', '#', 'admin', '2023-11-09 23:08:31', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2021, '回测订单分析图删除', 2017, 4, '#', '', 'F', '0', '1', 'quant:tradeView:remove', '#', 'admin', '2023-11-09 23:08:31', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2022, '回测订单分析图导出', 2017, 5, '#', '', 'F', '0', '1', 'quant:tradeView:export', '#', 'admin', '2023-11-09 23:08:31', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2023, '订单记录', 2008, 1, '/crud/tradeOrder', '', 'C', '0', '1', 'quant:tradeOrder:view', '#', 'admin', '2023-11-09 23:28:03', '', NULL, '订单记录菜单');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2024, '订单记录查询', 2023, 1, '#', '', 'F', '0', '1', 'quant:tradeOrder:list', '#', 'admin', '2023-11-09 23:28:03', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2025, '订单记录新增', 2023, 2, '#', '', 'F', '0', '1', 'quant:tradeOrder:add', '#', 'admin', '2023-11-09 23:28:03', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2026, '订单记录修改', 2023, 3, '#', '', 'F', '0', '1', 'quant:tradeOrder:edit', '#', 'admin', '2023-11-09 23:28:03', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2027, '订单记录删除', 2023, 4, '#', '', 'F', '0', '1', 'quant:tradeOrder:remove', '#', 'admin', '2023-11-09 23:28:03', '', NULL, '');
INSERT INTO `sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2028, '订单记录导出', 2023, 5, '#', '', 'F', '0', '1', 'quant:tradeOrder:export', '#', 'admin', '2023-11-09 23:28:03', '', NULL, '');
COMMIT;

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) DEFAULT NULL COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
BEGIN;
INSERT INTO `sys_notice` (`notice_id`, `notice_title`, `notice_type`, `notice_content`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '管理员');
INSERT INTO `sys_notice` (`notice_id`, `notice_title`, `notice_type`, `notice_content`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '管理员');
COMMIT;

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint DEFAULT '0' COMMENT '消耗时间',
  PRIMARY KEY (`oper_id`),
  KEY `idx_sys_oper_log_bt` (`business_type`),
  KEY `idx_sys_oper_log_s` (`status`),
  KEY `idx_sys_oper_log_ot` (`oper_time`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8mb3 COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (100, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"116\"],\"parentId\":[\"3\"],\"menuType\":[\"C\"],\"menuName\":[\"系统接口\"],\"url\":[\"/tool/swagger\"],\"target\":[\"menuItem\"],\"perms\":[\"tool:swagger:view\"],\"orderNum\":[\"3\"],\"icon\":[\"fa fa-gg\"],\"visible\":[\"1\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-10-28 19:06:24', 37);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (101, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"4\"],\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"若依官网\"],\"url\":[\"http://ruoyi.vip\"],\"target\":[\"menuBlank\"],\"perms\":[\"\"],\"orderNum\":[\"4\"],\"icon\":[\"fa fa-location-arrow\"],\"visible\":[\"1\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-10-28 19:06:34', 14);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (102, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"111\"],\"parentId\":[\"2\"],\"menuType\":[\"C\"],\"menuName\":[\"数据监控\"],\"url\":[\"/monitor/data\"],\"target\":[\"menuItem\"],\"perms\":[\"monitor:data:view\"],\"orderNum\":[\"3\"],\"icon\":[\"fa fa-bug\"],\"visible\":[\"1\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-10-28 19:21:02', 30);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (103, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"dfcf_trade\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-10-29 11:34:35', 110);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (104, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"东方财富\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"10\"],\"icon\":[\"fa fa-bank\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-10-29 11:36:05', 9);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (105, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"dfcf_trade\"],\"tableComment\":[\"东方财富交易界面\"],\"className\":[\"DfcfTrade\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"账号id\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"accountId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].isEdit\":[\"1\"],\"columns[0].isList\":[\"1\"],\"columns[0].isQuery\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"textarea\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"密码\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"password\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"验证秘钥\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"validateKey\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"cookie缓存\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"cookie\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"账户总额\"],\"columns[4].javaType\":[\"BigDecimal\"],\"columns[4].javaField\":[\"totalAmount\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"可', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-10-29 11:36:21', 29);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (106, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"dfcf_trade\"],\"tableComment\":[\"东方财富交易界面\"],\"className\":[\"DfcfTrade\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"账号id\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"accountId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].isEdit\":[\"1\"],\"columns[0].isList\":[\"1\"],\"columns[0].isQuery\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"textarea\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"密码\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"password\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"验证秘钥\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"validateKey\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"cookie缓存\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"cookie\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"账户总额\"],\"columns[4].javaType\":[\"BigDecimal\"],\"columns[4].javaField\":[\"totalAmount\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"可', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-10-29 11:36:49', 33);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (107, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/dfcf_trade', '127.0.0.1', '内网IP', '\"dfcf_trade\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-10-29 11:36:52', 143);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (108, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"dfcf_trade\"],\"tableComment\":[\"东方财富交易界面\"],\"className\":[\"DfcfTrade\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"账号id\"],\"columns[0].javaType\":[\"String\"],\"columns[0].javaField\":[\"accountId\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].isEdit\":[\"1\"],\"columns[0].isList\":[\"1\"],\"columns[0].isQuery\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"textarea\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"密码\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"password\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"验证秘钥\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"validateKey\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"cookie缓存\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"cookie\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"账户总额\"],\"columns[4].javaType\":[\"BigDecimal\"],\"columns[4].javaField\":[\"totalAmount\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"可', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-10-29 11:45:34', 33);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (109, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/dfcf_trade', '127.0.0.1', '内网IP', '\"dfcf_trade\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-10-29 11:45:40', 138);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (110, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '研发部门', '/tool/gen/synchDb/dfcf_trade', '127.0.0.1', '内网IP', '\"dfcf_trade\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-10-29 21:55:28', 95);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (111, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"股票管理\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"200\"],\"icon\":[\"fa fa-database\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 19:27:14', 31);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (112, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"2008\"],\"menuType\":[\"C\"],\"menuName\":[\"股票列表\"],\"url\":[\"/quant/stocks\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"10\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 19:27:36', 19);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (113, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"2008\"],\"menuType\":[\"C\"],\"menuName\":[\"行情面板\"],\"url\":[\"/panel\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"20\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 19:28:18', 29);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (114, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"dfcf_trade_order\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 22:46:36', 109);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (115, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"dfcf_trade_view\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 22:46:58', 68);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (116, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"3\"],\"tableName\":[\"dfcf_trade_view\"],\"tableComment\":[\"回测订单分析图\"],\"className\":[\"DfcfTradeView\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"27\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"28\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"代码\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"code\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"29\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"最大收益\"],\"columns[2].javaType\":[\"BigDecimal\"],\"columns[2].javaField\":[\"maxProfit\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"30\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"最大回撤\"],\"columns[3].javaType\":[\"BigDecimal\"],\"columns[3].javaField\":[\"maxCut\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"31\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"夏普率\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"sharp\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"32\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"创建时间\"],\"columns[5].javaType\":[\"Date\"],\"columns[5].javaField\":[\"createTime\"],\"columns[5].isI', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 22:57:53', 32);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (117, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"2\"],\"tableName\":[\"dfcf_trade_order\"],\"tableComment\":[\"订单记录\"],\"className\":[\"DfcfTradeOrder\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"10\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"11\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"买卖方向\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"direction\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"12\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"账号\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"accountId\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"13\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"代码\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"code\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"14\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"名称\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"name\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"LIKE\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"15\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"价格\"],\"columns[5].javaType\":[\"BigDecimal\"],\"columns[5].javaField\":[\"price\"],\"columns[5].isInsert\":[\"1\"],', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 22:58:49', 50);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (118, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"3\"],\"tableName\":[\"dfcf_trade_view\"],\"tableComment\":[\"回测订单分析图\"],\"className\":[\"DfcfTradeView\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"27\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"28\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"代码\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"code\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"29\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"最大收益\"],\"columns[2].javaType\":[\"BigDecimal\"],\"columns[2].javaField\":[\"maxProfit\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"30\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"最大回撤\"],\"columns[3].javaType\":[\"BigDecimal\"],\"columns[3].javaField\":[\"maxCut\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"31\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"夏普率\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"sharp\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"32\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"创建时间\"],\"columns[5].javaType\":[\"Date\"],\"columns[5].javaField\":[\"createTime\"],\"columns[5].isI', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 22:59:07', 97);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (119, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"2\"],\"tableName\":[\"dfcf_trade_order\"],\"tableComment\":[\"订单记录\"],\"className\":[\"DfcfTradeOrder\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"10\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"11\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"买卖方向\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"direction\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"12\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"账号\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"accountId\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"13\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"代码\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"code\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"14\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"名称\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"name\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"LIKE\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"15\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"价格\"],\"columns[5].javaType\":[\"BigDecimal\"],\"columns[5].javaField\":[\"price\"],\"columns[5].isInsert\":[\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 22:59:36', 85);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (120, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"2\"],\"tableName\":[\"dfcf_trade_order\"],\"tableComment\":[\"订单记录\"],\"className\":[\"DfcfTradeOrder\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"10\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"11\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"买卖方向\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"direction\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"12\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"账号\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"accountId\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"13\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"代码\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"code\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"14\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"名称\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"name\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"LIKE\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"15\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"价格\"],\"columns[5].javaType\":[\"BigDecimal\"],\"columns[5].javaField\":[\"price\"],\"columns[5].isInsert\":[\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:00:08', 77);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (121, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"2\"],\"tableName\":[\"dfcf_trade_order\"],\"tableComment\":[\"订单记录\"],\"className\":[\"DfcfTradeOrder\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"10\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"11\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"买卖方向\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"direction\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"12\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"账号\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"accountId\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"13\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"代码\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"code\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"14\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"名称\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"name\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"LIKE\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"15\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"价格\"],\"columns[5].javaType\":[\"BigDecimal\"],\"columns[5].javaField\":[\"price\"],\"columns[5].isInsert\":[\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:01:11', 54);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (122, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"3\"],\"tableName\":[\"dfcf_trade_view\"],\"tableComment\":[\"回测订单分析图\"],\"className\":[\"DfcfTradeView\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"27\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"28\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"代码\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"code\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"29\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"最大收益\"],\"columns[2].javaType\":[\"BigDecimal\"],\"columns[2].javaField\":[\"maxProfit\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"30\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"最大回撤\"],\"columns[3].javaType\":[\"BigDecimal\"],\"columns[3].javaField\":[\"maxCut\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"31\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"夏普率\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"sharp\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"32\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"创建时间\"],\"columns[5].javaType\":[\"Date\"],\"columns[5].javaField\":[\"createTime\"],\"columns[5].isI', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:01:23', 53);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (123, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":[\"dfcf_trade_view,dfcf_trade_order\"]}', NULL, 0, NULL, '2023-11-09 23:01:29', 223);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (124, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/dfcf_trade_order', '127.0.0.1', '内网IP', '\"dfcf_trade_order\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:01:49', 97);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (125, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/dfcf_trade_view', '127.0.0.1', '内网IP', '\"dfcf_trade_view\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:01:53', 108);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (126, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/dfcf_trade_view', '127.0.0.1', '内网IP', '\"dfcf_trade_view\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:02:06', 190);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (127, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"3\"],\"tableName\":[\"dfcf_trade_view\"],\"tableComment\":[\"回测订单分析图\"],\"className\":[\"DfcfTradeView\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"27\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"28\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"代码\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"code\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"29\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"最大收益\"],\"columns[2].javaType\":[\"BigDecimal\"],\"columns[2].javaField\":[\"maxProfit\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"30\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"最大回撤\"],\"columns[3].javaType\":[\"BigDecimal\"],\"columns[3].javaField\":[\"maxCut\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"31\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"夏普率\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"sharp\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"32\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"创建时间\"],\"columns[5].javaType\":[\"Date\"],\"columns[5].javaField\":[\"createTime\"],\"columns[5].isI', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:02:35', 70);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (128, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"2\"],\"tableName\":[\"dfcf_trade_order\"],\"tableComment\":[\"订单记录\"],\"className\":[\"DfcfTradeOrder\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"10\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"11\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"买卖方向\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"direction\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"12\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"账号\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"accountId\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"13\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"代码\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"code\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"14\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"名称\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"name\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"LIKE\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"15\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"价格\"],\"columns[5].javaType\":[\"BigDecimal\"],\"columns[5].javaField\":[\"price\"],\"columns[5].isInsert\":[\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:02:44', 82);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (129, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/dfcf_trade_view', '127.0.0.1', '内网IP', '\"dfcf_trade_view\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:02:54', 123);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (130, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/dfcf_trade_order', '127.0.0.1', '内网IP', '\"dfcf_trade_order\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:02:56', 101);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (131, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', '研发部门', '/tool/gen/synchDb/dfcf_trade_order', '127.0.0.1', '内网IP', '\"dfcf_trade_order\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:12:27', 64);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (132, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"2\"],\"tableName\":[\"dfcf_trade_order\"],\"tableComment\":[\"订单记录\"],\"className\":[\"DfcfTradeOrder\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"10\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"11\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"买卖方向\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"direction\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"12\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"账号\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"accountId\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"13\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"代码\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"code\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"14\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"名称\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"name\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"LIKE\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"15\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"价格\"],\"columns[5].javaType\":[\"BigDecimal\"],\"columns[5].javaField\":[\"price\"],\"columns[5].isInsert\":[\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:12:36', 47);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (133, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"3\"],\"tableName\":[\"dfcf_trade_view\"],\"tableComment\":[\"回测订单分析图\"],\"className\":[\"DfcfTradeView\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"27\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键id\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"28\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"代码\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"code\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"29\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"最大收益\"],\"columns[2].javaType\":[\"BigDecimal\"],\"columns[2].javaField\":[\"maxProfit\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"30\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"最大回撤\"],\"columns[3].javaType\":[\"BigDecimal\"],\"columns[3].javaField\":[\"maxCut\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"31\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"夏普率\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"sharp\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"32\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"创建时间\"],\"columns[5].javaType\":[\"Date\"],\"columns[5].javaField\":[\"createTime\"],\"columns[5].isI', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:12:47', 41);
INSERT INTO `sys_oper_log` (`oper_id`, `title`, `business_type`, `method`, `request_method`, `operator_type`, `oper_name`, `dept_name`, `oper_url`, `oper_ip`, `oper_location`, `oper_param`, `json_result`, `status`, `error_msg`, `oper_time`, `cost_time`) VALUES (134, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.genCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/genCode/dfcf_trade_order', '127.0.0.1', '内网IP', '\"dfcf_trade_order\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2023-11-09 23:12:53', 148);
COMMIT;

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` bigint NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
BEGIN;
INSERT INTO `sys_post` (`post_id`, `post_code`, `post_name`, `post_sort`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_post` (`post_id`, `post_code`, `post_name`, `post_sort`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_post` (`post_id`, `post_code`, `post_name`, `post_sort`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2023-10-28 18:28:39', '', NULL, '');
INSERT INTO `sys_post` (`post_id`, `post_code`, `post_name`, `post_sort`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2023-10-28 18:28:39', '', NULL, '');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb3 COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` (`role_id`, `role_name`, `role_key`, `role_sort`, `data_scope`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1, '超级管理员', 'admin', 1, '1', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '超级管理员');
INSERT INTO `sys_role` (`role_id`, `role_name`, `role_key`, `role_sort`, `data_scope`, `status`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2, '普通角色', 'common', 2, '2', '0', '0', 'admin', '2023-10-28 18:28:39', '', NULL, '普通角色');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_dept` (`role_id`, `dept_id`) VALUES (2, 100);
INSERT INTO `sys_role_dept` (`role_id`, `dept_id`) VALUES (2, 101);
INSERT INTO `sys_role_dept` (`role_id`, `dept_id`) VALUES (2, 105);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 2);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 3);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 4);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 100);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 101);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 102);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 103);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 104);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 105);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 106);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 107);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 108);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 109);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 110);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 111);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 112);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 113);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 114);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 115);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 116);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 500);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 501);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1000);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1001);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1002);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1003);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1004);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1005);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1006);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1007);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1008);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1009);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1010);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1011);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1012);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1013);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1014);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1015);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1016);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1017);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1018);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1019);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1020);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1021);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1022);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1023);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1024);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1025);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1026);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1027);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1028);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1029);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1030);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1031);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1032);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1033);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1034);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1035);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1036);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1037);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1038);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1039);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1040);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1041);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1042);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1043);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1044);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1045);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1046);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1047);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1048);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1049);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1050);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1051);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1052);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1053);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1054);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1055);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1056);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1057);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1058);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1059);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1060);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (2, 1061);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  `salt` varchar(20) DEFAULT '' COMMENT '盐加密',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `pwd_update_date` datetime DEFAULT NULL COMMENT '密码最后更新时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb3 COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` (`user_id`, `dept_id`, `login_name`, `user_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `salt`, `status`, `del_flag`, `login_ip`, `login_date`, `pwd_update_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2023-11-09 23:33:44', '2023-10-28 18:28:39', 'admin', '2023-10-28 18:28:39', '', '2023-11-09 23:33:43', '管理员');
INSERT INTO `sys_user` (`user_id`, `dept_id`, `login_name`, `user_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `salt`, `status`, `del_flag`, `login_ip`, `login_date`, `pwd_update_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '0', '127.0.0.1', '2023-10-28 18:28:39', '2023-10-28 18:28:39', 'admin', '2023-10-28 18:28:39', '', NULL, '测试员');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online` (
  `sessionId` varchar(50) NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int DEFAULT '0' COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='在线用户记录';

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_online` (`sessionId`, `login_name`, `dept_name`, `ipaddr`, `login_location`, `browser`, `os`, `status`, `start_timestamp`, `last_access_time`, `expire_time`) VALUES ('e6933721-dc88-4b3d-970c-26b529663d8f', 'admin', '研发部门', '127.0.0.1', '内网IP', 'Chrome 11', 'Mac OS X', 'on_line', '2023-11-09 22:46:20', '2023-11-09 23:35:00', 1800000);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_post` (`user_id`, `post_id`) VALUES (1, 1);
INSERT INTO `sys_user_post` (`user_id`, `post_id`) VALUES (2, 2);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (1, 1);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (2, 2);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
