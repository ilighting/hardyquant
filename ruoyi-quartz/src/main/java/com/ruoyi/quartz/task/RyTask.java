package com.ruoyi.quartz.task;

import cn.hutool.core.date.DateUtil;
import com.ruoyi.quant.domain.Stocks;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.AnalysisPipeService;
import com.ruoyi.quant.service.StockQuoteService;
import com.ruoyi.quant.service.StocksService;
import com.ruoyi.quant.tdx.TdxCategory;
import com.ruoyi.quant.trade.crud.service.IDfcfTradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ruoyi.common.utils.StringUtils;

import java.util.List;

/**
 * 定时任务调度测试
 * 
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{

    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams()
    {
        System.out.println("执行无参方法");
    }
    public void test(String accountId)
    {
        System.out.println(accountId);
    }

    @Autowired
    AnalysisPipeService analysisPipeService;
    @Autowired
    StockQuoteService quoteService;

    @Autowired
    StocksService stocksService;


    public void syncMinuteQuote(){
        Stocks stocks1 = new Stocks();
        stocks1.setCode("600519");
        List<Stocks> stocks = stocksService.listAll(stocks1);

        for (Stocks stock : stocks) {
            AnalysisQuery query = new AnalysisQuery();
            query.setCode(stock.getCode());
            query.setTdxCategory(TdxCategory.m1);
            query.setDate(DateUtil.now());
            analysisPipeService.updateMinute(query);
        }
    }

}
