package com.ruoyi.common.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 表格分页数据对象
 *
 * @author ruoyi
 */
public class PageAjax implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 总记录数 */
    private long total;

    /** 列表数据 */
    private List<?> rows;

    /** 消息状态码 */
    private Long code;

    /** 消息内容 */
    private String msg;

    /**
     * 表格数据对象
     */
    public PageAjax()
    {
    }

    /**
     * 分页
     *
     * @param list 列表数据
     * @param total 总记录数
     */
    public PageAjax(List<?> list, Long total)
    {
        this.code=0L;
        this.rows = list;
        this.total = total;
        this.msg="";
    }
    public PageAjax(int code,List<?> list, Long total)
    {
        this.code=0L;
        this.rows = list;
        this.total = total;
        this.msg="";
    }

	public static <T> PageAjax buildSuccess(final List<T> list, Long count) {
        return new PageAjax(list,count);
	}

	public static <T> PageAjax buildError() {
        return new PageAjax(500,new ArrayList<>(),0L);
	}

	public static <T> PageAjax buildSuccess(final List<T> list, Integer count) {
        return new PageAjax(list,count.longValue());
	}

	public long getTotal()
    {
        return total;
    }

    public void setTotal(long total)
    {
        this.total = total;
    }

    public List<?> getRows()
    {
        return rows;
    }

    public void setRows(List<?> rows)
    {
        this.rows = rows;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(final Long code) {
        this.code = code;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }
}
