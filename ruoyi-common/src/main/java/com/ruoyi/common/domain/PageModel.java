package com.ruoyi.common.domain;

import java.time.LocalDateTime;

public abstract class PageModel implements IPageModel
{

	private String tableDesc;

	private static final long serialVersionUID = 1L;
	private Integer pageNum;
	private Integer pageSize;

	private String updateBy;
	private LocalDateTime updateTime;

	private String createBy;
	private LocalDateTime createTime;

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(final Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(final Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getTableDesc() {
		return tableDesc;
	}

	public void setTableDesc(String tableDesc) {
		this.tableDesc = tableDesc;
	}

	protected abstract String setTableDesc();
	@Override
	public String tableDesc() {
		return setTableDesc();
	}
}
