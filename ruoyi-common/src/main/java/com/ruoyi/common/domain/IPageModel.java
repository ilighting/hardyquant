package com.ruoyi.common.domain;

import java.io.Serializable;


public interface IPageModel extends Serializable {
    String tableDesc();
}
