package com.ruoyi.common.events;


public class JobEvent implements CommonEvent {

    private String jobName;
    private String accountId;

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Override
    public String toString() {
        return "JobEvent{" +
                "jobName='" + jobName + '\'' +
                '}';
    }
}
