package com.ruoyi.common.events;

public class LoginEvent implements CommonEvent {
    private String yzmCode;

    private String jobName;
    private String accountId;

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getYzmCode() {
        return yzmCode;
    }

    public void setYzmCode(String yzmCode) {
        this.yzmCode = yzmCode;
    }

    @Override
    public String toString() {
        return "LoginEvent{" +
                "yzmCode='" + yzmCode + '\'' +
                ", jobName='" + jobName + '\'' +
                ", accountId='" + accountId + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
