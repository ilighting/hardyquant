package com.ruoyi.common.utils.math;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

public class SharpUtil {


    public static double calculate(double[] returns, double riskFreeRate) {
        double averageReturn = StatUtils.mean(returns);
        double portfolioRisk = new StandardDeviation().evaluate(returns);
        double excessReturn = averageReturn - riskFreeRate;
        return excessReturn / portfolioRisk;
    }


}

