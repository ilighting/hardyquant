//package com.ruoyi.common.config;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class EasyTransMybatisPlusConfig {
//	/*** service的包路径*/
//	@Value("${easy-trans.autotrans.package}")
//	private String packageNames;
//
//	@Bean
//	@ConditionalOnProperty(name = "easy-trans.is-enable-auto", havingValue = "true")
//	public MybatisPlusTransableRegister mybatisPlusTransableRegister() {
//		MybatisPlusTransableRegister result = new MybatisPlusTransableRegister(packageNames);
//		return result;
//	}
//}
