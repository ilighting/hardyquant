//package com.ruoyi.common.config;
//
//import com.baomidou.mybatisplus.core.enums.SqlMethod;
//import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
//import com.fhs.core.trans.vo.VO;
//import com.fhs.trans.service.AutoTransable;
//import org.apache.ibatis.session.SqlSession;
//
//public class MybatisPlusTransableAdapter implements AutoTransable {
//	private Class<? extends VO> voClass;
//
//	public MybatisPlusTransableAdapter(Class<? extends VO> voClass) {
//		this.voClass = voClass;
//	}
//
//	@Override
//	public VO selectById(Object primaryValue) {
//		SqlSession sqlSession = this.sqlSession();
//		VO result;
//		try {
//			result = (VO) sqlSession.selectOne(this.sqlStatement(SqlMethod.SELECT_BY_ID), primaryValue);
//		} finally {
//			sqlSession.close();
//		}
//		return result;
//	}
//
//	protected SqlSession sqlSession() {
//		return SqlHelper.sqlSession(this.voClass);
//	}
//
//	protected String sqlStatement(SqlMethod sqlMethod) {
//		return this.sqlStatement(SqlMethod.valueOf(sqlMethod.getMethod()));
//	}
//}
