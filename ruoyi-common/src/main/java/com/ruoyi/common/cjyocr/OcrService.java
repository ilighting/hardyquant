package com.ruoyi.common.cjyocr;

public interface OcrService {

    String process(String url);

}
