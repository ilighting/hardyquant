// 创建axios实例并设置统一的请求配置
const http = axios.create({
    baseURL: 'http://localhost:8088', // 设置基准URL
    headers: {
        'Content-Type': 'application/json' // 设置默认请求头
    }
});
