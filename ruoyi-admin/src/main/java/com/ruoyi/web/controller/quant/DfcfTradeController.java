package com.ruoyi.web.controller.quant;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.quant.trade.api.TradeResultVo;
import com.ruoyi.quant.trade.api.request.BaseTradeRequest;
import com.ruoyi.quant.trade.api.response.GetAssetsResponse;
import com.ruoyi.quant.trade.api.response.GetStockListResponse;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeUser;
import com.ruoyi.quant.trade.crud.service.IDfcfTradeService;
import com.ruoyi.quant.trade.model.po.TradeMethod;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 东方财富交易界面Controller
 *
 * @author ruoyi
 * @date 2023-10-29
 */
@Controller
@RequestMapping("/trade/apiTrade")
public class DfcfTradeController extends BaseController {
    private String prefix = "trade/apiTrade";

    @Autowired
    private IDfcfTradeService dfcfTradeService;

    @RequiresPermissions("trade:apiTrade:view")
    @GetMapping()
    public String apiTrade() {
        return prefix + "/apiTrade";
    }

    /**
     * 查询东方财富交易界面列表
     */
    @RequiresPermissions("trade:apiTrade:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DfcfTradeUser dfcfTradeUser) {
        startPage();
        List<DfcfTradeUser> list = dfcfTradeService.selectDfcfTradeList(dfcfTradeUser);
        return getDataTable(list);
    }

    /**
     * 导出东方财富交易界面列表
     */
    @RequiresPermissions("trade:apiTrade:export")
    @Log(title = "东方财富交易界面", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DfcfTradeUser dfcfTradeUser) {
        List<DfcfTradeUser> list = dfcfTradeService.selectDfcfTradeList(dfcfTradeUser);
        ExcelUtil<DfcfTradeUser> util = new ExcelUtil<DfcfTradeUser>(DfcfTradeUser.class);
        return util.exportExcel(list, "东方财富交易界面数据");
    }

    /**
     * 新增东方财富交易界面
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增东方财富交易界面
     */
    @GetMapping("/login")
    public String login() {
        return prefix + "/dcLogin";
    }

    /**
     * 新增东方财富交易界面
     */

    @PostMapping(value = "/doLogin")
    @ResponseBody
    public AjaxResult doLogin(DfcfTradeUser dfcfTradeUser) {
        String message = dfcfTradeService.loginDfcfApi(dfcfTradeUser);
        return AjaxResult.success(message);
    }

    @GetMapping(value = "/getPositionList")
    @ResponseBody
    public TradeResultVo<?> getPositionList(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeService.userPositionList(dfcfTradeUser);
    }

    @GetMapping(value = "/getAssetList")
    @ResponseBody
    public TradeResultVo<?> getAssetList(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeService.userAssetsList(dfcfTradeUser);
    }

    @GetMapping(value = "/getUserOrderList")
    @ResponseBody
    public TradeResultVo<?> getUserOrdersList(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeService.userOrdersList(dfcfTradeUser);
    }

    @GetMapping(value = "/getUserDealList")
    @ResponseBody
    public TradeResultVo<?> getUserDealList(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeService.userDealList(dfcfTradeUser);
    }

    @GetMapping(value = "/getUserHisDealList")
    @ResponseBody
    public TradeResultVo<?> getuserHisDealData(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeService.userHisDealData(dfcfTradeUser);
    }

    @GetMapping(value = "/getUserHisOrders")
    @ResponseBody
    public TradeResultVo<?> getUserHisOrders(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeService.userHisOrders(dfcfTradeUser);
    }

    @GetMapping(value = "/getNewIpoList")
    @ResponseBody
    public TradeResultVo<?> getNewIpoList(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeService.newIpoList(dfcfTradeUser);
    }

    @GetMapping(value = "/getNewIpoList2")
    @ResponseBody
    public TradeResultVo<?> getNewIpoList2(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeService.newIpoList2(dfcfTradeUser);
    }

    @GetMapping(value = "/ipoBatchTake")
    @ResponseBody
    public TradeResultVo<?> ipoBatchTake(DfcfTradeUser dfcfTradeUser) {
        return dfcfTradeService.batchIpo(dfcfTradeUser);
    }

    @GetMapping("/queryVerifyCodeUrl")
    @ResponseBody
    public AjaxResult queryVerifyCodeUrl() {
        TradeMethod tradeMethod = dfcfTradeService.getTradeMethodByName(BaseTradeRequest.TradeRequestMethod.YZM);
        String url = tradeMethod.getUrl();
        return AjaxResult.success(url);
    }


    /**
     * 新增保存东方财富交易界面
     */
    @RequiresPermissions("trade:apiTrade:add")
    @Log(title = "东方财富交易界面", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DfcfTradeUser dfcfTradeUser) {
        return toAjax(dfcfTradeService.insertDfcfTrade(dfcfTradeUser));
    }

    /**
     * 修改东方财富交易界面
     */
    @RequiresPermissions("trade:apiTrade:edit")
    @GetMapping("/edit/{accountId}")
    public String edit(@PathVariable("accountId") String accountId, ModelMap mmap) {
        DfcfTradeUser dfcfTradeUser = dfcfTradeService.selectDfcfTradeByAccountId(accountId);
        mmap.put("dfcfTrade", dfcfTradeUser);
        return prefix + "/edit";
    }

    /**
     * 修改保存东方财富交易界面
     */
    @RequiresPermissions("trade:apiTrade:edit")
    @Log(title = "东方财富交易界面", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DfcfTradeUser dfcfTradeUser) {
        return toAjax(dfcfTradeService.updateDfcfTrade(dfcfTradeUser));
    }

    /**
     * 删除东方财富交易界面
     */
    @RequiresPermissions("trade:apiTrade:remove")
    @Log(title = "东方财富交易界面", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(dfcfTradeService.deleteDfcfTradeByAccountIds(ids));
    }
}
