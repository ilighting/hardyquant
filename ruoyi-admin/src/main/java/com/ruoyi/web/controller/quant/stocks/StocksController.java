package com.ruoyi.web.controller.quant.stocks;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;

import com.ruoyi.common.domain.PageAjax;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.quant.domain.Stocks;
import com.ruoyi.quant.service.StocksService;
import com.ruoyi.quant.utils.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/quant/stocks")
public class StocksController extends BaseController
{
    private String prefix = "quant/stocks";

    @Autowired
    StocksService stocksService;

    @GetMapping()
    public String stocks(ModelMap map)
    {
        map.put("titleName","股票列表");
        return prefix + "/stocks";
    }


    @PostMapping("/list")
    @ResponseBody
    public PageAjax list(Stocks stocks) {
        PageAjax pageAjax = stocksService.pageList(stocks);
        return pageAjax;
    }


    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Stocks stocks) {
        List<Stocks> stocksList = stocksService.listAll(stocks);
        ExcelUtil<Stocks> util = new ExcelUtil<Stocks>(Stocks.class);
        return util.exportExcel(stocksList, "股票列表");
    }

    /**
     * 新增参数配置
     */
//    @GetMapping("/add")
//    public String add() {
//        return prefix + "/add";
//    }
 /**
     * 新增参数配置
     */
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult add(String id) {

        return AjaxResult.success("加入自选成功");
    }

    /**
     * 新增保存参数配置
     */

    @PostMapping("/addOrUpdate")
    @ResponseBody
    public AjaxResult addOrUpdate(@Validated Stocks stocks) {

        int i = stocksService.addOrUpdate(stocks);
        return AjaxResult.onResult(i);
    }

    /**
     * 修改参数配置
     */
    @GetMapping("/edit/{configId}")
    public String edit(@PathVariable("configId") Long configId, ModelMap mmap) {
        Stocks stocks=new Stocks();
        stocks.setId(configId.toString());
        mmap.put("config", stocksService.getOne(configId));
        return prefix + "/edit";
    }
    /**
     * 删除参数配置
     */

    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        Long b = stocksService.removeBath(Convert.toStrList(ids),Stocks.class);
        return AjaxResult.onResult(b);
    }

}
