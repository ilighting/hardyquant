package com.ruoyi.web.controller.quant.tamodel;

import com.ruoyi.common.domain.PageAjax;
import com.ruoyi.quant.domain.models.DayTaModel;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.mg.MgSort;
import com.ruoyi.quant.pipe.AnalysisPipeService;
import com.ruoyi.quant.service.CommonTaService;
import com.ruoyi.quant.tdx.TdxCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/taModels")
public class TaModelController {

	@Autowired
	CommonTaService<DayTaModel> commonTaService;

	@Autowired
	AnalysisPipeService analysisPipeService;

	@GetMapping("/getTaModel")
	@ResponseBody
	public PageAjax getTaModel(DayTaModel taModel) {
		MgSort sort = new MgSort();
		sort.put("date", 1);
		String stockCode = taModel.getStockCode();
		taModel.setCode(stockCode);
		List<DayTaModel> list = commonTaService.pageList(taModel, taModel.getPageNum(),taModel.getPageSize());
		if(list.size()>0){
			return PageAjax.buildSuccess(list, list.size());
		}else{
			AnalysisQuery query=new AnalysisQuery();
			query.setCode(taModel.getCode());
			query.setTdxCategory(TdxCategory.day);
			query.setTalibName(taModel.getType());
			query.setMgSort(sort);
			analysisPipeService.workDay(query);
		}


		return PageAjax.buildError();
	}

	@GetMapping("/adSoc")
	public ModelAndView getAdSoc() {
		var code = "600100";
		ModelAndView view = new ModelAndView();
//		long count = commonTaService.count(q);
		MgSort sort = new MgSort();
		sort.put("date", 1);
		List<DayTaModel> list = commonTaService.listAll("code", code, sort, DayTaModel.class);
		List<String> dates = list.stream().map(item -> item.getDate()).collect(Collectors.toList());
		List<Double> closePrices = list.stream().map(item -> item.getClose()).collect(Collectors.toList());
		List<Double> adOscList = list.stream().map(item -> item.getVal() / 1000000).collect(Collectors.toList());

		view.addObject("dates", dates);
		view.addObject("closePrices", closePrices);
		view.addObject("adOscList", adOscList);
		view.addObject("stockName", "同方股份");
		view.addObject("code", code);
		view.setViewName("quant/tamodel/index.html");
		return view;
	}

	@GetMapping("/getCci")
	public ModelAndView getCci() {
		var code = "002176";
		ModelAndView view = new ModelAndView();

//		long count = commonTaService.count(q);
		MgSort sort = new MgSort();
		sort.put("date", 1);
		List<DayTaModel> list = commonTaService.listAll("code", code, sort, DayTaModel.class);
		List<String> dates = list.stream().map(item -> item.getDate()).collect(Collectors.toList());
		List<Double> closePrices = list.stream().map(item -> item.getClose()).collect(Collectors.toList());
		List<Double> adOscList = list.stream().map(item -> item.getVal() / item.getClose()).collect(Collectors.toList());

		view.addObject("dates", dates);
		view.addObject("closePrices", closePrices);
		view.addObject("adOscList", adOscList);
		view.addObject("stockName", "江特电机");
		view.addObject("code", code);
		view.setViewName("quant/tamodel/index.html");
		return view;
	}


}
