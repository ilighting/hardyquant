package com.ruoyi.web.controller.quant;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeView;
import com.ruoyi.quant.trade.crud.service.IDfcfTradeViewService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 回测订单分析图Controller
 *
 * @author ruoyi
 * @date 2023-11-09
 */
@Controller
@RequestMapping("/quant/tradeView")
public class DfcfTradeViewController extends BaseController
{
    private String prefix = "quant/tradeView";

    @Autowired
    private IDfcfTradeViewService dfcfTradeViewService;

    @RequiresPermissions("quant:tradeView:view")
    @GetMapping()
    public String tradeView()
    {
        return prefix + "/tradeView";
    }

    /**
     * 查询回测订单分析图列表
     */
    @RequiresPermissions("quant:tradeView:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DfcfTradeView dfcfTradeView)
    {
        startPage();
        List<DfcfTradeView> list = dfcfTradeViewService.selectDfcfTradeViewList(dfcfTradeView);
        return getDataTable(list);
    }

    /**
     * 导出回测订单分析图列表
     */
    @RequiresPermissions("quant:tradeView:export")
    @Log(title = "回测订单分析图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DfcfTradeView dfcfTradeView)
    {
        List<DfcfTradeView> list = dfcfTradeViewService.selectDfcfTradeViewList(dfcfTradeView);
        ExcelUtil<DfcfTradeView> util = new ExcelUtil<DfcfTradeView>(DfcfTradeView.class);
        return util.exportExcel(list, "回测订单分析图数据");
    }

    /**
     * 新增回测订单分析图
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存回测订单分析图
     */
    @RequiresPermissions("quant:tradeView:add")
    @Log(title = "回测订单分析图", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DfcfTradeView dfcfTradeView)
    {
        return toAjax(dfcfTradeViewService.insertDfcfTradeView(dfcfTradeView));
    }

    /**
     * 修改回测订单分析图
     */
    @RequiresPermissions("quant:tradeView:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        DfcfTradeView dfcfTradeView = dfcfTradeViewService.selectDfcfTradeViewById(id);
        mmap.put("dfcfTradeView", dfcfTradeView);
        return prefix + "/edit";
    }

    /**
     * 修改保存回测订单分析图
     */
    @RequiresPermissions("quant:tradeView:edit")
    @Log(title = "回测订单分析图", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DfcfTradeView dfcfTradeView)
    {
        return toAjax(dfcfTradeViewService.updateDfcfTradeView(dfcfTradeView));
    }

    /**
     * 删除回测订单分析图
     */
    @RequiresPermissions("quant:tradeView:remove")
    @Log(title = "回测订单分析图", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(dfcfTradeViewService.deleteDfcfTradeViewByIds(ids));
    }
}
