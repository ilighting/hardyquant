package com.ruoyi.web.controller.quant;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.quant.basedata.domain.StockCode;
import com.ruoyi.quant.basedata.service.IStockCodeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.servlet.ModelAndView;

/**
 * 股票列表Controller
 * 
 * @author ruoyi
 * @date 2023-11-28
 */
@Controller
@RequestMapping("/quant/stockCode")
public class StockCodeController extends BaseController
{
    private String prefix = "quant/stockCode";

    @Autowired
    private IStockCodeService stockCodeService;

    @RequiresPermissions("quant:stockCode:view")
    @GetMapping()
    public String stockCode()
    {
        return prefix + "/stockCode";
    }

    @RequiresPermissions("quant:stockCode:view")
    @GetMapping("/details")
    public ModelAndView detailsStockCode(String codes)
    {
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("codes",codes);
        modelAndView.setViewName(prefix + "/detailsStockCode");
        return modelAndView;
    }

    /**
     * 查询股票列表列表
     */
    @RequiresPermissions("quant:stockCode:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StockCode stockCode)
    {
        startPage();
        List<StockCode> list = stockCodeService.selectStockCodeList(stockCode);
        return getDataTable(list);
    }

    /**
     * 导出股票列表列表
     */
    @RequiresPermissions("quant:stockCode:export")
    @Log(title = "股票列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StockCode stockCode)
    {
        List<StockCode> list = stockCodeService.selectStockCodeList(stockCode);
        ExcelUtil<StockCode> util = new ExcelUtil<StockCode>(StockCode.class);
        return util.exportExcel(list, "股票列表数据");
    }

    /**
     * 新增股票列表
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存股票列表
     */
    @RequiresPermissions("quant:stockCode:add")
    @Log(title = "股票列表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StockCode stockCode)
    {
        return toAjax(stockCodeService.insertStockCode(stockCode));
    }

    /**
     * 修改股票列表
     */
    @RequiresPermissions("quant:stockCode:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StockCode stockCode = stockCodeService.selectStockCodeById(id);
        mmap.put("stockCode", stockCode);
        return prefix + "/edit";
    }

    /**
     * 修改保存股票列表
     */
    @RequiresPermissions("quant:stockCode:edit")
    @Log(title = "股票列表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StockCode stockCode)
    {
        return toAjax(stockCodeService.updateStockCode(stockCode));
    }

    /**
     * 删除股票列表
     */
    @RequiresPermissions("quant:stockCode:remove")
    @Log(title = "股票列表", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(stockCodeService.deleteStockCodeByIds(ids));
    }
}
