package com.ruoyi.web.controller.quant;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeOrder;
import com.ruoyi.quant.trade.crud.service.IDfcfTradeOrderService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单记录Controller
 *
 * @author ruoyi
 * @date 2023-11-09
 */
@Controller
@RequestMapping("/crud/tradeOrder")
public class DfcfTradeOrderController extends BaseController
{
    private String prefix = "crud/tradeOrder";

    @Autowired
    private IDfcfTradeOrderService dfcfTradeOrderService;

    @RequiresPermissions("crud:tradeOrder:view")
    @GetMapping()
    public String tradeOrder()
    {
        return prefix + "/tradeOrder";
    }

    /**
     * 查询订单记录列表
     */
    @RequiresPermissions("crud:tradeOrder:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DfcfTradeOrder dfcfTradeOrder)
    {
        startPage();
        List<DfcfTradeOrder> list = dfcfTradeOrderService.selectDfcfTradeOrderList(dfcfTradeOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单记录列表
     */
    @RequiresPermissions("crud:tradeOrder:export")
    @Log(title = "订单记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DfcfTradeOrder dfcfTradeOrder)
    {
        List<DfcfTradeOrder> list = dfcfTradeOrderService.selectDfcfTradeOrderList(dfcfTradeOrder);
        ExcelUtil<DfcfTradeOrder> util = new ExcelUtil<DfcfTradeOrder>(DfcfTradeOrder.class);
        return util.exportExcel(list, "订单记录数据");
    }

    /**
     * 新增订单记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存订单记录
     */
    @RequiresPermissions("crud:tradeOrder:add")
    @Log(title = "订单记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DfcfTradeOrder dfcfTradeOrder)
    {
        return toAjax(dfcfTradeOrderService.insertDfcfTradeOrder(dfcfTradeOrder));
    }

    /**
     * 修改订单记录
     */
    @RequiresPermissions("crud:tradeOrder:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        DfcfTradeOrder dfcfTradeOrder = dfcfTradeOrderService.selectDfcfTradeOrderById(id);
        mmap.put("dfcfTradeOrder", dfcfTradeOrder);
        return prefix + "/edit";
    }

    /**
     * 修改保存订单记录
     */
    @RequiresPermissions("crud:tradeOrder:edit")
    @Log(title = "订单记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DfcfTradeOrder dfcfTradeOrder)
    {
        return toAjax(dfcfTradeOrderService.updateDfcfTradeOrder(dfcfTradeOrder));
    }

    /**
     * 删除订单记录
     */
    @RequiresPermissions("crud:tradeOrder:remove")
    @Log(title = "订单记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(dfcfTradeOrderService.deleteDfcfTradeOrderByIds(ids));
    }

}
