package com.ruoyi.web.controller.quant;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.quant.basedata.domain.StockConcept;
import com.ruoyi.quant.basedata.service.IStockConceptService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 板块信息Controller
 * 
 * @author ruoyi
 * @date 2023-11-29
 */
@Controller
@RequestMapping("/quant/stockConcept")
public class StockConceptController extends BaseController
{
    private String prefix = "quant/stockConcept";

    @Autowired
    private IStockConceptService stockConceptService;

    @RequiresPermissions("quant:stockConcept:view")
    @GetMapping()
    public String stockConcept()
    {
        return prefix + "/stockConcept";
    }

    /**
     * 查询板块信息列表
     */
    @RequiresPermissions("quant:stockConcept:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StockConcept stockConcept)
    {
        startPage();
        List<StockConcept> list = stockConceptService.selectStockConceptList(stockConcept);
        return getDataTable(list);
    }

    /**
     * 导出板块信息列表
     */
    @RequiresPermissions("quant:stockConcept:export")
    @Log(title = "板块信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StockConcept stockConcept)
    {
        List<StockConcept> list = stockConceptService.selectStockConceptList(stockConcept);
        ExcelUtil<StockConcept> util = new ExcelUtil<StockConcept>(StockConcept.class);
        return util.exportExcel(list, "板块信息数据");
    }

    /**
     * 新增板块信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存板块信息
     */
    @RequiresPermissions("quant:stockConcept:add")
    @Log(title = "板块信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StockConcept stockConcept)
    {
        return toAjax(stockConceptService.insertStockConcept(stockConcept));
    }

    /**
     * 修改板块信息
     */
    @RequiresPermissions("quant:stockConcept:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StockConcept stockConcept = stockConceptService.selectStockConceptById(id);
        mmap.put("stockConcept", stockConcept);
        return prefix + "/edit";
    }

    /**
     * 修改保存板块信息
     */
    @RequiresPermissions("quant:stockConcept:edit")
    @Log(title = "板块信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StockConcept stockConcept)
    {
        return toAjax(stockConceptService.updateStockConcept(stockConcept));
    }

    /**
     * 删除板块信息
     */
    @RequiresPermissions("quant:stockConcept:remove")
    @Log(title = "板块信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(stockConceptService.deleteStockConceptByIds(ids));
    }
}
