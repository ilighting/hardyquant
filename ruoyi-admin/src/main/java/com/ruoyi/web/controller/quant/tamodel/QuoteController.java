package com.ruoyi.web.controller.quant.tamodel;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.domain.PageAjax;
import com.ruoyi.quant.domain.MinuteQuote;
import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.domain.models.DayTaModel;
import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.mg.MgSort;
import com.ruoyi.quant.pipe.AnalysisPipeService;
import com.ruoyi.quant.service.CommonTaService;
import com.ruoyi.quant.service.StockQuoteService;
import com.ruoyi.quant.tdx.TdxCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/quant/quote")
public class QuoteController {

	@Autowired
	StockQuoteService quoteService;
	@Autowired
	StockQuoteService<MinuteQuote> minuteQuoteStockQuoteService;

	@Autowired
	AnalysisPipeService analysisPipeService;

	@Autowired
	CommonTaService<DayTaModel> taModelService;

	@PostMapping("/pageList")
	@ResponseBody
	public PageAjax quoteList(StockQuote quote) {
		PageAjax pageAjax = quoteService.pageList(quote);
		return pageAjax;
	}

	@PostMapping("/list")
	@ResponseBody
	public PageAjax quoteAll(@RequestBody StockQuote quote) {
		String replace = quote.getStockCode();
		quote.setCode(replace);
		List<StockQuote> stockQuotes = quoteService.listAll(quote, new MgSort());
		if (stockQuotes.isEmpty()) {
			AnalysisQuery query = new AnalysisQuery();
			query.setCode(quote.getCode());
			query.setTdxCategory(TdxCategory.day);
			analysisPipeService.quoteUpdate(query);
			List<StockQuote> again = quoteService.listAll(quote, new MgSort());
			PageAjax pageAjax = PageAjax.buildSuccess(again, stockQuotes.size());
			return pageAjax;
		}

		PageAjax pageAjax = PageAjax.buildSuccess(stockQuotes, stockQuotes.size());
		return pageAjax;
	}

	@PostMapping("/minuteList")
	@ResponseBody
	public PageAjax minuteAll(@RequestBody MinuteQuote quote) {
        String replace = quote.getStockCode();
        quote.setCode(replace);
		String date = quote.getDate();



		// 使用 Instant 解析时间戳
		Instant instant = Instant.ofEpochMilli(Long.parseLong(date));

		// 获取默认时区
		ZoneId zoneId = ZoneId.systemDefault();

		// 使用 ZonedDateTime 将 Instant 转换为指定时区的日期时间
		ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(instant, zoneId);

		// 转换为 LocalDateTime
		LocalDateTime localDateTime = zonedDateTime.toLocalDateTime();

		System.out.println("Timestamp: " + localDateTime);


		DayOfWeek dayOfWeek = localDateTime.getDayOfWeek();
		int i = dayOfWeek.getValue() - 5;
		String dateStr="";
		if (i<0) {
			LocalDateTime dateTime = LocalDateTime.now();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
			String formattedDate = dateTime.format(formatter);
			dateStr=formattedDate;
		}
		if(i>0){
			LocalDateTime twoDaysAgo = localDateTime.minusDays(i);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
			String formattedDate = twoDaysAgo.format(formatter);
			dateStr=formattedDate;
		}
		quote.setDate("^"+dateStr);

		List<MinuteQuote> stockQuotes = quoteService.listAll(quote, new MgSort());
		if (stockQuotes.isEmpty()) {
			AnalysisQuery query = new AnalysisQuery();
			query.setCode(quote.getCode());
			query.setTdxCategory(TdxCategory.m1);
			analysisPipeService.updateMinute(query);
			List<MinuteQuote> again = quoteService.listAll(quote, new MgSort());
			PageAjax pageAjax = PageAjax.buildSuccess(again, stockQuotes.size());
			return pageAjax;
		}

        PageAjax pageAjax = PageAjax.buildSuccess(stockQuotes, stockQuotes.size());
        return pageAjax;
    }



	@PostMapping("/models")
	@ResponseBody
	public AjaxResult models(@RequestBody StockQuote quote) {
		List<String> models=new ArrayList<>(){{
			add("talib-adsoc");
			add("talib-ad");
			add("talib-boll");
			add("talib-cci");
			add("talib-obv");
			add("talib-macd");
		}};
		Map<String,Object> queryParam=new HashMap<>();
		queryParam.put("code",quote.getCode());
		queryParam.put("type","");
		List<DayTaModel> byCode= taModelService.listAll(queryParam, new MgSort(), DayTaModel.class);

		Map<String, List<DayTaModel>> collect = byCode.stream().collect(Collectors.groupingBy(DayTaModel::getType, Collectors.mapping(value -> value, Collectors.toList())));
		return AjaxResult.success(collect);
	}

}
