package com.ruoyi.web.controller.quant.tamodel;

import com.ruoyi.common.domain.PageAjax;
import com.ruoyi.quant.domain.Stocks;
import com.ruoyi.quant.service.StocksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/panel")
public class PanelController {
	private static final String prefix="/quant/panel";

	@Autowired
	StocksService stocksService;

	@GetMapping()
	public String  panelIndex(ModelMap map){
		map.put("message","helloworld");
		map.put("name","helloworld");
		return prefix+"/panel";
	}

	@GetMapping("/stocks")
	@ResponseBody
	public PageAjax stockList(Stocks stocks){
		PageAjax pageAjax = stocksService.pageList(stocks);
		return pageAjax;
	}

}
