package com.ruoyi.web;

import com.ruoyi.quant.trade.crud.domain.DfcfTradeUser;
import com.ruoyi.quant.trade.utils.BeanMapUtils;

import java.util.Map;

public class TestBean2Map {

    public static void main(String[] args) {
        DfcfTradeUser dfcfTradeUser = new DfcfTradeUser();
        dfcfTradeUser.setAccountId("123123");
        dfcfTradeUser.setPassword("123123");
        Map<String, Object> stringObjectMap = BeanMapUtils.bean2Map(dfcfTradeUser, true);
        System.out.println(stringObjectMap);
    }
}
