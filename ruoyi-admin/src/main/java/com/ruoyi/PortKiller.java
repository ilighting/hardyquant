package com.ruoyi;

import java.net.Socket;

public class PortKiller {

    public static void main(String[] args) {
        // 指定要查杀的端口
        int port = 8088;

        try {
            // 创建一个Socket对象并尝试连接该端口
            Socket socket = new Socket("localhost", port);

            // 如果连接成功，则说明端口被占用
            System.out.println("端口 " + port + " 被占用");

            // 关闭Socket连接
            socket.close();
        } catch (Exception ex) {
            // 如果连接失败，则说明端口未被占用
            System.out.println("端口 " + port + " 未被占用");
        }
    }
}
