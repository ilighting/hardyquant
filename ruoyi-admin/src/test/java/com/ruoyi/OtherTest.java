package com.ruoyi;

import com.ruoyi.quant.domain.Stocks;
import com.ruoyi.quant.service.StocksService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

@SpringBootTest
public class OtherTest {

	@Autowired
	StocksService stocksService;

	@Test
	public void testO1() {
		Stocks stocks=new Stocks();
		stocks.setCode("000635");
		Stocks byCol = stocksService.getOne(stocks);

		System.out.println(byCol);
	}
	@Test
	public  void test02(){
		Stocks stocks=new Stocks();
		stocks.setCode("123");
		getInfo(stocks);
	}


	@SneakyThrows
	public <T> void getInfo(T entity){
		// Get the BeanInfo for the JavaBean class
		BeanInfo beanInfo = Introspector.getBeanInfo(entity.getClass());

		// Iterate over the PropertyDescriptors
		for (PropertyDescriptor propertyDescriptor : beanInfo.getPropertyDescriptors()) {
			// Get the name of the property
			String propertyName = propertyDescriptor.getName();

			// Get the getter method for the property
			Method getter = propertyDescriptor.getReadMethod();

			// Invoke the getter method to get the value of the property
			Object propertyValue = getter.invoke(entity);

			// Print the name and value of the property
			System.out.println(propertyName + ": " + propertyValue);
		}
	}



}
