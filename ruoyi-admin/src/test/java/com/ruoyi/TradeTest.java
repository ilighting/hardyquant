package com.ruoyi;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.ruoyi.quant.domain.Stocks;
import com.ruoyi.quant.domain.TradeModel;
import com.ruoyi.quant.pipe.TradePipeService;
import com.ruoyi.quant.service.StocksService;
import com.ruoyi.quant.trade.crud.domain.DfcfTradeView;
import com.ruoyi.quant.trade.crud.service.IDfcfTradeViewService;
import nonapi.io.github.classgraph.json.Id;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TradeTest {
	@Autowired
	TradePipeService tradePipeService;

	@Autowired
	StocksService stocksService;

	@Autowired
	IDfcfTradeViewService tradeViewService;

	@Test
	public void test01(){
		int duration = 2;
		int tradeDay = 252;
		TradeModel tradeModel=new TradeModel();
		tradeModel.setCode("002176");
		tradeModel.setTradeDays(tradeDay);
		tradeModel.setDuration(2);
		tradeModel.setModelType("CCI");

		DfcfTradeView view = createView(tradeModel.getCode());
		if (null!=view) {
			tradePipeService.cciTradeDemo(tradeModel,view);
		}

	}

	public DfcfTradeView createView(String code){
		Stocks stocks=new Stocks();
		stocks.setCode(code);
		Stocks one = stocksService.getOne(stocks);
		DfcfTradeView tradeView=new DfcfTradeView();

		tradeView.setCode(code);
		tradeView.setName(one.getName());
		tradeView.setId(IdWorker.getId());
		int i = tradeViewService.insertDfcfTradeView(tradeView);
		return tradeView;
	}
}
