package com.ruoyi;


import com.ruoyi.quant.domain.query.AnalysisQuery;
import com.ruoyi.quant.pipe.AnalysisPipeService;
import com.ruoyi.quant.tdx.Market;
import com.ruoyi.quant.tdx.TdxCategory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

@SpringBootTest
public class TestPipes {

    @Autowired
    AnalysisPipeService analysisPipeService;

    @Test
    public void testAdosc() {
        AnalysisQuery query = new AnalysisQuery();
        query.setCode("002176");
        query.setName("江特电机");
        query.setTdxCategory(TdxCategory.day);
        query.setMarket(Market.sz);
        query.setTalibName("talibcci");


        analysisPipeService.workDay(query);
    }

    @Test
    public void updateQuote() {
        AnalysisQuery query = new AnalysisQuery();
        query.setCode("002176");
        query.setName("江特电机");
        query.setTdxCategory(TdxCategory.day);
        query.setMarket(Market.sz);
        query.setTalibName("adosc");
        analysisPipeService.quoteUpdate(query);
    }

    @Test
    public void testAdosc2() {
        AnalysisQuery query = new AnalysisQuery();
        query.setCode("600100");
        query.setName("同方股份");
        query.setTdxCategory(TdxCategory.day);
        query.setMarket(Market.sh);
        query.setTalibName("talibcci");
        query.setCount(2000);

        analysisPipeService.quoteUpdate(query);
        analysisPipeService.workDay(query);
    }

    @Test
    public void testMacd() {
        AnalysisQuery query = new AnalysisQuery();
        query.setCode("600100");
        query.setName("同方股份");
        query.setTdxCategory(TdxCategory.m1);
        query.setMarket(Market.sh);
        query.setTalibName("minute_talib_macd");
        query.setCount(2000);
        analysisPipeService.workMinute(query);
    }

    @Test
    public void testPlainText() {
        String abc = "12.000000000000000000000000";
        BigDecimal d = new BigDecimal(abc);
        System.out.println(d.toPlainString());
    }

    @Test
    public void testCci() {
        AnalysisQuery query = new AnalysisQuery();
        query.setCode("002176");
        query.setName("江特电机");
        query.setTdxCategory(TdxCategory.day);
        query.setMarket(Market.sz);
        query.setTalibName("talibcci");
        analysisPipeService.workDay(query);


    }
//@Test
//    public void testCci(){
//        AnalysisQuery query=new AnalysisQuery();
//        query.setCode("002176");
//        query.setName("江特电机");
//        query.setCategory(Category.day);
//        query.setMarket(Market.sz);
//        query.setTalibName("talibcci");
//        query.setCount(2000);
//        analysisPipeService.work(query);
//    }

//    @Test
//    public void dde() {
//        double[] input = new double[]{1, 2, 3, 4, 5}; // 输入数据
//        double[] output = new double[input.length]; // 输出结果
//
//        Core core = new Core();
//        MInteger begin = new MInteger();
//        MInteger length = new MInteger();
//
//        // 计算 DDE
//        core.dde(0, input.length - 1, input, input, begin, length, output);
//
//        // 打印结果
//        for (int i = 0; i < input.length; i++) {
//            System.out.println("DDE 值[" + i + "]: " + output[i]);
//        }
//    }
//    @Autowired
//    CommonTaService<CciModel> taModelService;


    @Test
    public void testdelete() {


//        Long code = taModelService.deleteAllByCol("code", "002176",CciModel.class);


    }


}
