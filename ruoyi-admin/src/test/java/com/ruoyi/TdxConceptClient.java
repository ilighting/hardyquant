package com.ruoyi;


import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.mongodb.BasicDBObject;

import com.ruoyi.quant.basedata.domain.StockCode;
import com.ruoyi.quant.basedata.domain.StockConcept;
import com.ruoyi.quant.basedata.service.IStockCodeService;
import com.ruoyi.quant.basedata.service.IStockConceptService;
import com.ruoyi.quant.domain.TdxConcept;
import com.ruoyi.quant.service.StocksService;
import com.ruoyi.quant.service.TdxConceptService;
import com.ruoyi.quant.tdx.*;
import com.ruoyi.quant.tdx.domain.TickTransactions;
import com.ruoyi.quant.tdx.impl.TdxClientImpl;
import lombok.SneakyThrows;
import org.apache.commons.collections4.ListUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@SpringBootTest
public class TdxConceptClient {
	private void printBytes(byte[] bytes, int len) {
		for (int i = 0; i < len; ++i) {
			System.out.print(bytes[i] + " ,");
		}
	}

	public void testAddress() {
		InetSocketAddress address1 = new InetSocketAddress("localhost", 8080);
		InetSocketAddress address2 = new InetSocketAddress("localhost", 8080);
		assert address2.equals(address1);
	}

//    public void testAsync() throws IOException, InterruptedException, ExecutionException {
//
//        final BlockingQueue queue = new LinkedBlockingQueue();
//
//        final TdxClientImpl[] clients = new TdxClientImpl[2];
//        List<FutureTask> tasks = new ArrayList<FutureTask>();
//
//
//
//        for(int i=0; i < 2; ++i){
//            TdxClientImpl client = new TdxClientImpl();
//            client.setSocketAddress(new InetSocketAddress("119.147.212.81", 7709));
//            client.connect();
//            clients[i] = client;
//            Thread thread= new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try{
//
//                        String code = "000001";
//                        int start = 0;
//
//
//                        while (true){
//                            FutureTask data = (FutureTask)queue.poll(20000, TimeUnit.MILLISECONDS);
//                            if(data==null){
//                                break;
//                            }
//
//                            data.run();
//                            data.get();
//
//
//                        }
//
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }
//            });
//            thread.setName(String.valueOf(i));
//            thread.start();
//        }
//
//
//        long now = System.currentTimeMillis();
//        for(int i=0; i < 100; ++i){
//            FutureTask task = new FutureTask(new Callable() {
//                @Override
//                public Object call() throws Exception {
//
//                    Thread thread = Thread.currentThread();
//                    String name = thread.getName();
//                    int index = Integer.parseInt(name);
//                    TdxClientImpl  client = clients[index];
//                    List<Quote> quotes = client.getIndexQuotes(Category.day, Market.sh,"000001",0,1);
//                    return null;
//                }
//            });
//            queue.add(task);
//            tasks.add(task);
//        }
//
//        for(int i=0; i < 100; ++i){
//            tasks.get(i).get();
//        }
//
//        System.out.println("耗时"+(System.currentTimeMillis()-now));
//    }

	public void test() throws IOException {
		TdxClientImpl client = new TdxClientImpl();
		client.setSocketAddress(new InetSocketAddress("119.147.212.81", 7709));
		client.connect();

//        String code = "000001";
//        int start = 0;
//        int count = 1;
//
//        System.out.println(client.getCount(Market.sh));
//        System.out.println(client.getCount(Market.sz));

		//        List<Quote> quotes = client.getQuotes(Category.day, Market.sz,code,start,100);
//        System.out.println(quotes);
//
//        System.out.println(client.getStockList(Market.sh, 0));
//
//        System.out.println(client.getIndexQuotes(Category._d,Market.sh,code,start,100));

//
//        for(char i='a'; i <= 'z'; ++i){
//            for(char j='a'; j <= 'z'; ++j){
//                try{
//                    System.out.println(client.getBlockInfo(String.format("block_%c%c.dat",i,j)));
//                    System.out.println(String.format("%c%c",i,j));
//                }catch (Exception e){
//
//                }
//            }
//        }
		// System.out.println(client.getBlockInfo("spblock.dat"));
	}

	static final String path = "/Users/sxq/Desktop/job5/hardy-quant/javatdx/src/main/java/com/quant/tdx/downloads/";

	public void testReadFile() throws IOException {

		TdxClientImpl client = new TdxClientImpl();
		client.setSocketAddress(new InetSocketAddress("119.147.212.81", 7709));
		client.connect();
		client.setTdxRootDir(path);
		Collection<BlockStock> stocks = new ArrayList<>();
		//通达信行业 2
		System.out.println("通达信行业=============");
//        stocks=client.getBlockInfo(BlockType.TdxIndustry);
//        System.out.println(stocks);

		System.out.println("正在获取概念=============");
		stocks = client.getBlockInfo(BlockType.Concept);
		System.out.println(stocks);

		System.out.println("正在获取指数=============");
		stocks = client.getBlockInfo(BlockType.Index);
		System.out.println(stocks);

		System.out.println("正在获取风格=============");
		stocks = client.getBlockInfo(BlockType.Style);
		System.out.println(stocks);
	}

	@Autowired
	TdxConceptService tdxConceptService;



	@SneakyThrows
	@Test
	public void saveTdxIndexs(){
		TdxClientFactory service = TdxClientFactory.getInstance(1);

		System.out.println("正在获取指数=============");

		Future<Collection<BlockStock>> blockInfo = service.getBlockInfo(BlockType.Index);
		Collection<BlockStock> blockStocks = blockInfo.get();
		tdxConceptService.saveStockBlocks(blockStocks);

		System.out.println();
	}
	@Autowired
	private IStockCodeService stockCodeService;


	@SneakyThrows
	@Test
	public void saveTdxStockListIndexs(){
		TdxClientFactory service = TdxClientFactory.getInstance(1);

		System.out.println("正在获取指数=============");

		Future<Integer> shCount = service.getCount(Market.sh);
		Future<Integer> szCount = service.getCount(Market.sz);
		Integer shi = shCount.get();
		Integer szi = szCount.get();
		List<StockCode> stockCodeList=new ArrayList<>();
		List<StockInfo> stockInfos=new ArrayList<>();
		for (int i = 1000; i < shi; i=i+1000) {
			Future<List<StockInfo>> shStockList = service.getStockList(Market.sh, i);
			System.out.println(shStockList.get());
			stockInfos.addAll(shStockList.get());

		}
		for (int i = 1000; i < szi; i=i+1000) {
			Future<List<StockInfo>> szStockList = service.getStockList(Market.sz, i);
			stockInfos.addAll(szStockList.get());
		}

		for (List<StockInfo> infos : ListUtils.partition(stockInfos, 1000)) {
			List<StockCode> collect = infos.stream().map(item -> {
				StockCode stockCode = new StockCode();
				stockCode.setCode(item.getCode());
				stockCode.setId(IdWorker.getId());
				stockCode.setMarket(item.getMarket().name());
				stockCode.setName(item.getName());
				return stockCode;
			}).collect(Collectors.toList());
			stockCodeService.saveBatch(collect);
		}


	}



	@SneakyThrows
	@Test
	public void tdxHy(){
		TdxClientFactory service = TdxClientFactory.getInstance(1);
		System.out.println("通达信行业=============");
		Future<List<BlockStock>> blockInfo = service.getBlockInfo(BlockFileType.BLOCK_FG);
		Collection<BlockStock> stocks = blockInfo.get();
//		tdxConceptService.saveStockBlocks(stocks);
		System.out.println(stocks);
	}
	@SneakyThrows
	@Test
	public void tdxStyleList(){
		TdxClientFactory service = TdxClientFactory.getInstance(1);
		System.out.println("正在获取风格=============");
		Future<Collection<BlockStock>> styleStocks = service.getBlockInfo(BlockType.Style);
		Collection<BlockStock> stocks = styleStocks.get();
		tdxConceptService.saveStockBlocks(stocks);
//		System.out.println(stocks);
	}
	@SneakyThrows
	@Test
	public void tdxIndexList(){
		TdxClientFactory service = TdxClientFactory.getInstance(1);
		System.out.println("正在获取指数=============");
		Future<Collection<BlockStock>> blockInfo = service.getBlockInfo(BlockType.Index);
		Collection<BlockStock> stocks = blockInfo.get();
		tdxConceptService.saveStockBlocks(stocks);
//		System.out.println(stocks);
	}

	@Autowired
	private IStockConceptService stockConceptService;

	/**
	 * 通达信概念
	 */
	@SneakyThrows
	@Test
	public void tdxGainianList() {
		TdxClientFactory service = TdxClientFactory.getInstance(1);

		System.out.println("正在获取概念=============");
		Future<Collection<BlockStock>> blockInfo = service.getBlockInfo(BlockType.Concept);
		Collection<BlockStock> blockStocks = blockInfo.get();
		System.out.println(blockStocks);
		List<StockConcept> concepts=new ArrayList<>();
		for (BlockStock blockStock : blockStocks) {
			StockConcept concept=new StockConcept();
			concept.setCode(blockStock.getCode());
			concept.setCodes(String.join(",",blockStock.getCodes()));
			concept.setId(IdWorker.getId());
			concept.setName(blockStock.getName());
			concept.setType(blockStock.getType().getType());
			concept.setLevel(blockStock.getLevel());
			concepts.add(concept);
		}
		stockConceptService.saveBatch(concepts);
	}

	@Autowired
	StocksService stocksService;

	@Test
	public void getGnList() {

//		List<TdxConcept> list = tdxConceptService.lambdaAggregate().match(new Wrapper<TdxConcept>().eq(TdxConcept::getType,BlockType.Index.name())).
//				custom(BasicDBObject.parse("{\n" +
//						"  \"$limit\":2 \n" +
//						"}\n")).custom(BasicDBObject.parse("{\n" +
//						"  \"$skip\":0 \n" +
//						"}\n")).list();
//		System.out.println(list);
	}

	@Test
	public void gainianList() {
//
//		List<TdxConcept> list1 = tdxConceptService.lambdaAggregate()
//				.lookup("stocks", "codes", "code", "stockList")
//				.custom(BasicDBObject.parse("{\n" +
//						"  \"$skip\":1 \n" +
//						"}\n"))
//				.custom(BasicDBObject.parse("{\n" +
//						"  \"$limit\":2 \n" +
//						"}\n")).list();
//
//		System.out.println(list1);
	}


	@Test
	@SneakyThrows
	public void test1() {
		TdxClientFactory service = TdxClientFactory.getInstance(1);
//		Future<List<TimePrice>> timePrice = service.getTimePrice(Market.sz, "002262");
//		List<TimePrice> timePrices = timePrice.get();
//		System.out.println(timePrices);
		Future<List<TickTransactions>> historyTimePrice = service.getHistoryTicks(Market.sz, "002262", "20230925");


		List<TickTransactions> timeHisPrices = historyTimePrice.get();
		System.out.println(timeHisPrices);

	}


}
