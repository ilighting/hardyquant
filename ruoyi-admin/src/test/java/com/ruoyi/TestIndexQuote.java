package com.ruoyi;


import com.ruoyi.quant.tdx.Market;
import com.ruoyi.quant.tdx.TdxCategory;
import com.ruoyi.quant.tdx.TdxClientFactory;
import com.ruoyi.quant.tdx.domain.Quote;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.concurrent.Future;

@SpringBootTest
public class TestIndexQuote {

    @Test
    @SneakyThrows
    public void testIndexQuote(){
        TdxClientFactory service = TdxClientFactory.getInstance(1);
        Future<List<Quote>> indexQuotes = service.getIndexQuotes(TdxCategory.m5, Market.sh, "600519", 0, 10);
        List<Quote> quotes = indexQuotes.get();
        System.out.println(quotes);
    }
}
