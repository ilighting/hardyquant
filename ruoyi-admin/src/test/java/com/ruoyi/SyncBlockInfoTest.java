package com.ruoyi;


import com.ruoyi.quant.tdx.BlockStock;
import com.ruoyi.quant.tdx.BlockType;
import com.ruoyi.quant.tdx.TdxClientFactory;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;
import java.util.concurrent.Future;

@SpringBootTest
public class SyncBlockInfoTest {

	@SneakyThrows
	@Test
	public void test(){
		TdxClientFactory service = TdxClientFactory.getInstance(1);
		Future<Collection<BlockStock>> blockInfo = service.getBlockInfo(BlockType.Area);
		System.out.println(blockInfo.get());

	}
}
