package com.ruoyi;


import com.ruoyi.quant.tdx.domain.Quote;
import com.ruoyi.quant.tdx.reader.TdxQuoteReader;
import com.ruoyi.quant.tdx.utils.TdxDataInputStream;
import com.ruoyi.quant.tdx.utils.TdxDataOutputStream;
import com.ruoyi.quant.tdx.writer.TdxQuoteWriter;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;


public class TestReaderWriter{

    @Test
    public void test() throws IOException {
        Quote quote = new Quote();
        quote.setDate("20190101");
        quote.setOpen(1);
        quote.setClose(2);
        quote.setHigh(3);
        quote.setLow(4);
        quote.setAmt(5);
        quote.setVol(6);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TdxDataOutputStream os = new TdxDataOutputStream(byteArrayOutputStream);
        TdxQuoteWriter.writeForDay(quote, os);
        byte[] bytes = byteArrayOutputStream.toByteArray();

        ByteArrayInputStream is = new ByteArrayInputStream(bytes);
        TdxDataInputStream dataInputStream = new TdxDataInputStream();
        dataInputStream.setBuf(bytes);
        Quote quote1 = TdxQuoteReader.parseForDay(dataInputStream);


        System.out.println(quote1);


        assert (quote1.getDate().equals(quote.getDate()));

    }

    public void test2() throws IOException {
        Quote quote = new Quote();
        quote.setDate("201901010302");
        quote.setOpen(1);
        quote.setClose(2);
        quote.setHigh(3);
        quote.setLow(4);
        quote.setAmt(5);
        quote.setVol(6);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream os = new DataOutputStream(byteArrayOutputStream);
        TdxQuoteWriter.writeForMin(quote, os);
        byte[] bytes = byteArrayOutputStream.toByteArray();

        ByteArrayInputStream is = new ByteArrayInputStream(bytes);
        TdxDataInputStream dataInputStream = new TdxDataInputStream();
        dataInputStream.setBuf(bytes);
        Quote quote1 = TdxQuoteReader.parseForMin(dataInputStream);


        System.out.println(quote1);

        assert (quote1.getDate().equals(quote.getDate()));

    }
}
