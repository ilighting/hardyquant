package com.ruoyi;


import com.ruoyi.quant.domain.TdxIpConfig;
import com.ruoyi.quant.service.IpConfigService;
import com.ruoyi.quant.tdx.impl.IpInfo;
import com.ruoyi.quant.tdx.impl.Ips;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class TestGoodIpInfo {

	@Autowired
	IpConfigService ipConfigService;

	@Test
	public void testIpInfo(){
		List<TdxIpConfig> configs =new ArrayList<>();
		for (IpInfo ipInfo : Ips.IP_INFOS) {

			boolean isConnected = testAddress(ipInfo.getHost(), ipInfo.getPort());
			if (isConnected) {
				TdxIpConfig ipConfig=new TdxIpConfig();
				ipConfig.setIp(ipInfo.getHost());
				ipConfig.setPort(ipInfo.getPort());
				ipConfig.setName(ipInfo.getName());
				configs.add(ipConfig);

				System.out.println("连接成功");
			} else {
				System.out.println("连接失败");
			}
		}
		ipConfigService.saveAll(configs);

	}
	public static boolean testAddress(String host, int port) {
		try (Socket socket = new Socket()) {
			InetSocketAddress address = new InetSocketAddress(host, port);
			socket.connect(address, 1000); // 设置连接超时时间为1秒
			return true;
		} catch (IOException e) {
			return false;
		}
	}


}
