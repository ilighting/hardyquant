package com.ruoyi;


import com.ruoyi.quant.service.StockQuoteService;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @className: Talib4jExamples
 * @author: Yan Lang
 * @date: 2022/12/11 15:39
 * @description: 在Java中使用ta-lib
 */
@SpringBootTest
public class Talib4jExamples {


	@Autowired
	StockQuoteService service;


	@Test
	public void TEST01() {
		// 示例数据：收盘价数组
		double[] closePrice = {
				100, 110, 120,150, 220, 3220,6660, 11110, 99990,12312312, 123123131, 12312312
		};

		// 创建 Core 对象
		Core core = new Core();

		// 计算 MACD
		int beginIndex = 0; // 起始索引
		int endIndex = closePrice.length - 1; // 结束索引
		int optInFastPeriod = 12; // 快速移动平均线期间数
		int optInSlowPeriod = 26; // 慢速移动平均线期间数
		int optInSignalPeriod = 9; // MACD信号线期间数

		// 分配输出内存
		MInteger outBegIdx = new MInteger();
		MInteger outNbElement = new MInteger();
		double outMACD[] = new double[ closePrice.length];
		double outMACDSignal[] = new double[ closePrice.length];
		double outMACDHist[] = new double[ closePrice.length];

		// 调用 talib4j 的 MACD 方法计算 MACD 指标
		core.macd(beginIndex, endIndex, closePrice, optInFastPeriod, optInSlowPeriod, optInSignalPeriod,
				outBegIdx, outNbElement, outMACD, outMACDSignal, outMACDHist);

		// 打印计算结果
		for (int i = 0; i < closePrice.length; i++) {
			System.out.println("MACD: " + outMACD[i] + ", MACD Signal: " + outMACDSignal[i] + ", MACD Histogram: " + outMACDHist[i]);
		}
	}
}
