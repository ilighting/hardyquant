package com.ruoyi;

import cn.hutool.core.date.DateUtil;

import com.ruoyi.quant.domain.StockQuote;
import com.ruoyi.quant.domain.Stocks;
import com.ruoyi.quant.domain.models.DayTaModel;
import com.ruoyi.quant.service.CommonTaService;
import com.ruoyi.quant.service.StockQuoteService;
import com.ruoyi.quant.service.StocksService;
import com.ruoyi.quant.tdx.Market;
import com.ruoyi.quant.tdx.TdxCategory;
import com.ruoyi.quant.tdx.TdxClientFactory;
import com.ruoyi.quant.tdx.domain.Quote;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@SpringBootTest
public class TestSocketClientService{

    @Autowired
    private StocksService stocksService;


    @Test
    public void testO1() {
//        Stocks byCol = stocksService.getByCol(Stocks::getCode, "000635");
//        System.out.println(byCol);
    }
    @Test
    public void getList(){
        List<Stocks> stockList = stocksService.listAll(new Stocks());
        System.out.println(stockList);
    }

    @Test
    public void testKLine(){

    }

    @Autowired
    StockQuoteService quoteService;

    @Test
    public void test() throws ExecutionException, InterruptedException {
        TdxClientFactory service = TdxClientFactory.getInstance(1);
        long now = System.currentTimeMillis();
//        for(int i=0; i < 1; ++i){
//            Future<List<Quote>> quotes = service.getQuotes(Category.m1, Market.sh, "000001", 0, 1);
//            System.out.println(quotes.get());
//        }
//        System.out.println("耗时"+(System.currentTimeMillis()-now));

//
//        Future<List<Quote>> quotes = service.getQuotes(Category._d, Market.sh, "000001", 0, 1);
//
//        System.out.println(quotes.get());

//
//        Future<Integer> count = service.getCount(Market.sh);
//        System.out.println(count.get());
//        Integer szCount = count.get();
//        int pageSize=1000;
//        int i=0;
//
//        while(i<=szCount){
//
//            Future<List<StockInfo>> list = service.getStockList(Market.sh, i);
//            i=i+pageSize;
//            List<StockInfo> result = list.get();
//            System.out.println(result);
//            System.out.println("************************************************");
//            System.out.println(i);
//            if(null!=result){
//                stocksService.saveBatch(result);
//            }
//
//        }
        String code="002536";
        int count=50000;
        int pageSize=1000;
        int i=0;
        List<Quote> total=new ArrayList<>();
        while(i<=count){
            Future<List<Quote>> quotes = service.getQuotes(TdxCategory.day, Market.sz, code, i, pageSize);
//        Future<List<StockInfo>> stockList = service.getStockList();
            i=i+pageSize;
            List<Quote> quoteList = quotes.get();
            total.addAll(quoteList);
        }
        total.sort((o1, o2) -> DateUtil.date(Long.parseLong(o1.getDate())).after(DateUtil.date(Long.parseLong(o2.getDate())))?1:-1);
        if(total.size()>0){
            quoteService.saveQuotes(total,code);
        }

    }

    @SneakyThrows
    @Test
    public void getQuote(){
        TdxClientFactory service = TdxClientFactory.getInstance(1);
        String code="000536";
        Future<List<Quote>> quotes = service.getQuotes(TdxCategory.day, Market.sz, code, 0, 1);
        System.out.println(quotes.get());
//        int count=50000;
//        int pageSize=1000;
//        int i=0;
//        List<Quote> total=new ArrayList<>();
//        while(i<=count){
//            Future<List<Quote>> quotes = service.getQuotes(Category.day, Market.sz, code, i, pageSize);
//            i=i+pageSize;
//            List<Quote> quoteList = quotes.get();
//            total.addAll(quoteList);
//        }
//        total.sort((o1, o2) -> DateUtil.date(Long.parseLong(o1.getDate())).after(DateUtil.date(Long.parseLong(o2.getDate())))?1:-1);
//        if(total.size()>0){
//            quoteService.saveQuotes(total,code);
//        }

    }




    /**
     * The number of periods to average together.
     * 含义：20日移动平均
     */
    public static final int PERIODS_AVERAGE = 20;

    /*
    简单移动平均，需要懂的参数一览:
        int startIdx：开始的索引
        int endIdx：结束的索引
        double[] inReal：收盘价集合
        int optInTimePeriod：移动平均的周期数，如 MA20
        MInteger outBegIdx：如 MA20 线开始的索引值
        MInteger outNBElement：如 MA20 线的长度
        double[] outReal：当日的 MA20 移动平均线的数值
     */
    @Test
    public void simpleMovingAverage(){
        List<StockQuote> list = quoteService.pageList(new StockQuote(),0,1000);
        //数据存在
        if (list != null){
            //参数准备
            //收盘价的数组
            double[] closePrice = new double[list.size()];
            //移动平均线数值的数组
            double[] out = new double[list.size()];
            //MInteger类: mutable integer，可变整数
            MInteger begin = new MInteger();
            MInteger length = new MInteger();

            //获取收盘价数组 length = 2580
            for (int i = 0; i < list.size(); i++) {
                double close =  list.get(i).getClose();
                closePrice[i] = close;
            }

            //Talib的核心类
            Core core = new Core();
            //调用核心类
            RetCode retCode = core.sma(
                    //int startIdx：开始的索引
                    0,
                    //int endIdx：结束的索引
                    closePrice.length - 1,
                    //收盘价集合
                    closePrice,
                    //移动平均的周期数，如 MA20
                    PERIODS_AVERAGE,
                    //如 MA20 线开始的索引值
                    begin,
                    //如 MA20 线的长度
                    length,
                    //当日的 MA20 移动平均线的数值
                    out
            );

            //打印信息
            if (retCode == RetCode.Success) {

                //数值上 = PERIODS_AVERAGE-1
                System.out.println("输出开始的周期数: " + begin.value);
                //总周期数
                System.out.println("输出结束的周期数: " + (begin.value + length.value - 1));

                //遍历有线
                for (int i = begin.value; i < begin.value + length.value; i++) {
                    //检验当前记录对应的id
                    String id = list.get(i).getDate();
                    System.out.println("当前记录的id: "+id);
                    //当日收盘价（保留两位小数）
                    String tempClose = String.format("%.2f", closePrice[i]);
                    //移动平均数（保留两位小数）
                    String tempMovAver = String.format("%.2f", out[i - begin.value]);
                    //拼接字符串
                    String line = "Period # " +
                            i +
                            " 当日收盘价 = " +
                            tempClose +
                            " 移动平均数 = " +
                            tempMovAver;
                    System.out.println(line);
                }
            }
            else {
                System.out.println("Error");
            }

        }

    }

    @Test
    public void cci(){
        List<StockQuote> list = quoteService.pageList(new StockQuote(),0,1000);
        //数据存在
        if (list != null){
            //参数准备
            //收盘价的数组
            double[] closePrice = new double[list.size()];
            double[] highPrice = new double[list.size()];
            double[] lowPrice = new double[list.size()];
            double[] volPack = new double[list.size()];
            //移动平均线数值的数组
            double[] out = new double[list.size()];
            //MInteger类: mutable integer，可变整数
            MInteger begin = new MInteger();
            MInteger length = new MInteger();

            //获取收盘价数组 length = 2580
            for (int i = 0; i < list.size(); i++) {
                double close =  list.get(i).getClose();
                double high =  list.get(i).getHigh();
                double low =  list.get(i).getLow();
                double vol =  list.get(i).getVol();

                closePrice[i] = close;
                highPrice[i]=high;
                lowPrice[i]=low;
                volPack[i]=vol;
            }

            //Talib的核心类
            Core core = new Core();
            //调用核心类
            RetCode retCode = core.cci(
                    //int startIdx：开始的索引
                    0,
                    //int endIdx：结束的索引
                    closePrice.length - 1,
                    //高价集合
                    highPrice,

                    //低价集合
                    lowPrice,
                    //收盘价集合
                    closePrice,
                    //移动平均的周期数，如 MA20
                    PERIODS_AVERAGE,
                    //如 MA20 线开始的索引值
                    begin,
                    //如 MA20 线的长度
                    length,
                    //当日的 MA20 移动平均线的数值
                    out
            );

            //打印信息
            if (retCode == RetCode.Success) {

                //数值上 = PERIODS_AVERAGE-1
                System.out.println("输出开始的周期数: " + begin.value);
                //总周期数
                System.out.println("输出结束的周期数: " + (begin.value + length.value - 1));

                //遍历有线
                for (int i = begin.value; i < begin.value + length.value; i++) {
                    //检验当前记录对应的id
                    String id = list.get(i).getDate();
                    System.out.println("当前记录的id: "+id);
                    //当日收盘价（保留两位小数）
                    String tempClose = String.format("%.6f", closePrice[i]);
                    //移动平均数（保留两位小数）
                    String tempMovAver = String.format("%.6f", out[i - begin.value]);
                    //拼接字符串
                    String line = "Period # " +
                            i +
                            " 当日收盘价 = " +
                            tempClose +
                            " cci移动平均数 = " +
                            tempMovAver;
                    System.out.println(line);
                }
            }
            else {
                System.out.println("Error");
            }

        }

    }

    /**
     * DDE = ((C-REF(C,1))/REF(C,1)) * VOL;
     * 其中，C表示当天的收盘价，REF(C,1)表示昨天的收盘价，VOL表示当天的成交量。该公式通过计算当天收盘价与昨天收盘价的差值，再乘以当天的成交量，得到DDE指标值。如果当天收盘价高于昨天收盘价，则DDE指标值为正数；如果当天收盘价低于昨天收盘价，则DDE指标值为负数。
     * 需要注意的是，DDE指标只是一种辅助指标，不能单独用于决策。在实际操作中，需要结合其他技术指标和市场基本面进行分析，以制定科学的投资策略。
     */
    @Test
    public void dde(){
        List<StockQuote> list = quoteService.pageList(new StockQuote(),0,1000);

    }
    /**
     * 1，函数名：AD
     *
     * 名称：Chaikin A/D Line 累积/派发线（Accumulation/Distribution Line） 简介：Marc
     * Chaikin提出的一种平衡交易量指标，以当日的收盘价位来估算成交流量，用于估定一段时间内该证券累积的资金流量。
     *
     * 例子：real = AD(high, low, close, volume)
     *
     * 2，函数名：ADOSC
     *
     * 名称：Chaikin A/D Oscillator Chaikin震荡指标
     * 简介：将资金流动情况与价格行为相对比，检测市场中资金流入和流出的情况 计算公式：fastperiod A/D - slowperiod
     * A/D 研判：1、交易信号是背离：看涨背离做多，看跌背离做空
     * 2、股价与90天移动平均结合，与其他指标结合
     * 3、由正变负卖出，由负变正买进 例子：real = ADOSC(high, low, close, volume, fastperiod=3, slowperiod=10)
     *
     * 3，函数名：OBV
     * 名称：On Balance Volume 能量潮
     * 简介：Joe Granville提出，通过统计成交量变动的趋势推测股价趋势
     * 计算公式：以某日为基期，逐日累计每日上市股票总成交量，若隔日指数或股票上涨，则基期OBV加上本日成交量为本日OBV。隔日指数或股票下跌，则基期OBV减去本日成交量为本日OBV
     * 研判：1、以“N”字型为波动单位，一浪高于一浪称“上升潮”，下跌称“跌潮”；上升潮买进，跌潮卖出
     * 2、须配合K线图走势
     * 3、用多空比率净额法进行修正，但不知TA-Lib采用哪种方法
     * 多空比率净额= [（收盘价－最低价）－（最高价-收盘价）] ÷（ 最高价－最低价）×成交量
     * 例子：real = OBV(close, volume)
     */

    @Autowired
    CommonTaService taModelService;
    @Test
    public void ADOSC(){
        String code="000536";
        int fast=3;
        int slow=10;
        //震荡指标
        List<StockQuote> list = quoteService.pageList(new StockQuote(),0,1000);
        int size = list.size();
        //数据存在
        if (list != null){
            //参数准备
            //收盘价的数组
            double[] closePrice = new double[list.size()];
            double[] highPrice = new double[list.size()];
            double[] lowPrice = new double[list.size()];
            double[] volPack = new double[list.size()];
            //移动平均线数值的数组
            double[] out = new double[list.size()];

            //获取收盘价数组 length = 2580
            for (int i = 0; i < size; i++) {
                double close =  list.get(i).getClose();
                double high =  list.get(i).getHigh();
                double low =  list.get(i).getLow();
                double vol =  list.get(i).getVol();

                closePrice[i] = close;
                highPrice[i]=high;
                lowPrice[i]=low;
                volPack[i]=vol;
            }
            MInteger outBegIdx= new MInteger();
            MInteger outNBElement= new MInteger();

            //Talib的核心类
            Core core = new Core();
            //调用核心类
            RetCode retCode = core.adOsc(
                    //int startIdx：开始的索引
                    0,
                    //int endIdx：结束的索引
                    size - 1,
                    //高价集合
                    highPrice,

                    //低价集合
                    lowPrice,
                    //收盘价集合
                    closePrice,
                    //成交量集合
                    volPack,

                    //快线
                    fast,
                    slow,
                    //如 MA20 线的长度
                    outBegIdx,
                    outNBElement,
                    //当日的 MA20 移动平均线的数值
                    out
            );

            //打印信息
            if (retCode == RetCode.Success) {

                //数值上 = PERIODS_AVERAGE-1
                System.out.println("输出开始的周期数: " + outBegIdx.value);
                //总周期数
                System.out.println("输出结束的周期数: " + ( outNBElement.value));
                List<DayTaModel> taModels=new ArrayList<>();
                //遍历有线
                for (int i = outBegIdx.value; i < size ; i++) {
                    //检验当前记录对应的id
                    String date = list.get(i).getDate();
                    System.out.println("当前记录的id: "+date);
                    //当日收盘价（保留两位小数）

                    DayTaModel taModel=new DayTaModel();

                    Double tempClose = closePrice[i];
                    //移动平均数（保留两位小数）
                    Double tempMovAver =out[i - outBegIdx.value];
//                    String tempNBE = String.format("%.6f", out[i - outNBElement.value]);
                    //拼接字符串
                    String line = "Period # " +
                            i +
                            " 当日收盘价 = " +
                            tempClose +
                            " asosc移动平均数 = " +
                            tempMovAver
//                            "tempNBE="+tempNBE
                            ;
                    System.out.println(line);
                    taModel.setCode(code);
                    taModel.setDate(date);
                    taModel.setClose(tempClose);
                    taModel.setVal(tempMovAver);
                    taModel.setType("ADOSC");
                    taModels.add(taModel);
                }
                taModelService.saveAll(taModels);
            }
            else {
                System.out.println("Error");
            }

        }


    }

}
